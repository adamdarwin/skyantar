const express = require('express'),
    router = express.Router();

const terminalAuth = require('./../middlewares/terminalAuth');

const TerminalModel = require('./../models/Terminal');
const OrdersTempModel = require('./../models/OrdersTemp');


module.exports = function () {

    /*
    * Terminal info
    * */

    router.get('/terminal', terminalAuth, async(req, res) => {
        try {
            req.terminal.log.push({url : req.originalUrl, method: req.method, status : 200 ,body : req.body});
            req.terminal = await req.terminal.save();
            delete req.terminal._doc.password;
            delete req.terminal._doc.tokens;
            return res.status(200).send(req.terminal);
        } catch (e) {
            console.log('/terminal', e.message);
            if(req.terminal){
                req.terminal.log.push({url : req.originalUrl, method: req.method, status : 500 , errorMessage : e.message, body : req.body});
                await req.terminal.save();
            }
            return res.sendStatus(500);
        }
    });

    /*
    * Login
    * */

    router.post('/terminal/login', async(req, res) => {
        try {
            if(!req.body.login || !req.body.password) return res.sendStatus(400);
            let terminal = await TerminalModel.findByCredentials({login: req.body.login, password: req.body.password});
            if(!terminal) return res.sendStatus(403);
            const token = await terminal.generateJWT();
            terminal.log.push({url : req.originalUrl, method: req.method, status : 200});
            terminal.status = 'active';
            terminal = await terminal.save();
            delete terminal._doc.tokens;
            delete terminal._doc.password;
            delete terminal._doc.log;
            return res.status(200).send({terminal, token});
        } catch (e) {
            console.log('/terminal/login', e.message);
            return res.sendStatus(500);
        }
    });

    /*
    * Logout
    * */

    router.post('/terminal/logout', terminalAuth, async(req, res) => {
        try{
            if (!req.terminal) return res.sendStatus(401);
            req.terminal.tokens = req.terminal.tokens.filter((token) => {
                return token.token !== req.token;
            });
            req.terminal.log.push({url : req.originalUrl, method: req.method, status : 200});
            await req.terminal.save();
            return res.status(200).send('Logged out');
        }catch (e) {
            console.log('/terminal/logout', e.message);
            if(req.terminal){
                req.terminal.log.push({url : req.originalUrl, method: req.method, status : 500 , errorMessage : e.message, body : req.body});
                await req.terminal.save();
            }
            return res.sendStatus(500);
        }
    });

    /*
    * Check sms code for order
    * */

    router.get('/terminal/sms', terminalAuth, async(req, res) => {
        try {
            if(!req.terminal) return res.sendStatus(401);
            if(!req.query.id || !req.query.sign){
                req.terminal.log.push({url : req.originalUrl, method: req.method, status : 400, errorMessage : 'Not enough fields: id: ' + req.query.id + ', sign: ' + req.query.sign});
                await req.terminal.save();
                return  res.sendStatus(400);
            }
            let order = await OrdersTempModel.findOne({id : req.query.id, phoneSign : req.query.sign});
            if(!order){
                req.terminal.log.push({url : req.originalUrl, method: req.method, status : 303, errorMessage : 'Not found'});
                await req.terminal.save();
                return  res.sendStatus(303);
            }
            req.terminal.log.push({url : req.originalUrl, method: req.method, status : 200});
            await req.terminal.save();
            return  res.sendStatus(200);
        } catch (e) {
            console.log('/terminal/sms', e.message);
            if(req.terminal){
                req.terminal.log.push({url : req.originalUrl, method: req.method, status : 500 , errorMessage : e.message, body : req.body});
                await req.terminal.save();
            }
            return res.sendStatus(500);
        }
    });

    return router;
};