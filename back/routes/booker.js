const express = require('express'),
    router = express.Router();

const bookerAuth = require('./../middlewares/bookerAuth');

const SubscriptionTypesModel = require('./../models/SubscriprtionTypes');
const ServicesModel = require('./../models/Services');
const ProductsModel = require('./../models/Products');

/*
* Check authorization
* */

router.get('/booker', bookerAuth, async (req, res) => {
    try {
        res.status(200).send('Welcome, ' + req.headers.login);
    }  catch (e) {
        console.log('/booker', e.message);
        return res.sendStatus(500);
    }
});

/*
* Edit something by id
* */

router.post('/booker/edit', bookerAuth, async(req, res) => {
    try {
        if(!req.body.query || !Array.isArray(req.body.query)) return res.sendStatus(400);

        let response = [];

        for (let i = 0; i < req.body.query.length; i++) {
            if(!req.body.query[i].id || typeof req.body.query[i].id !== 'string'){
                response.push(400);
                continue;
            }
            if(!req.body.query[i].time){
                response.push(400);
                continue;
            }
            if(req.body.query[i].time.find(val => val.cost < 0)) {
                response.push(400);
                continue;
            }
            switch (req.body.query[i].id.slice(0,1)) {
                case '0' : {
                    let service = await ServicesModel.findOne({id: req.body.query[i].id});
                    if(!service) { response.push(303); break;}

                    if(req.body.query[i].day_week < 1 || req.body.query[i].day_week > 7){ response.push(400); break;}
                    if(req.body.query[i].time[0].start){
                        if(!req.body.query[i].time[1].start){
                            response.push(400);
                            break;
                        }
                        service.evening_diff = req.body.query[i].time[0].start < req.body.query[i].time[1].start ? req.body.query[i].time[0].cost - req.body.query[i].time[1].cost : req.body.query[i].time[1].cost - req.body.query[i].time[0].cost;
                        service.cost[req.body.query[i].day_week === 7 ? 0 : req.body.query[i].day_week] = req.body.query[i].time[0].start < req.body.query[i].time[1].start ? req.body.query[i].time[0].cost : req.body.query[i].time[1].cost;
                        service.evening_time = req.body.query[i].time[0].start < req.body.query[i].time[1].start ?
                            (req.body.query[i].time[1].start < 10 ? '0' + req.body.query[i].time[1].start + ':00' : '' + req.body.query[i].time[1].start + ':00')
                            : (req.body.query[i].time[0].start < 10 ? '0' + req.body.query[i].time[0].start + ':00' : '' + req.body.query[i].time[0].start + ':00')
                    }else{
                        service.cost[req.body.query[i].day_week === 7 ? 0 : req.body.query[i].day_week] = req.body.query[i].time[0].cost;
                    }
                    if(req.body.query[i].Calendar){
                        if(!req.body.query[i].time ||!req.body.query[i].time[0].cost) {response.push(400); break;}
                        let custom = {dateStart: req.body.query[i].Calendar, dateEnd: new Date(new Date(new Date(req.body.query[i].Calendar).setUTCDate(new Date(req.body.query[i].Calendar).getDate() + 1)).setUTCHours(0,0,0)).toISOString(), cost: req.body.query[i].time[0].cost};
                        if(service.customPrice.find(val => new Date(custom.dateStart) <= new Date(val.dateEnd) && new Date(custom.dateEnd) >= new Date(val.dateStart))){response.push(400); break;}
                        service.customPrice.push(custom);
                    }
                    service.markModified('cost');
                    let result = await service.save();
                    delete result._doc.products;
                    delete result._doc.structure;
                    delete result._doc.preordered;
                    delete result._doc.status;
                    delete result._doc._id;
                    delete result._doc.time;
                    delete result._doc.type;
                    delete result._doc.__v;
                    response.push(result);
                    break
                }
                case '1' : {
                    let id = req.body.query[i].id.split('_');
                    let product = await ProductsModel.findOne({id: id[0]});
                    if(!product){response.push(303); break;}
                    if(id.length === 1){
                        if(Array.isArray(product.amount)){response.push(400); break;}
                        product.cost = req.body.query[i].time[0].cost;
                        let result = await product.save();
                        delete result._doc.preferred;
                        delete result._doc.status;
                        delete result._doc._id;
                        delete result._doc.__v;
                        response.push(result);
                        break
                    }else{
                        if(!Array.isArray(product.amount)){response.push(400); break}
                        product.amount.forEach(val => val.id === id[1] ? val.cost = req.body.query[i].time[0].cost : null);
                        product.markModified('amount');
                        let result = await product.save();
                        delete result._doc.preferred;
                        delete result._doc.status;
                        delete result._doc._id;
                        delete result._doc.__v;
                        response.push(result);
                        break;
                    }
                }
                case '2' : {
                    let subscription = await SubscriptionTypesModel.findOne({id: req.body.query[i].id});
                    if(!subscription){ response.push(303); break}
                    subscription.cost = req.body.query[i].time[0].cost;
                    let result = await subscription.save();
                    delete result._doc.maxVisitsPerTime;
                    delete result._doc.description;
                    delete result._doc._id;
                    delete result._doc.services;
                    delete result._doc.appliedProducts;
                    delete result._doc.visits;
                    delete result._doc.expired;
                    delete result._doc.setRelatives;
                    delete result._doc.age;
                    delete result._doc.__v;
                    response.push(result);
                    break
                }
                default: {
                    return res.sendStatus(400);
                }
            }
        }
        return res.status(200).send(response);
    } catch (e) {
        console.log('/booker/edit', e.message);
        return res.sendStatus(500);
    }
});

/*
* Edit subscription
* */

router.post('/booker/edit/subscriptions', bookerAuth, async (req, res) => {
    try {
        if(!req.body.id) return res.sendStatus(400);
        let subscription = await SubscriptionTypesModel.findOne({id: req.body.id});
        if(!subscription) return res.sendStatus(303);
        Object.keys(req.body).forEach(key => key === 'id' ? false : subscription[key] = req.body[key]);
        let result = await subscription.save();
        delete result._doc.maxVisitsPerTime;
        delete result._doc.description;
        delete result._doc._id;
        delete result._doc.services;
        delete result._doc.appliedProducts;
        delete result._doc.visits;
        delete result._doc.expired;
        delete result._doc.setRelatives;
        delete result._doc.age;
        delete result._doc.__v;
        return res.status(200).send(result);
    }catch (e) {
        console.log('/booker/edit/subscriptions', e.message);
        return res.sendStatus(400);
    }
});

/*
* Get subscriptions
* */

router.get('/booker/subscriptions', bookerAuth, async(req, res) => {
    try {
        return req.query.id ? res.status(200).send({arr :await SubscriptionTypesModel.find({id:  req.query.id})}) : res.status(200).send({arr :await SubscriptionTypesModel.find()});
    } catch (e) {
        console.log('/booker/subscriptions', e.message);
        return res.sendStatus(500);
    }
});

/*
* Edit service
* */

router.post('/booker/edit/services', bookerAuth, async (req, res) => {
    try {
        if(!req.body.id) return res.sendStatus(400);

        let service = await ServicesModel.findOne({id: req.body.id});
        if(!service) return res.sendStatus(303);
        if(req.body.cost){
            if(!req.body.day || req.body.day > 8 || req.body.day < 0) return res.sendStatus(400);
            await ServicesModel.findOneAndUpdate({id: req.body.id}, {['cost.' + (+req.body.day)] : req.body.cost});
            service = await ServicesModel.findOne({id: req.body.id});
        }
        Object.keys(req.body).forEach(key => {
            if(key === 'name' || key === 'bdayDiscount' || key === 'max_people')return service[key] = req.body[key];
        });
        let result = await service.save();
        delete result._doc.products;
        delete result._doc.structure;
        delete result._doc.preordered;
        delete result._doc.status;
        delete result._doc._id;
        delete result._doc.time;
        delete result._doc.type;
        delete result._doc.__v;
        return res.status(200).send(result);
    } catch (e) {
        console.log('/booker/edit/services', e.message);
        return res.sendStatus(400);
    }
});

/*
* Get services
* */

router.get('/booker/services', bookerAuth, async(req, res) => {
    try {
        return req.query.id ? res.status(200).send({arr :await ServicesModel.find({id:  req.query.id})}) : res.status(200).send({arr :await ServicesModel.find()});
    } catch (e) {
        console.log('/booker/subscriptions', e.message);
        return res.sendStatus(500);
    }
});

/*
* Edit product
* */

router.post('/booker/edit/products', bookerAuth, async (req, res) => {
    try {
        if(!req.body.id) return res.sendStatus(400);
        if(req.body.name && await ProductsModel.findOne({name: req.body.name})) return res.sendStatus(303);

        let product = await ProductsModel.findOne({id: req.body.id});
        if(!product) return res.sendStatus(303);

        if(typeof req.body.amount === 'object'){
            for (let i = 0; i < req.body.amount.length; i++) {
                if(!req.body.amount[i].option || !req.body.amount[i].amount || !req.body.amount[i].cost) return res.sendStatus(400);
                if((req.body.amount.filter(val => val.option === req.body.amount[i].option)).length > 1) return res.sendStatus(400);
            }
        }else{
            if(req.body.amount && typeof req.body.amount !== 'number') return res.sendStatus(400);
        }

        Object.keys(req.body).forEach(key => key === 'id' ? false : product[key] = req.body[key]);

        let result = await product.save();
        delete result._doc.preferred;
        delete result._doc.status;
        delete result._doc._id;
        delete result._doc.__v;
        return res.status(200).send(result);
    } catch (e) {
        console.log('/booker/edit/products', e.message);
        return res.sendStatus(500);
    }
});

/*
* Get products
* */

router.get('/booker/products', bookerAuth, async(req, res) => {
    try {
        return req.query.id ? res.status(200).send({arr :await ProductsModel.find({id:  req.query.id})}) : res.status(200).send({arr :await ProductsModel.find()});
    } catch (e) {
        console.log('/booker/subscriptions', e.message);
        return res.sendStatus(500);
    }
});

module.exports = router;