const express = require('express'),
    router = express.Router(),
    bcrypt = require('bcryptjs');



module.exports = function(io){
    router.get('/', async (req, res) => {
        try {
            io.sockets.emit('drop');
            return res.sendStatus(200);
        }catch (e) {
            console.log('/', e.message);
            return res.sendStatus(500);
        }
    });
    return router;
};