const express = require('express'),
    router = express.Router(),
    request = require('request-promise-native'),
    bcrypt = require('bcryptjs'),
    validator = require('validator'),
    objectHash = require('object-hash'),
    fs = require('fs'),
    LiqPay = require('./../liqpay/liqpay'),
    base64 = require('js-base64').Base64;

const SMSController = require('./../controllers/SMS');

const UsersModel = require('./../models/Users');
const OrdersModel = require('.././models/Orders');
const ServicesModel = require('.././models/Services');
const ProductsModel = require('.././models/Products');
const SubscriptionsModel = require('../models/Subscriptions');
const NotificationsModel = require('../models/Notifications');

const auth = require('.././middlewares/auth');

/*
* Generates verification code
*
* @return   {string}    Verification code
* */

const makeId = () => {
    let text = "";
    let possible = "0123456789";

    for (let i = 0; i < 4; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};

module.exports = function(io){
    /*
* Get verification code for register
* */

    router.get('/register', async (req, res) => {
        try {
            if(!validator.isMobilePhone(req.query.phone, 'uk-UA')){
                res.status(303).send('Invalid phone');
                return false;
            }
            if(await UsersModel.isExists(req.query.phone))return res.sendStatus(303);
            let code = makeId();
            let sign = bcrypt.hashSync(process.env.PHONE_SECRET + code + req.query.phone + process.env.PHONE_SECRET);
            await SMSController.send(req.query.phone, code);
            return res.status(200).send({sign: sign});
        } catch (e) {
            console.log('/register', e.message);
            return res.sendStatus(500);
        }
    });

    /*
    * Check verification code and get source information about user (if exists)
    * */

    router.get('/register/verify', async (req, res) => {
        try {
            if(!req.query.code || !req.query.sign || !req.query.phone) return res.sendStatus(400);

            if(!bcrypt.compareSync(process.env.PHONE_SECRET + req.query.code + req.query.phone + process.env.PHONE_SECRET, req.query.sign))
                return res.sendStatus(303);

            let info = false;
            if(info){
                return res.status(200).send({info: info});
            }else{
                return res.sendStatus(200);
            }
        }catch (e) {
            console.log(e.message);
            return res.sendStatus(500);
        }
    });

    /*
    * Register new user
    * */

    router.post('/register', async (req, res) => {
        try{
            if(req.body.email && !validator.isEmail(req.body.email)){return res.status(303).send('Invalid email');}
            if(!req.query.code || !req.query.sign || !req.query.phone) return res.sendStatus(400);
            if(await UsersModel.isExists(req.query.phone))return res.sendStatus(303);
            if(!bcrypt.compareSync(process.env.PHONE_SECRET + req.query.code + req.query.phone + process.env.PHONE_SECRET, req.query.sign))
                return res.sendStatus(303);
            let user = {
                name: req.body.name,
                sname: req.body.sname,
                email: req.body.email ? req.body.email : null,
                phone: req.query.phone,
                bday: req.body.bday ? req.body.bday : null,
                password: req.body.password,
            };
            let result = await UsersModel.setUser(user);
            if(result){
                delete result._doc.password;
                delete result._doc._id;
                delete result._doc.__v;
                return res.status(200).send(result);
            }else{
                console.log('User register', user);
                return res.sendStatus(503);
            }
        }catch (e) {
            console.log(e.message);
            return res.sendStatus(500);
        }

    });

    /*
    * Login with credentials
    * */

    router.post('/login', async (req, res) => {
        try{
            const user = await UsersModel.findByCredentials({phone: req.body.phone, password: req.body.password});
            if(!user)return res.status(404).send('Incorrect');
            const token = await user.generateJWT();
            delete user._doc.tokens;
            delete user._doc.password;
            return res.status(200).send({user, token});
        }catch (e) {
            return res.status(500).send(e.message);
        }
    });

    /*
    * Logout with token
    * */

    router.post('/logout', auth, async(req, res) => {
        try {
            if(!req.user)return res.sendStatus(401);
            req.user.tokens = req.user.tokens.filter((token) => {
                return token.token !== req.token;
            });
            await req.user.save();
            return res.status(200).send('Logged out');
        } catch (e) {
            console.log(e.message);
            return res.sendStatus(500);
        }
    });

    /*
    * Restore user password
    * */

    router.get('/restore', async (req, res) => {
        try {
            if(!req.query.phone || !validator.isMobilePhone(req.query.phone, 'uk-UA')) return res.status(303).send('Invalid params');
            if(!await UsersModel.findOne({phone: req.query.phone}))return res.status(303).send('Invalid params');
            let code = makeId();
            let sign = bcrypt.hashSync(process.env.PHONE_SECRET+code+req.query.phone+process.env.PHONE_SECRET);
            await SMSController.send(req.query.phone, code);
            return res.status(200).send({phone: req.query.phone, sign: sign , code: code});
        } catch (e) {
            console.log(e.message);
            return res.sendStatus(500);
        }
    });

    /*
    * Check is verification code is accepted
    * */

    router.get('/restore/confirm', async (req, res) => {
        try {
            if(req.query.phone && req.query.sign && req.query.code){
                if(bcrypt.compareSync(process.env.PHONE_SECRET+req.query.code+req.query.phone+process.env.PHONE_SECRET, req.query.sign)){
                    return res.sendStatus(200);
                }else{
                    return res.status(303).send('Invalid params');
                }
            }else{
                return res.status(303).send('Invalid params');
            }
        }catch (e) {
            console.log(e.message);
            return res.sendStatus(500);
        }
    });

    /*
    * Change user password
    * */

    router.post('/restore/confirm', async (req, res) => {
        try {
            if(req.query.phone && req.query.sign && req.query.code && req.body.password){
                if(bcrypt.compareSync((process.env.PHONE_SECRET+req.query.code+req.query.phone+process.env.PHONE_SECRET), req.query.sign)){
                    if(await UsersModel.findOneAndUpdate({phone: req.query.phone}, {password: req.body.password}, {runValidators: true})){
                        let user = await UsersModel.findOne({phone: req.query.phone});
                        return res.sendStatus(200);
                    }else{
                        console.log('/restore/confirm SQL injection!!!');
                        return res.status(303).send('Invalid params');
                    }
                }else{
                    return res.status(303).send('Invalid params');
                }
            }else{
                return res.status(303).send('Invalid params');
            }
        }catch (e) {
            console.log(e.message);
            return res.sendStatus(500);
        }
    });

    /*
    * Get user info
    * */

    router.get('/user', auth, (req,res) => {
        try{
            if(!req.user)return res.sendStatus(401);
            let user = req.user;
            delete user._doc.tokens;
            delete user._doc.password;
            return res.status(200).send(user);
        }catch (e) {
            console.log(e);
            return res.sendStatus(500);
        }
    });

    /*
    * Temp route for delete user
    * */

    router.get('/user/delete', async(req, res) => {
        let result = await UsersModel.deleteUser(req.query.phone.toString());
        if(typeof result !== 'string'){
            return res.sendStatus(200)
        }else{
            return res.status(404).send(result);
        }
    });

    /*
    * Patch user data
    * */

    router.patch('/user', auth, async (req, res) => {
        try {
            if(!req.user)return res.sendStatus(401);

            let obj = {};
            Object.keys(req.body).forEach(key => {
                if(key !=='phone'){
                    obj[key] = req.body[key];
                }
            });
            if(obj.password){
                if(obj.current){
                    if(await bcrypt.compare(obj.current, req.user.password)){
                        delete obj.current;
                    }else{
                        return res.sendStatus(303);
                    }
                }else{
                    return res.sendStatus(400);
                }
            }
            let result = await UsersModel.findOneAndUpdate({phone: req.user.phone}, obj, { runValidators: true }).catch(e=>{return false;});
            let user = await UsersModel.findOne({phone: req.user.phone});
            if(result){
                delete user._doc.password;
                delete user._doc.tokens;
                delete user._doc._id;
                delete user._doc.__v;
                return res.status(200).send(user);
            }else{
                return res.sendStatus(400);
            }
        }catch (e) {
            console.log(e.message);
            return res.sendStatus(500);
        }
    });

    /*
    * Patch user phone
    * */

    router.patch('/user/phone', auth, async (req, res) => {
        try {
            if(!req.user)return res.sendStatus(401);

            if(!validator.isMobilePhone(req.body.phone, 'uk-UA')){
                return res.sendStatus(400);
            }
            if(await UsersModel.findOne({phone: req.body.phone})){
                return res.sendStatus(303);
            }else{
                let code = makeId();
                await SMSController.send(req.user.phone, code);
                let sign = bcrypt.hashSync(process.env.PHONE_SECRET + code + req.body.phone + process.env.PHONE_SECRET, 10);
                return res.status(200).send({
                    'sign': sign,
                    'phone': req.body.phone
                });
            }
        }catch (e) {
            console.log(e.message);
            return res.sendStatus(500);
        }
    });

    /*
    * Verify patched phone
    * */

    router.post('/user/phone', auth, async (req, res) => {
        try {
            if(!req.user)return res.sendStatus(401);
            if(!req.body.phone || !req.body.code || !req.body.sign)return res.sendStatus(400);
            if(!bcrypt.compareSync((process.env.PHONE_SECRET + req.body.code + req.body.phone + process.env.PHONE_SECRET), req.body.sign))
                return res.sendStatus(303);
            if(await UsersModel.findOneAndUpdate({phone: req.user.phone},{phone: req.body.phone}, { runValidators: true }).catch(e=>{
                console.log(e.message);
                return false;
            })){
                let user = await UsersModel.findOne({phone: req.body.phone});
                if(user){
                    delete user._doc.tokens;
                    delete user._doc.password;
                    delete user._doc.__v;
                    return res.status(200).send(user);
                }else{
                    return res.sendStatus(302);
                }
            }else{
                return res.sendStatus(500);
            }
        }catch (e) {
            console.log(e.message);
            return res.sendStatus(500);
        }
    });

    /*
    * Get all user orders
    * */

    router.get('/user/orders', auth, async (req, res) => {
        try{
            if(!req.user)return res.sendStatus(401);

            let result = await OrdersModel.getAll(req.user.phone);

            for (let i = 0; i < result.length; i++) {
                delete result[i]._doc._id;
                delete result[i]._doc.phoneSign;
                for (let j = 0; j < result[i].body.length; j++) {
                    result[i].body[j]._doc.name = (await ServicesModel.findService(result[i].body[j].id)).main.name;
                    for (let k = 0; k < result[i].body[j].products.length; k++) {
                        result[i].body[j].products[k]._doc.name = (await ProductsModel.getProduct(result[i].body[j].products[k].id)).name;
                    }
                }
                result[i]._doc.qr = bcrypt.hashSync(process.env.ORDER_SECRET + result[i]._doc.id + process.env.ORDER_SECRET);
            }
            return res.status(200).send(result);
        }catch (e) {
            console.log(e.message);
            return res.sendStatus(500);
        }
    });

    /*
    * Route for uploading docs to server
    * */

    router.post('/user/docs', auth, async (req, res) => {
        try {
            if(!req.user)return res.sendStatus(401);
            if(req.user.bdayVerified) return res.status(400).send('You\'re already verified');
            if(!req.files) return res.sendStatus(400);
            if(Object.keys(req.files).length + req.user.docs.length > 2)return res.status(400).send('To much files');
            if(!req.files.face && !req.files.passport)return res.sendStatus(400);
            if(req.files.face && req.files.face.length >1)return res.sendStatus(400);
            if(req.files.passport && req.files.passport.length >1)return res.sendStatus(400);
            if(req.files.passport && req.user.docs.find(val => val.indexOf('u_') === 0 && val.indexOf('passport') !== -1))return res.sendStatus(400);
            if(req.files.face && req.user.docs.find(val => val.indexOf('u_') === 0 && val.indexOf('face') !== -1))return res.sendStatus(400);
            let arr = [];
            Object.keys(req.files).forEach(key => {
                if(req.files[key].mimetype === 'image/jpeg' ){
                    let time = Date.now();
                    req.files[key].mv(process.env.FILE_PATH + 'u_'+req.user.phone+'_'+ time +'_'+key+'.jpg', e =>{
                        if(e)return arr = undefined;
                    });
                    arr.push('u_'+req.user.phone+'_'+ time +'_'+key+'.jpg');
                }else if(req.files[key].mimetype === 'image/png'){
                    let time = Date.now();
                    req.files[key].mv(process.env.FILE_PATH + 'u_' + req.user.phone + '_' + time +'_' + key+'.png', e =>{
                        if(e)return arr = undefined;
                    });
                    arr.push('u_'+req.user.phone+'_'+ time +'_'+key+'.png');
                }else if(req.files[key].mimetype === 'image/webp'){
                    let time = Date.now();
                    req.files[key].mv(process.env.FILE_PATH + 'u_' + req.user.phone + '_' + time +'_' + key+'.webp', e =>{
                        if(e)return arr = undefined;
                    });
                    arr.push('u_'+req.user.phone+'_'+ time +'_'+key+'.webp');
                }else if(req.files[key].mimetype === 'image/gif'){
                    let time = Date.now();
                    req.files[key].mv(process.env.FILE_PATH + 'u_' + req.user.phone + '_' + time +'_' + key+'.gif', e =>{
                        if(e)return arr = undefined;
                    });
                    arr.push('u_'+req.user.phone+'_'+ time +'_'+key+'.gif');
                }else{
                    arr = false;
                    return false;
                }
            });
            if(arr === undefined)return res.sendStatus(500);
            if(arr === false)return res.sendStatus(303);
            for (let i = 0; i < arr.length; i++) {
                await UsersModel.findOneAndUpdate({phone: req.user.phone}, {$push: {docs: arr[i]}});
            }

            //Create notification for manager (validate user docs)
            if((await UsersModel.findOne({phone: req.user.phone})).docs.length === 2 && !req.user.bdayVerified){
                let notification = await NotificationsModel.createNew('validate_user', req.user);
                Object.keys(io.sockets.connected).forEach(key => {
                    if(io.sockets.connected[key].manager.user._doc.access.user_edit || io.sockets.connected[key].manager.user._doc.access.user_verify){
                        io.sockets.emit('notification', notification);
                    }
                });
            }

            return res.status(200).send({docs: arr});

        }catch(e){
            console.log('/user/docs', e.message);
            return res.sendStatus(500);
        }
    });

    /*
    * Route to get all images by user
    * */

    router.get('/user/docs', auth, async (req, res) => {
        try {
            if(!req.user)return res.sendStatus(401);

            if(req.query.file){
                if(req.query.file.indexOf(req.user.phone) === 2){
                    res.sendFile(process.env.FILE_PATH + req.query.file);
                }else{
                    return res.sendStatus(303);
                }
            }else{
                return res.status(200).send({files: req.user.docs});
            }
        } catch (e) {
            console.log(e.message);
            res.sendStatus(500);
        }
    });

    /*
    * Delete unverified docs
    * */

    router.delete('/user/docs', auth, async (req, res) => {
        try {
            if(!req.user)return res.sendStatus(401);
            if(req.user.bdayVerified) return res.status(400).send('You\'re already verified');
            if(req.query.file){
                try {
                    fs.accessSync(process.env.FILE_PATH + req.query.file);
                    fs.unlinkSync(process.env.FILE_PATH + req.query.file);
                    await UsersModel.findOneAndUpdate({phone: req.user.phone}, {$pull: {docs : req.query.file}}, {runValidators: true});
                    let user = await UsersModel.findOne({phone: req.user.phone});
                    await NotificationsModel.findOneAndDelete({type: 'validate_user', 'user.phone': req.user.phone});
                    return res.status(200).send(user.docs);
                }catch (e) {
                    return res.sendStatus(303);
                }
            }else{
                return res.sendStatus(400);
            }
        }catch (e) {
            console.log(e.message);
            return res.sendStatus(500);
        }
    });

    /*
    * Get users subscriptions
    * */

    router.get('/user/subscriptions', auth, async (req, res) => {
        try {
            if(!req.user)return res.sendStatus(401);
            if(req.query.id){
                let subscription = await SubscriptionsModel.getOne(req.query.id);
                return subscription ? res.status(200).send(subscription) : res.sendStatus(303);
            }
            let arr = await SubscriptionsModel.getSubscriptions(req.user.phone);
            if(!arr)return res.status(200).send({subscriptions : []});
            arr.forEach(value => {
                delete value._doc.phoneSign;
                value._doc.qr = bcrypt.hashSync(process.env.ORDER_SECRET + value.id + process.env.ORDER_SECRET);
            });
            return res.status(200).send({subscriptions : arr});
        }catch (e) {
            console.log(e.message);
            res.sendStatus(500);
        }
    });

    /*
    * Patch user relatives
    * */

    router.post('/user/relatives', auth, async (req, res) => {
        try {
            if(!req.user)return res.sendStatus(401);
            if(!validator.isMobilePhone(req.body.phone, 'uk-UA'))return res.sendStatus(400);
            if(!req.body.name || !req.body.sname || !req.body.bday) return res.sendStatus(400);
            if(!new Date(req.body.bday).valueOf())return res.sendStatus(400);
            if(req.body.phone !== req.user.phone && await UsersModel.findOne({phone: req.user.phone, relatives: {$elemMatch : {phone: req.body.phone}}}))return res.status(303).send('Phone already exists');
            if(req.files){
                if(!req.files.face && !req.files.passport)return res.sendStatus(400);
                if(req.files.face && req.files.face.length >1)return res.sendStatus(400);
                if(req.files.passport && req.files.passport.length >1)return res.sendStatus(400);
                if(req.files.passport && req.user.docs.find(val => val.indexOf('r_') === 0 && val.indexOf('passport') !== -1))return res.sendStatus(400);
                if(req.files.face && req.user.docs.find(val => val.indexOf('r_') === 0 && val.indexOf('face') !== -1))return res.sendStatus(400);
                if(Object.keys(req.files).length > 2)return res.status(400).send('To much files');

                let arr = [];
                Object.keys(req.files).forEach(key => {
                    if(req.files[key].mimetype === 'image/jpeg' ){
                        let time = Date.now();
                        req.files[key].mv(process.env.FILE_PATH + 'r_' + req.user.phone+'_' + req.body.phone + '_' + time + '_' + key + '.jpg', e =>{
                            return e ? res.sendStatus(500) : true;
                        });
                        arr.push('r_'+req.user.phone+'_' + req.body.phone + '_' + time +'_'+key+'.jpg');
                    }else if(req.files[key].mimetype === 'image/png'){
                        let time = Date.now();
                        req.files[key].mv(process.env.FILE_PATH + 'r_' + req.user.phone + '_' + req.body.phone + '_' + time +'_' + key+'.png', e =>{
                            return e ? res.sendStatus(500) : true;
                        });
                        arr.push('r_'+req.user.phone+'_' + req.body.phone + '_' + time +'_'+key+'.png');
                    }else if(req.files[key].mimetype === 'image/webp'){
                        let time = Date.now();
                        req.files[key].mv(process.env.FILE_PATH + 'r_' + req.user.phone + '_' + req.query.phone + '_' + time +'_' + key+'.webp', e =>{
                            if(e)return arr = undefined;
                        });
                        arr.push('r_'+req.user.phone+'_' + req.query.phone + '_' + time +'_'+key+'.webp');
                    }else if(req.files[key].mimetype === 'image/gif'){
                        let time = Date.now();
                        req.files[key].mv(process.env.FILE_PATH + 'r_' + req.user.phone + '_' + req.query.phone + '_' + time +'_' + key+'.gif', e =>{
                            if(e)return arr = undefined;
                        });
                        arr.push('r_'+req.user.phone+'_' + req.query.phone + '_' + time +'_'+key+'.gif');
                    }else{
                        return res.sendStatus(303);
                    }
                });
                await UsersModel.findOneAndUpdate(
                    {phone: req.user.phone},
                    {$push: {relatives: {
                                name: req.body.name,
                                sname: req.body.sname,
                                phone: req.body.phone,
                                bday: req.body.bday,
                                docs: arr
                            }
                        }
                    },
                    {runValidators: true}
                );
                let user = await UsersModel.findOne({phone: req.user.phone});
                delete user._doc.tokens;
                delete user._doc.password;

                //Create notification for manager (validate relative docs)
                if(arr.length === 2){
                    let relative = user.relatives.find(val => val.docs.find(val => val === arr[0]));
                    let notification = await NotificationsModel.createNew('validate_relative', req.user, relative);
                    Object.keys(io.sockets.connected).forEach(key => {
                        if(io.sockets.connected[key].manager.user._doc.access.user_edit || io.sockets.connected[key].manager.user._doc.access.user_verify){
                            io.sockets.emit('notification', notification);
                        }
                    });
                }

                return res.status(200).send(user);
            }else{
                await UsersModel.findOneAndUpdate(
                    {phone: req.user.phone},
                    {$push: {relatives: {
                                name: req.body.name,
                                sname: req.body.sname,
                                phone: req.body.phone,
                                bday: req.body.bday
                            }
                        }
                    },
                    {runValidators: true}
                );
                let user = await UsersModel.findOne({phone: req.user.phone});
                delete user._doc.tokens;
                delete user._doc.password;
                return res.status(200).send(user);
            }
        }catch (e) {
            console.log(e.message);
            return res.sendStatus(500);
        }
    });

    /*
    * Get all users relatives
    * */

    router.get('/user/relatives', auth, async (req, res) => {
        try {
            if(!req.user)return res.sendStatus(401);
            if(req.query.verified){
                let arr = req.user.relatives.filter(val => val.verified === (req.query.verified === 'true'));
                return res.status(200).send(arr);
            }else{
                let result = {
                    relatives: req.user.relatives
                };
                return res.status(200).send(result);
            }
        }catch (e) {
            console.log(e.message);
            return res.sendStatus(500);
        }
    });

    /*
    * Change user relatives information
    * */

    router.patch('/user/relatives', auth, async (req, res) => {
        try {
            if(!req.user)return res.sendStatus(401);
            if(!req.query.id) return res.sendStatus(400);
            let relative = req.user.relatives.find(val => {if(val._id.toString() === req.query.id)return true;});
            if(relative.verified) return res.status(303).send('Already verified');

            if(req.body.phone && !validator.isMobilePhone(req.body.phone, 'uk-UA'))return res.sendStatus(400);
            if(req.body.bday && !new Date(req.body.bday).valueOf())return res.sendStatus(400);

            let user = await UsersModel.findOne({phone: req.user.phone});
            if(req.body.phone && req.body.phone !== req.user.phone && user.relatives.find(val => val.phone === req.body.phone && val._id.toString() !== req.query.id)) return res.sendStatus(303);

            user.relatives.find(val => {
                if(val._id.toString() === req.query.id){
                    Object.keys(req.body).forEach(key => val[key] = req.body[key]);
                }
            });
            user = await user.save();
            return res.status(200).send(user ? user.relatives : user);
        }catch (e) {
            console.log(e.message);
            return res.sendStatus(500);
        }
    });

    /*
    * Delete user relative
    * */

    router.delete('/user/relatives', auth, async (req, res) => {
        try {
            if(!req.user)return res.sendStatus(401);
            if(!req.query.id) return res.sendStatus(400);

            let relative = req.user.relatives.find(val => val._id.toString() === req.query.id);
            if(!relative) return res.sendStatus(303);
            relative.docs.forEach(val => {
                try {
                    fs.accessSync(process.env.FILE_PATH + val);
                    fs.unlinkSync(process.env.FILE_PATH + val);
                }catch (e) {
                    console.log(e.message);
                }
            });
            let result = await UsersModel.findOneAndUpdate({phone: req.user.phone, relatives: {$elemMatch: {_id: req.query.id}}},
                {$pull : {relatives: {_id: req.query.id}}},
                {runValidators: false});
            if(!result)return res.sendStatus(303);
            let user = await UsersModel.findOne({phone: req.user.phone});
            await NotificationsModel.findOneAndDelete({type: 'validate_relative', 'user.phone': req.user.phone, 'relative.id' : req.query.id});
            return res.status(200).send(user.relatives);
        } catch (e) {
            console.log(e.message);
            return res.sendStatus(500);
        }
    });

    /*
    * Upload relatives documents
    * */

    router.post('/user/relatives/docs', auth, async (req, res) => {
        try {
            if(!req.user)return res.sendStatus(401);
            if(!req.query.id || !req.files)return res.sendStatus(400);
            let relative = req.user.relatives.find(val => val._id.toString() === req.query.id);
            if(!relative) return res.sendStatus(303);
            if(relative.verified) return res.status(303).send('Already verified');

            let arr = [];
            if(Object.keys(req.files).length + relative.docs.length > 2) return res.status(303).send('Too much files');
            if(!req.files.face && !req.files.passport)return res.sendStatus(400);
            if(req.files.face && req.files.face.length >1)return res.sendStatus(400);
            if(req.files.passport && req.files.passport.length >1)return res.sendStatus(400);
            if(req.files.passport && req.user.relatives.find(val => val._id.toString() === req.query.id ? val.docs.find(val => val.indexOf('passport') !== -1) : false))return res.sendStatus(400);
            if(req.files.face && req.user.relatives.find(val => val._id.toString() === req.query.id ? val.docs.find(val => val.indexOf('face') !== -1) : false))return res.sendStatus(400);
            Object.keys(req.files).forEach(key => {
                if(req.files[key].mimetype === 'image/jpeg' ){
                    let time = Date.now();
                    req.files[key].mv(process.env.FILE_PATH + 'r_' + req.user.phone+'_' + relative.phone + '_' + time + '_' + key + '.jpg', e =>{
                        return e ? res.sendStatus(500) : true;
                    });
                    arr.push('r_'+req.user.phone+'_' + relative.phone + '_' + time +'_'+key+'.jpg');
                }else if(req.files[key].mimetype === 'image/png'){
                    let time = Date.now();
                    req.files[key].mv(process.env.FILE_PATH + 'r_' + req.user.phone + '_' + relative.phone + '_' + time +'_' + key+'.png', e =>{
                        return e ? res.sendStatus(500) : true;
                    });
                    arr.push('r_'+req.user.phone+'_' + relative.phone + '_' + time +'_'+key+'.png');
                }else if(req.files[key].mimetype === 'image/webp'){
                    let time = Date.now();
                    req.files[key].mv(process.env.FILE_PATH + 'r_' + req.user.phone + '_' + relative.phone + '_' + time +'_' + key+'.webp', e =>{
                        if(e)return arr = undefined;
                    });
                    arr.push('r_'+req.user.phone+'_' + relative.phone + '_' + time +'_'+key+'.webp');
                }else if(req.files[key].mimetype === 'image/gif'){
                    let time = Date.now();
                    req.files[key].mv(process.env.FILE_PATH + 'r_' + req.user.phone + '_' + relative.phone + '_' + time +'_' + key+'.gif', e =>{
                        if(e)return arr = undefined;
                    });
                    arr.push('r_'+req.user.phone+'_' + relative.phone + '_' + time +'_'+key+'.gif');
                }else{
                    return res.sendStatus(303);
                }
            });
            for (let i = 0; i < arr.length; i++) {
                await UsersModel.findOneAndUpdate({phone: req.user.phone, relatives: {$elemMatch: {_id: req.query.id}}}, {$push: {'relatives.$.docs' : arr[i]}}, {runValidators: true});
            }

            // Create notification for verify relative
            let user = await UsersModel.findOne({phone: req.user.phone});
            relative = user.relatives.find(val => val._id.toString() === req.query.id);
            if(relative.docs.length === 2){
                let notification = await NotificationsModel.createNew('validate_relative', req.user, relative);
                Object.keys(io.sockets.connected).forEach(key => {
                    if(io.sockets.connected[key].manager.user._doc.access.user_edit || io.sockets.connected[key].manager.user._doc.access.user_verify){
                        io.sockets.emit('notification', notification);
                    }
                });
            }

            return res.status(200).send({docs: arr});

        }catch (e) {
            console.log('/user/relatives/docs', e.message);
            return res.sendStatus(500);
        }
    });

    /*
    * Get docs of relative
    * */

    router.get('/user/relatives/docs', auth, async(req, res) => {
        try {
            if(!req.user)return res.sendStatus(401);
            if(!req.query.file)return res.sendStatus(400);
            return res.sendFile(process.env.FILE_PATH + req.query.file);
        }catch (e) {
            console.log('/user/relatives/docs', e.message);
            return res.sendStatus(500);
        }
    });

    /*
    * Delete docs from relatves
    * */

    router.delete('/user/relatives/docs', auth, async(req, res) => {
        try {
            if(!req.user)return res.sendStatus(401);
            if(!req.query.id || !req.query.file)return res.sendStatus(400);
            let relative = req.user.relatives.find(val => val._id.toString() === req.query.id);
            if(relative.verified) return res.status(303).send('Already verified');

            try {
                fs.accessSync(process.env.FILE_PATH + req.query.file);
                fs.unlinkSync(process.env.FILE_PATH + req.query.file);
                await UsersModel.findOneAndUpdate({phone: req.user.phone, relatives: {$elemMatch: {_id: req.query.id}}}, {$pull: {'relatives.$.docs' : req.query.file}}, {runValidators: true});
                let user = await UsersModel.findOne({phone: req.user.phone});
                await NotificationsModel.findOneAndDelete({type: 'validate_relative', 'user.phone': req.user.phone, 'relative.id' : req.query.id});
                return res.status(200).send(user.relatives.find(val => val._id.toString() === req.query.id).docs);
            }catch (e) {
                return res.sendStatus(303);
            }
        }catch (e) {
            console.log(e.message);
            return res.sendStatus(500);
        }
    });

    /*
    * Refund order
    * */

    router.post('/user/refund', auth, async (req, res) => {
        try {
            if(!req.user)return res.sendStatus(401);
            if(!req.body.order) return res.sendStatus(400);
            if(req.body.pos && typeof req.body.pos !== 'object') return res.sendStatus(400);

            let order = await OrdersModel.findOne({id : req.body.order});
            if(!order) return res.sendStatus(303);
            if(order.type === 'payment'){
                let liqpay = new LiqPay(process.env.LPAY_PUBLIC, process.env.LPAY_PRIVATE, process.env.LPAY_PUBLIC, process.env.LPAY_PRIVATE);
                if(req.body.pos && req.body.pos.length > 0){
                    let total = 0;
                    for (let i = 0; i < req.body.pos.length; i++) {
                        if(!order.body[req.body.pos[i]]) return res.sendStatus(303);
                        if(order.body[req.body.pos[i]].start.getTime() - Date.now() < 24 * 60 * 60 * 1000) return res.sendStatus(303);
                        if(order.body[req.body.pos[i]].status !=='ordered') return res.sendStatus(303);
                        let refund = 0;
                        refund += order.body[req.body.pos[i]].cost;
                        order.body[req.body.pos[i]].products.forEach(val => {
                            if(Array.isArray(val.amount)){
                                val.amount.forEach(val => refund += val.cost * val.amount)
                            }else{
                                refund += val.cost * val.amount
                            }
                        });
                        order.body[req.body.pos[i]].status = 'processing';
                        order.body[req.body.pos[i]].refund = refund;
                        order.body[i].refundM = 0.8;
                        total += refund;
                    }
                    total = total * 0.8;
                    await liqpay.api("request", {
                        "action"   : "refund",
                        "version"  : "3",
                        "order_id" : order.id,
                        "amount"   : total,
                        'server_url'    :  'https://apisky.zapleo.com/form/complete?type=refund'
                    });
                    return res.status(200).send({order : await order.save(), refund : total});
                }else{
                    let total = 0;
                    for (let i = 0; i < order.body.length; i++) {
                        if(!order.body[i]) return res.sendStatus(303);
                        if(order.body[i].start.getTime() - Date.now() < 24 * 60 * 60 * 1000) return res.sendStatus(303);
                        if(order.body[i].status !=='ordered') return res.sendStatus(303);
                        let refund = 0;
                        refund += order.body[i].cost;
                        order.body[i].products.forEach(val => {
                            if(Array.isArray(val.amount)){
                                val.amount.forEach(val => refund += val.cost * val.amount)
                            }else{
                                refund += val.cost * val.amount
                            }
                        });
                        order.body[i].status = 'processing';
                        order.body[i].refund = refund;
                        order.body[i].refundM = 0.8;
                        total += refund;
                    }
                    total = total *  0.8;
                    await liqpay.api("request", {
                        "action"   : "refund",
                        "version"  : "3",
                        "order_id" : order.id,
                        "amount"   : total,
                        'server_url'    :  'https://apisky.zapleo.com/form/complete?type=refund'
                    });
                    return res.status(200).send({order : await order.save(), refund : total});
                }
            }else if(order.type === 'subscription'){
                if(!order.body[0]) return res.sendStatus(303);
                if(order.body[0].start.getTime() - Date.now() < 24 * 60 * 60 * 1000) return res.sendStatus(303);
                if(order.body[0].status !=='ordered') return res.sendStatus(303);
                await SubscriptionsModel.findOneAndUpdate({id: order.subscription.id}, {$inc: {'visitsAmount' : 1}}, {runValidators: true});
                order.body[0].status = 'refunded';
                return res.status(200).send({order: await order.save()});
            }
        }catch (e) {
            console.log('/user/refund', e.message);
            return res.sendStatus(500);
        }
    });

    return router;
};