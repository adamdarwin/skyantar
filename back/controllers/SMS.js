const request = require('request-promise-native');

module.exports = class SMS {
    static async send (phone, mes){
        try {
            return await request('https://smsukraine.com.ua/api/http.php?version=http&login=' + process.env.SMS_LOG +'&password=' + process.env.SMS_PASS + '&key=' + process.env.SMS_KEY + '&from=' + process.env.SMS_FROM + '&to='+phone+'&message=' + mes + '&command=send');
        }catch (e) {
            console.log(e.message);
            return false
        }
    }
};