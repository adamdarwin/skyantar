const jwt = require('jsonwebtoken');

const ManagersModel = require('.././models/Manager');

const auth = async (req, res, next) =>{
    try{
        const token = req.header('Authorization').replace('Bearer ', '');
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const user = await ManagersModel.findOne({_id: decoded.id, 'tokens.token': token});
        if(!user){
            req.user = false;
            return next();
        }
        if(user.status === 'banned'){
            if(req.originalUrl === '/manager/logout' ){
                req.user = user;
                req.token = token;
                return next();
            }
            req.user = false;
            return next();
        }
        if(user.new === true && req.originalUrl !== '/manager' && req.method !== 'GET' && req.method !== 'PATCH'){
            req.user = false;
            return next();
        }
        req.token = token;
        req.user = user;
        next();
    }catch (e) {
        req.user = false;
        next();
    }
};

module.exports = auth;