const bcrypt = require('bcryptjs');

module.exports = async function (req, res, next) {
    try {
        if(!req.headers.login)return res.sendStatus(401);
        if(!req.headers.authorization)return res.sendStatus(401);
        if(!bcrypt.compareSync((process.env.API_C_SECRET + req.headers.login + process.env.API_C_SECRET), req.headers.authorization)) return res.sendStatus(401);
        return next();
    }catch (e) {
        console.log('bookerAuth', e.message);
        return res.sendStatus(401);
    }
};