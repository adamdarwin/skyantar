const jwt = require('jsonwebtoken');

const UsersModel = require('.././models/Users');

const auth = async (req, res, next) =>{
  try{
    const token = req.header('Authorization').replace('Bearer ', '');
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const user = await UsersModel.findOne({_id: decoded.id, 'tokens.token': token});

    if(!user){
      req.user = false;
      return next();
    }
    req.token = token;
    req.user = user;

    next();
  }catch (e) {
    req.user = false;
    next();
  }
};

module.exports = auth;