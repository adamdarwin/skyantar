const request = require('request-promise-native');

const OrdersModel = require('./../models/Orders');
const UsersModel = require('./../models/Users');

module.exports = Booker = class {
    static async sendOrder (id = null, _id = null, bday = false){
        let total = 0;
        switch (bday) {
            case true : {
                if(!id  || ! _id) return false;
                let order = await OrdersModel.findOne({id});
                if(!order) return false;
                let pos = order.body.find(val => val._id.toString() === _id);
                if(!pos)return false;
                let query = {
                    type: 'sale',
                    data: '',
                    IDdoc: '',
                    Customer: [],
                    family: [],
                    product: []
                };
                query.data = new Date(+order.id).toISOString();
                query.IDdoc = order.id;
                let user = await UsersModel.findOne({phone: order.phone});
                query.Customer.push({
                    IDWebKL: order.phone,
                    Phone: order.phone,
                    F: user ? user.sname : 'empty',
                    N: user ? user.name : 'empty',
                    P: 'empty',
                    Birth: user ? user.bday : 'empty'
                });

                if (user && user.relatives[0]) {
                    user.relatives.forEach(val => query.family.push({
                        F: val.sname,
                        N: val.name,
                        Birth: val.bday,
                    }))
                } else {
                    query.family.push({
                        F: 'empty',
                        N: 'empty',
                        Birth: 'empty',
                    })
                }
                total += pos.cost * pos.people;
                query.product.push({
                    IDWebTov: pos.id,
                    quantity: pos.people,
                    price: pos.cost * pos.people,
                    pay: order.type === 'cash' ? 1 : 2
                });
                pos.products.forEach(product => {
                    if (Array.isArray(product.amount)) {
                        product.amount.forEach(amount => {
                            total += amount.cost;
                            query.product.push({
                                IDWebTov: product.id + '_' + amount.id,
                                quantity: amount.amount,
                                price: amount.cost * amount.amount,
                                pay: order.type === 'cash' ? 1 : 2
                            });
                        })
                    } else {
                        total += product.cost;
                        query.product.push({
                            IDWebTov: product.id,
                            quantity: product.amount,
                            price: product.cost * product.amount,
                            pay: order.type === 'cash' ? 1 : 2
                        });
                    }
                });
                pos.sentToBooker = true;
                await order.save();

                let options = {
                    url: process.env.API_C,
                    headers: {
                        'cache-control': 'no-cache',
                        'Content-Type': 'application/json',
                        'accept-encoding': 'gzip, deflate',
                        Authorization: 'Basic '+ new Buffer(process.env.API_C_LOGIN + ':' + process.env.API_C_PASSWORD).toString('base64')
                    },
                    body: query,
                    json: true
                };
                let req = {};
                await request.post(options).on('response', resp => req = resp)
                    .catch(e => {
                        console.log(e.message);
                    });

                if(req.statusCode.toString()[0] === '4'){
                    console.log('1C server error');
                    this.pingApi(options, 10000);
                } else if(req.statusCode.toString()[0] === '5'){
                    console.log('1C client error!');
                } else if(req.statusCode.toString()[0] === '3'){
                    console.log('1C request error!')
                }
                console.log('Order with birthday: ', total);
                return order;
            }
            case false : {
                let orders = id ? await OrdersModel.find({id}) : await OrdersModel.find({
                    $or: [
                        {type: 'payment', body: {$elemMatch: {status: 'used', bday: false, sentToBooker: false}}},
                        {type: 'payment', body: {$elemMatch: {status: 'refunded', bday: false, sentToBooker: false}}}
                    ]
                });
                for (let i = 0; i < orders.length; i++) {
                    let query = {
                        type: 'sale',
                        data: '',
                        IDdoc: '',
                        Customer: [],
                        family: [],
                        product: []
                    };
                    query.data = new Date(+orders[i].id).toISOString();
                    query.IDdoc = orders[i].id;
                    let user = await UsersModel.findOne({phone: orders[i].phone});
                    query.Customer.push({
                        IDWebKL: orders[i].phone,
                        Phone: orders[i].phone,
                        F: user ? user.sname : 'empty',
                        N: user ? user.name : 'empty',
                        P: 'empty',
                        Birth: user ? user.bday : 'empty'
                    });

                    if (user && user.relatives[0]) {
                        user.relatives.forEach(val => query.family.push({
                            F: val.sname,
                            N: val.name,
                            Birth: val.bday,
                        }))
                    } else {
                        query.family.push({
                            F: 'empty',
                            N: 'empty',
                            Birth: 'empty',
                        })
                    }
                    for (let j = 0; j < orders[i].body.length; j++) {
                        if(orders[i].body[j].status !== 'used' && orders[i].body[j].status !== 'refunded' && !id) continue;
                        let cost = orders[i].body[j].cost * (orders[i].type === 'cash' ? 1 : (orders[i].body[j].status === 'used' ? 1 : orders[i].body[j].refundM));
                        total += cost;
                        query.product.push({
                            IDWebTov: orders[i].body[j].id,
                            quantity: orders[i].body[j].people,
                            price: cost,
                            pay: orders[i].type === 'cash' ? 1 : 2
                        });
                        orders[i].body[j].products.forEach(product => {
                            if (Array.isArray(product.amount)) {
                                product.amount.forEach(amount => {
                                    cost = amount.cost * (orders[i].type === 'cash' ? 1 : (orders[i].body[j].status === 'used' ? 1 : orders[i].body[j].refundM));
                                    total += cost;
                                    query.product.push({
                                        IDWebTov: product.id + '_' + amount.id,
                                        quantity: amount.amount,
                                        price: cost,
                                        pay: orders[i].type === 'cash' ? 1 : 2
                                    });
                                })
                            } else {
                                let cost = product.cost * (orders[i].type === 'cash' ? 1 : (orders[i].body[j].status === 'used' ? 1 : orders[i].body[j].refundM));
                                total += cost;
                                query.product.push({
                                    IDWebTov: product.id,
                                    quantity: product.amount,
                                    price: cost,
                                    pay: orders[i].type === 'cash' ? 1 : 2
                                });
                            }
                        });
                        orders[i].body[j].sentToBooker = true;
                    }
                    await orders[i].save();
                    let options = {
                        url: process.env.API_C,
                        headers: {
                            'cache-control': 'no-cache',
                            'Content-Type': 'application/json',
                            'accept-encoding': 'gzip, deflate',
                            Authorization: 'Basic '+ new Buffer(process.env.API_C_LOGIN + ':' + process.env.API_C_PASSWORD).toString('base64')
                        },
                        body: query,
                        json: true
                    };
                    let req = {};
                    await request.post(options).on('response', resp => req = resp)
                        .catch(e => {
                            console.log(e.message);
                        });

                    if(req.statusCode.toString()[0] === '4'){
                        console.log('1C server error');
                        this.pingApi(options, 10000);
                    } else if(req.statusCode.toString()[0] === '5'){
                        console.log('1C client error!');
                    } else if(req.statusCode.toString()[0] === '3'){
                        console.log('1C request error!')
                    }
                }
                console.log('Today you earned ', total);
                return true;
            }
            default : {
                return false;
            }
        }
    }
    static async pingApi (options, timeout){
        let req;
        while(req !== 200){
            await new Promise(resolve => setTimeout(resolve, timeout));
            await request.post(options).on('response', resp => req = resp.statusCode)
                .catch(e => {
                    console.log(e.message);
                });
            console.log(req);
        }
        return true;
    };
};