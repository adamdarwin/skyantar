
const SubscriptionModel = require('./../models/Subscriptions');
const ServiceModel = require('./../models/Services');

class Subscriptions{

    /*
    * Decrease subscription visits amount
    *
    * @param    {object}    order   Order information
    *
    * @return   {object}    Services and products which subscription provides
    * */

    static async useSubscription(order, user = null){
        try {
            let subscriptionInfo = await SubscriptionModel.findOne({id : order.info.subscription});
            let service = await ServiceModel.findService(order.body.id);
            let subscription = {
                name: subscriptionInfo.name,
                id: order.info.subscription,
                service: order.body.id,
                start: order.body.start,
                end: order.body.end,
                relative: subscriptionInfo.relative.id ? {
                    id : subscriptionInfo.relative.id,
                    name : subscriptionInfo.relative.name,
                    sname : subscriptionInfo.relative.sname,
                    phone : subscriptionInfo.relative.phone
                } : null
            };
            if(service.main.type === 'reserve'){
                subscription.amount = 1;
                let reserve = await SubscriptionModel.decreaseOn(order.info.subscription, 1);
                if(reserve === false)return false;
                subscription.appliedProducts = [];
                subscriptionInfo.appliedProducts.forEach(val => {
                    subscription.appliedProducts.push(val);
                });
                return subscription;
            }else if(service.main.type === 'visit'){
                order.body.people=1;
                subscription.amount = order.body.people;
                let reserve = await SubscriptionModel.decreaseOn(order.info.subscription, order.body.people);
                if(reserve === false)return false;
                subscription.appliedProducts = [];
                subscriptionInfo.appliedProducts.forEach(val => {
                    subscription.appliedProducts.push({id: val.id, amount: val.amount * order.body.people});
                });
                return subscription
            }
        }catch (e) {
            console.log(e.message);
            return false;
        }
    }
}

module.exports = Subscriptions;