const request = require('request-promise-native');

const BookerController = require('./../controllers/Booker');

const OrdersModel = require('./../models/Orders');

module.exports = Daily = class {
    static async dailyCleaner() {
        let interval = new Date(new Date().setDate(new Date().getDate() + 1)).setUTCHours(0, 0, 0, 0) - Date.now();
        setTimeout(async () => {
            try {
                interval = 24 * 60 * 60 * 1000;
                let arr = await OrdersModel.find({body: {$elemMatch: {status: 'active'}}});
                for (let i = 0; i < arr.length; i++) {
                    for (let j = 0; j < arr[i].body.length; j++) {
                        if (arr[i].body[j].status === 'active') arr[i].body[j].status = 'used';
                    }
                    await arr[i].save();
                }
                console.log('Set as used ' + arr.length + ' order(s).');
                await BookerController.sendOrder();
                return this.dailyCleaner(interval);
            } catch (e) {
                console.log('dailyCleaner', e.message);
                return false;
            }
        }, interval);
    };
};