const ServicesModel = require('./../models/Services');
const ProductsModel = require('./../models/Products');
const OrdersModel = require('./../models/Orders');
const HolidaysModel = require('./../models/holidays');

const SubscriptionModel = require('./../models/Subscriptions');

const SubscriptionController = require('./../controllers/Subscriptions');

class Orders{

    /*
    * Check is service or product with selected params are available
    * @param    {string}  type      Type of checking information
    * @param    {string}  id        Id
    * @param    {number}  people    Amount of people
    * @param    {string}  option    Option of product
    * @param    {number}  amount    Amount of product
    * @param    {string}  start     Start time
    * @param    {string}  end       End time
    *
    * @returns  {bool}  Is available
    * */

    static async checkProperty({type, id, people = null, option = null, amount = null, start, end}){
        switch (type) {
            case 'service':{
                let service = await ServicesModel.findService(id);
                return await OrdersModel.checkServicesByTime(
                    {
                        service: service,
                        people: people,
                        start: start,
                        end: end
                    });
            }
            case 'product':{
                let result = 0, max = 0;
                if(option){
                    result = await OrdersModel.checkProductsByTime({
                        id: id.toString(),
                        start: start.toString(),
                        end: end.toString(),
                        option: option
                    });
                    if(result === false){
                        return false;
                    }
                    max = await ProductsModel.getMaxAvailable(id.toString(), option);
                }else{
                    result = await OrdersModel.checkProductsByTime({
                        id: id.toString(),
                        start: start.toString(),
                        end: end.toString()
                    });
                    if(result === false){
                        return false;
                    }
                    max = await ProductsModel.getMaxAvailable(id.toString());
                }
                result += + amount;
                return result <= max;
            }
            case 'date': {
                console.log(id, start);
                let result = await OrdersModel.getBusyDate(id, start);
                break;
            }
            case 'time': {
                let result = await OrdersModel.getBusy(id, start, people);
                break;
            }
            default: {
                return false;
            }
        }
        return false;
    }

    /*
    * Check is order available to create
    *
    * @param    {object}     order   Order array
    * @param    {object}    user    Authorized user model
    *
    * @return   {bool}  Isa order available
    * */

    static async checkOrder(order, user = null){
        try{
            if(order.info.type === 'subscription' && user && order.info.subscription){
                if(new Date(order.body.start) < new Date())return false;
                if(new Date(order.body.start) >= new Date(order.body.end))return false;
                if(new Date(order.body.end) > new Date(new Date(order.body.start).setDate(new Date().getDate()+1)).setHours(0,0,0,0))return false;
                let service = await ServicesModel.findService(order.body.id, user);
                if(!service) return false;
                order.body.people = 1;
                if(new Date(order.body.start) - new Date() > service.main.preordered)return false;// Check is preordering is not so far
                if(!await this.checkSubscription(order))return false;
                if(!await this.checkProperty({
                    type: 'service',
                    id: order.body.id,
                    people: service.main.type === 'reserve' ? 1 : order.body.people,
                    start: order.body.start,
                    end: order.body.end
                })) return false;
                return true;
            }else if(order.info.type === 'payment' || order.info.type === 'cash'){
                if(!order.body[0]) return false;
                for (let i = 0; i < order.body.length; i++) {
                    if(new Date(order.body[i].start) < new Date() && order.body[i].id !== '05')return false;
                    if(new Date(order.body[i].start) >= new Date(order.body[i].end))return false;
                    if(new Date(order.body[i].end) > new Date(new Date(order.body.start).setDate(new Date().getDate()+1)).setHours(0,0,0,0))return false;
                    let service = await ServicesModel.findService(order.body[i].id, user);
                    if(!service) return false;
                    if(new Date(order.body[i].start) - new Date() > service.main.preordered)return false;// Check is preordering is not so far
                    if(service.main.type === 'visit' && !order.body[i].people)return false;
                    if(!await this.checkProperty({
                        type: 'service',
                        id: order.body[i].id,
                        people: service.main.type === 'reserve' ? 1 : order.body[i].people,
                        start: order.body[i].start,
                        end: order.body[i].end
                    }))return false;
                    if(order.body[i].products){
                        for (let j = 0; j < order.body[i].products.length; j++) {
                            if(!service.main.products.find(val => {return val===order.body[i].products[j].id}))return false;
                            let product = await ProductsModel.findOne({id : order.body[i].products[j].id});
                            if(!product) return false;
                            if(!product.status) return false;
                            if(typeof order.body[i].products[j].amount === 'number'){
                                if(!await this.checkProperty({
                                    type : 'product',
                                    id: order.body[i].products[j].id,
                                    amount: order.body[i].products[j].amount,
                                    start: order.body[i].start,
                                    end: order.body[i].end
                                }))return false;
                            }else if(typeof order.body[i].products[j].amount === 'object'){
                                for (let k = 0; k < order.body[i].products[j].amount.length; k++) {
                                    if(!await this.checkProperty({
                                        type : 'product',
                                        id: order.body[i].products[j].id,
                                        option: order.body[i].products[j].amount[k].option,
                                        amount: order.body[i].products[j].amount[k].amount,
                                        start: order.body[i].start,
                                        end: order.body[i].end
                                    }))return false;
                                }
                            }
                        }
                    }
                }
                return true;
            }

        }catch (e) {
            console.log('checkOrder', e.message);
            return false;
        }
    }

    /*
    * Check is subscription available with selected params
    *
    * @param    {object}    order       Order with subscription
    *
    * @return   {bool}  Is subscription available for this type
    * */

    static async checkSubscription(order){
        try {
            if(!order.info.subscription) return false;
            let subscription = await SubscriptionModel.getOne(order.info.subscription);
            if(!subscription)return false;
            let service = await ServicesModel.findService(order.body.id);
            if(!service)return false;
            if(!subscription.services.find(value => value.id === order.body.id))return false;
            if(service.main.type === 'reserve' && subscription.visitsAmount < 1)return false;
            if(order.body.people && order.body.people < 0)return false;
            if(service.main.type === 'visit' && subscription.visitsAmount < order.body.people)return false;
            for (let i = 0; i < subscription.appliedProducts.length; i++) {
                if(typeof subscription.appliedProducts.amount === 'number'){
                    if(!await this.checkProperty({
                        type : 'product',
                        id: subscription.appliedProducts[i].id,
                        amount: subscription.appliedProducts[i].amount,
                    }))return false;
                }else if(typeof subscription.appliedProducts.amount === 'object'){
                    for (let j = 0; j < subscription.appliedProducts.amount.length; j++) {
                        if(!await this.checkProperty({
                            type : 'product',
                            id: subscription.appliedProducts[i].id,
                            option: subscription.appliedProducts[i].amount[j].option,
                            amount: subscription.appliedProducts[i].amount[j].amount,
                        }))return false;
                    }
                }
            }
            return true;
        }catch (e) {
            console.log(e.message);
            return false;
        }
    }

    /*
    * Build order from given array
    *
    * @param    {array} order   Order array
    *
    * @return   {bool}  Was order object created
    * */

    static async buildOrder(order, user = null){
        try{
            let result = {};
            result.phone = order.info.phone;
            result.email = order.info.email ? order.info.email : null;
            if(order.info.type === 'subscription'){
                result.subscription = await SubscriptionController.useSubscription(order, user);
                if(result.subscription === false)return false;
                let checkout = await OrdersModel.setOrder({type: 'subscription', body: result});
                return checkout ? checkout : false;
            }else if(order.info.type === 'payment' || order.info.type === 'cash'){
                result.total = 0;
                result.body = [];
                for (let i = 0; i < order.body.length; i++) {
                    let service = await ServicesModel.findOne({id: order.body[i].id});
                    if(service.type === 'reserve')order.body[i].people = 1;
                    let cost = await ServicesModel.getCost(order.body[i].id, order.body[i].start, order.body[i].end, order.body[i].people, user);
                    if(!cost) return false;
                    let bday = cost.bday;
                    cost = cost.cost;
                    let products = [];
                    if(order.body[i].products){
                        products = await Promise.all(order.body[i].products.map(async function(val){
                            let product = await ProductsModel.findOne({id : val.id});
                            if(typeof val.amount === 'number'){
                                val.cost = product.cost * val.amount;
                                result.total += val.cost;
                                return val;
                            }else{
                                let id = val.id;
                                val.amount = await Promise.all(val.amount.map( async function (val){
                                    let option = product.amount.find(amount => amount.option === val.option);
                                    val.cost = option.cost * val.amount;
                                    val.id = option.id;
                                    result.total += val.cost;
                                    return val;
                                }));
                                return val;
                            }
                        }));
                    }
                    result.total += cost;
                    result.body.push({
                        id: order.body[i].id,
                        start : order.body[i].start,
                        end : order.body[i].end,
                        people : order.body[i].people,
                        products : products,
                        cost : cost,
                        bday: bday
                    });
                }
                return result;
            }
        }catch (e) {
            console.log('buildOrder', e.message);
            return false;
        }
    }

    /*
    * Returns all busy days of month for service
    *
    * @param    {string}    id      Service id
    * @param    {string}    date    Month date
    *
    * @return   {array} Array of busy days
    * */

    static async checkMonth(id, date = null){
        try {
            date = date ? new Date(date) : new Date();
            let result = [];
            let monthDays = new Date(date.getFullYear(), date.getMonth()+1, 0).getDate();
            for (let i = 1; i <= monthDays; i++) {
                let current = new Date(new Date(date.setDate(i)).setUTCHours(0,0,0));
                if(!await OrdersModel.getBusyDate(id, current)){
                    result.push(new Date(date.setDate(i)).toISOString());
                }
            }
            return result;
        }catch (e) {
            console.log('checkMonth', e.message);
            return false;
        }
    }

}
module.exports = Orders;