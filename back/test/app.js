const express = require('express'),
    mongoose = require('mongoose'),
    helmet = require('helmet'),
    session = require('cookie-session'),
    cookieParser = require('cookie-parser'),
    fs = require('fs'),
    http = require('http'),
    https = require('https'),
    cors = require('cors'),
    fileUpload = require('express-fileupload'),
    socket = require('socket.io'),
    jwt = require('jsonwebtoken');

require('dotenv').config({ path: process.env.TEST_ENV_PATH });

const ManagersModel = require('./models/Manager');

mongoose.connect('mongodb://'+process.env.DB_USER+':'+process.env.DB_PASSWORD+'@localhost:27017/'+process.env.DB_NAME+'', {useNewUrlParser: true})
    .then(console.log('Mongo connected TEST'))
    .catch(err => {if(err)console.log(err.message)});

let app = express();

app.use(helmet());
app.use(cors());
app.use(function(req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Methods", "*");
    res.setHeader("Access-Control-Allow-Headers", "*");
    next();
});

app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.use(session({
    name: 'session',
    keys: ['key1', 'key2'],
    cookie: {
        secure: true,
        httpOnly: true,
        domain: 'localhost:4001'
    }
}));
app.use(cookieParser());
app.use(fileUpload({
    limits: { fileSize: 20 * 1024 * 1024},
    abortOnLimit: true
}));

//order routes
app.all('/form', require('./routes/form'));
app.all('/form/submit', require('./routes/form'));
app.all('/form/submit/check', require('./routes/form'));
app.all('/form/check', require('./routes/form'));
app.all('/form/complete', require('./routes/form'));

//subscription orders
app.all('/subscriptions', require('./routes/subscriptions'));
app.all('/subscriptions/buy', require('./routes/subscriptions'));
app.all('/subscriptions/add-relative', require('./routes/subscriptions'));

const PORT = process.env.port || 4001;

let privateKey = fs.readFileSync( './config/api.KEY' );
let certificate = fs.readFileSync( './config/api.CRT' );

const DailyController = require('./controllers/Daily.js');

if(PORT === '443'){
    let server = https.createServer({
        key: privateKey,
        cert: certificate
    }, app).listen(PORT, () => {
        console.log('TEST server up on ', PORT,' port');
        DailyController.dailyCleaner();

        let io = socket(server);

        let userRouter = require('./routes/user')(io);
        let managerRouter = require('./routes/manager')(io);
        let bookerRouter = require('./routes/booker');
        let terminalRouter = require('./routes/terminal')();

        // User requests
        app.all('/register', );
        app.all('/register/verify', userRouter);
        app.all('/login', userRouter);
        app.all('/logout', userRouter);
        app.all('/restore', userRouter);
        app.all('/restore/confirm', userRouter);
        app.all('/user', userRouter);
        app.all('/user/phone', userRouter);
        app.all('/user/delete', userRouter);
        app.all('/user/orders', userRouter);
        app.all('/user/subscriptions', userRouter);
        app.all('/user/relatives', userRouter);
        app.all('/', userRouter);
        app.all('/user/docs', userRouter);
        app.all('/user/relatives/docs', userRouter);
        app.all('/user/refund', userRouter);

        // Manager requests
        app.all('/manager/create-manager', managerRouter);
        app.all('/manager/login', managerRouter);
        app.all('/manager/logout', managerRouter);
        app.all('/manager', managerRouter);
        app.all('/manager/phone', managerRouter);
        app.all('/manager/access', managerRouter);
        app.all('/manager/restore', managerRouter);
        app.all('/manager/restore/confirm', managerRouter);
        app.all('/manager/edit/profile', managerRouter);
        app.all('/manager/edit/access', managerRouter);
        app.all('/manager/block', managerRouter);

        app.all('/manager/get/managers', managerRouter);
        app.all('/manager/get/notifications', managerRouter);
        app.all('/manager/get/users', managerRouter);
        app.all('/manager/get/users/docs', managerRouter);
        app.all('/manager/get/users/subscriptions', managerRouter);

        app.all('/manager/get/users/orders', managerRouter);
        app.all('/manager/get/structures', managerRouter);

        app.all('/manager/get/structure', managerRouter);
        app.all('/manager/create/structure', managerRouter);
        app.all('/manager/edit/structure', managerRouter);
        app.all('/manager/delete/structure', managerRouter);

        app.all('/manager/get/service', managerRouter);
        app.all('/manager/create/service', managerRouter);
        app.all('/manager/edit/service', managerRouter);
        app.all('/manager/delete/service', managerRouter);

        app.all('/manager/get/product', managerRouter);
        app.all('/manager/create/product', managerRouter);
        app.all('/manager/edit/product', managerRouter);
        app.all('/manager/delete/product', managerRouter);

        app.all('/manager/get/subscription', managerRouter);
        app.all('/manager/create/subscription', managerRouter);
        app.all('/manager/edit/subscription', managerRouter);
        app.all('/manager/delete/subscription', managerRouter);

        app.all('/manager/verify/user', managerRouter);
        app.all('/manager/verify/relative', managerRouter);

        app.all('/manager/seed/users', managerRouter);

        app.all('/manager/qr/check', managerRouter);
        app.all('/manager/qr/subscription/new', managerRouter);

        app.all('/manager/clear/subscription', managerRouter);

        app.all('/manager/user/visit/code', managerRouter);

        app.all('/manager/orders/start', managerRouter);
        app.all('/manager/refund', managerRouter);

        app.all('/manager/terminal', managerRouter);

        // Booker requests
        app.all('/booker', bookerRouter);
        app.all('/booker/edit', bookerRouter);
        app.all('/booker/edit/subscriptions', bookerRouter);
        app.all('/booker/edit/services', bookerRouter);
        app.all('/booker/edit/products', bookerRouter);

        app.all('/booker/subscriptions', bookerRouter);
        app.all('/booker/services', bookerRouter);
        app.all('/booker/products', bookerRouter);

        app.all('/terminal', terminalRouter);
        app.all('/terminal/login', terminalRouter);
        app.all('/terminal/logout', terminalRouter);
        app.all('/terminal/sms', terminalRouter);

        io.use(async function (socket, next) {
            try {
                const token = socket.handshake.query.Authorization.replace('Bearer ', '');
                const decoded = jwt.verify(token, process.env.JWT_SECRET);
                const user = await ManagersModel.findOne({_id: decoded.id, 'tokens.token': token});
                if(!user)return false;
                if(Object.keys(socket.server.sockets.connected).find(key => socket.server.sockets.connected[key].manager.token === token))return false;
                let manager = {};
                manager.token = token;
                manager.user = user;
                socket.manager = manager;
                return next();
            }catch (e) {
                return false;
            }
        });

        io.on('connection', socket => {
            console.log('Test Socket up from: ', socket.handshake.headers['x-forwarded-for'] || socket.request.connection.remoteAddress);
        });
    });
}else{
    let server = http.Server(app).listen(PORT, () => {
        console.log('TEST Server up on ', PORT,' port');
        DailyController.dailyCleaner();
    }
    );
    let io = socket(server);

    let userRouter = require('./routes/user')(io);
    let managerRouter = require('./routes/manager')(io);
    let bookerRouter = require('./routes/booker');
    let terminalRouter = require('./routes/terminal')();

    app.all('/register', userRouter);
    app.all('/register/verify', userRouter);
    app.all('/login', userRouter);
    app.all('/logout', userRouter);
    app.all('/restore', userRouter);
    app.all('/restore/confirm', userRouter);
    app.all('/user', userRouter);
    app.all('/user/phone', userRouter);
    app.all('/user/delete', userRouter);
    app.all('/user/orders', userRouter);
    app.all('/user/subscriptions', userRouter);
    app.all('/user/relatives', userRouter);
    app.all('/', userRouter);
    app.all('/user/docs', userRouter);
    app.all('/user/relatives/docs', userRouter);
    app.all('/user/refund', userRouter);

    app.all('/manager/create-manager', managerRouter);
    app.all('/manager/login', managerRouter);
    app.all('/manager/logout', managerRouter);
    app.all('/manager', managerRouter);
    app.all('/manager/phone', managerRouter);
    app.all('/manager/access', managerRouter);
    app.all('/manager/restore', managerRouter);
    app.all('/manager/restore/confirm', managerRouter);
    app.all('/manager/edit/profile', managerRouter);
    app.all('/manager/edit/access', managerRouter);
    app.all('/manager/block', managerRouter);

    app.all('/manager/get/managers', managerRouter);
    app.all('/manager/get/notifications', managerRouter);
    app.all('/manager/get/users', managerRouter);
    app.all('/manager/get/users/docs', managerRouter);
    app.all('/manager/get/users/subscriptions', managerRouter);

    app.all('/manager/get/orders', managerRouter);

    app.all('/manager/get/structure', managerRouter);
    app.all('/manager/create/structure', managerRouter);
    app.all('/manager/edit/structure', managerRouter);
    app.all('/manager/delete/structure', managerRouter);

    app.all('/manager/get/service', managerRouter);
    app.all('/manager/create/service', managerRouter);
    app.all('/manager/edit/service', managerRouter);
    app.all('/manager/delete/service', managerRouter);

    app.all('/manager/get/product', managerRouter);
    app.all('/manager/create/product', managerRouter);
    app.all('/manager/edit/product', managerRouter);
    app.all('/manager/delete/product', managerRouter);

    app.all('/manager/get/subscription', managerRouter);
    app.all('/manager/create/subscription', managerRouter);
    app.all('/manager/edit/subscription', managerRouter);
    app.all('/manager/delete/subscription', managerRouter);

    app.all('/manager/verify/user', managerRouter);
    app.all('/manager/verify/relative', managerRouter);

    app.all('/manager/seed/users', managerRouter);

    app.all('/manager/qr/check', managerRouter);
    app.all('/manager/qr/subscription/new', managerRouter);

    app.all('/manager/clear/subscription', managerRouter);

    app.all('/manager/user/visit/code', managerRouter);

    app.all('/manager/orders/start', managerRouter);
    app.all('/manager/refund', managerRouter);

    app.all('/manager/terminal', managerRouter);

    app.all('/booker', bookerRouter);
    app.all('/booker/edit', bookerRouter);
    app.all('/booker/edit/subscriptions', bookerRouter);
    app.all('/booker/edit/services', bookerRouter);
    app.all('/booker/edit/products', bookerRouter);

    app.all('/booker/subscriptions', bookerRouter);
    app.all('/booker/services', bookerRouter);
    app.all('/booker/products', bookerRouter);

    app.all('/terminal', terminalRouter);
    app.all('/terminal/login', terminalRouter);
    app.all('/terminal/logout', terminalRouter);
    app.all('/terminal/sms', terminalRouter);


    io.use(async function (socket, next) {
        try {
            const token = socket.handshake.query.Authorization.replace('Bearer ', '');
            const decoded = jwt.verify(token, process.env.JWT_SECRET);
            const user = await ManagersModel.findOne({_id: decoded.id, 'tokens.token': token});
            if(!user)return false;
            if(Object.keys(socket.server.sockets.connected).find(key => socket.server.sockets.connected[key].manager.token === token))return false;
            let manager = {};
            manager.token = token;
            manager.user = user;
            socket.manager = manager;
            return next();
        }catch (e) {
            return false;
        }
    });

    io.on('connection', socket => {
        console.log('Test Socket up from: ', socket.handshake.headers['x-forwarded-for'] || socket.request.connection.remoteAddress);
    });
}