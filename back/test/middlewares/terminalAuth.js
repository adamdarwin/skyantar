const jwt = require('jsonwebtoken');

const TerminalModel = require('.././models/Terminal');

const auth = async (req, res, next = null) =>{
    try{
        const token = req.header('Authorization').replace('Bearer ', '');
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const user = await TerminalModel.findOne({_id: decoded.id, 'tokens.token': token});
        if(!user){
            req.terminal = false;
            return next ? next() : false;
        }
        req.token = token;
        req.terminal = user;
        return next ? next() : req.terminal;
    }catch (e) {
        req.terminal = false;
        return next ? next() : false;
    }
};

module.exports = auth;