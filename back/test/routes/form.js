const express = require('express'),
    router = express.Router(),
    LiqPay = require('./../liqpay/liqpay'),
    base64 = require('js-base64').Base64,
    bcrypt = require('bcryptjs'),
    qrcode = require('qrcode'),
    jwt = require('jsonwebtoken'),
    request = require('request-promise-native');

const ServicesModel = require('.././models/Services');

const OrdersController = require('.././controllers/Orders');
const BookersController = require('.././controllers/Booker');
const SMSController = require('./../controllers/SMS');

const OrdersModel = require('.././models/Orders');
const OrdersTempModel = require('.././models/OrdersTemp');
const HolidaysModel = require('.././models/holidays');
const SubscriptionModel = require('../models/Subscriptions');
const UsersModel = require('../models/Users');

const auth = require('.././middlewares/auth');
const terminalAuth = require('.././middlewares/terminalAuth');

/*
* Get service information for form view
* */
router.get('/form', auth, async (req, res) => {
    try {
        if(req.query.id){
            let form = await ServicesModel.findService(req.query.id);
            if(!form) return res.sendStatus(400);
            if(req.query.time && !new Date(req.query.time).valueOf())return res.sendStatus(400);
            if(req.query.date && !new Date(req.query.date).valueOf())return res.sendStatus(400);
            form.services = await ServicesModel.getAll();
            if(req.query.time){
                if(req.user){
                    if(req.user.bday.getMonth() === new Date(req.query.time).getMonth() && req.user.bday.getDate() === new Date(req.query.time).getDate() && req.user.bdayVerified){
                        form.main.cost = form.main.cost.map((item )=> { return item * form.main._doc.bdayDiscount; });
                        form.main._doc.bday = true;
                    }else{
                        form.main._doc.bday = false;
                    }
                }
                form.busyTimes = await OrdersModel.getBusyTime(req.query.id, req.query.time, req.query.people ? req.query.people : null);
                form.holidays = await HolidaysModel.getForMonth(req.query.time ? req.query.time : null);
            }else{
                form.busyDates = await OrdersController.checkMonth(req.query.id, req.query.date ? req.query.date : null);
                form.holidays = await HolidaysModel.getForMonth(req.query.date ? req.query.date : null);
                form.busyTimes = await OrdersModel.getBusyTime(req.query.id, new Date().toISOString(), req.query.people ? req.query.people : null);
            }
            return form ? res.send(form).status(200) : res.sendStatus(303);
        }else{
            return res.status(409).send('Invalid parameters');
        }
    }catch (e) {
        console.log('/form', e.message);
        return res.sendStatus(500);
    }
});

/*
* Check form params
* */
router.get('/form/check', async (req, res) => {
    try{
        let query = req.query;
        let check = await OrdersController.checkProperty({
            type: query.type,
            id: query.id,
            people: query.people?query.people:false,
            option: query.option?query.option:false,
            amount: query.amount?query.amount:false,
            start: query.start,
            end: query.end?query.end:false
        });
        if(check){
            res.sendStatus(200);
        }else{
            res.sendStatus(303);
        }
    }catch (e) {
        console.log(e.message);
        res.sendStatus(500);
    }
});

/*
* Generates verification code
*
* @return   {string}    Verification code
* */

const makeId = () => {
    let text = "";
    let possible = "0123456789";

    for (let i = 0; i < 4; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};

/*
* Submit order from user
* */
router.post('/form/submit', auth, async (req, res) => {
    try {
        if(!req.body.info || !req.body.body)return res.sendStatus(400);
        if(!req.body.info.phone) return res.sendStatus(400);
        if(req.body.info.type === 'subscription' && req.body.info.subscription){
            if(!req.user)return res.sendStatus(401);
            if(typeof req.body.body.length === 'number')return res.sendStatus(400);
            if(await OrdersController.checkOrder(req.body, req.user ? req.user : null)){
                let order = await OrdersController.buildOrder(req.body, req.user ? req.user : null);
                if(!order) return res.sendStatus(303);
                order.qr = bcrypt.hashSync(process.env.SUBSCRIPTION_SECRET + order.id + process.env.SUBSCRIPTION_SECRET);
                return res.status(200).send(order);
            }else{
                return res.sendStatus(303);
            }
        }else if(req.body.info.type === 'payment'){
            if(await OrdersController.checkOrder(req.body, req.user ? req.user : null)){
                let order = await OrdersController.buildOrder(req.body, req.user ? req.user : null);
                if(!order) return res.sendStatus(400);
                let orderTemp = await OrdersTempModel.setOrder(order);
                if(!orderTemp) return res.sendStatus(303);
                if(order){
                    let lpay = new LiqPay(process.env.LPAY_PUBLIC, process.env.LPAY_PRIVATE);
                    order.button = lpay.cnb_form({
                        'action'        :  'pay',
                        'amount'        :  order.total,
                        'currency'      :  'UAH',
                        'description'   :  'Order '+orderTemp.id,
                        'order_id'      :  orderTemp.id,
                        'version'       :  '3',
                        'sandbox'       :  '1',
                        'server_url'    :  'https://testapisky.zapleo.com/form/complete?type=service'
                    });
                    return res.status(200).send(order);
                }else{
                    return res.sendStatus(303);
                }
            }else{
                return res.sendStatus(303);
            }
        }else if(req.body.info.type === 'cash'){
            let terminal = await terminalAuth(req, res);
            if(!terminal) return res.sendStatus(403);
            if(terminal.status === 'disabled'){
                terminal.log.push({url : req.originalUrl, method: req.method, status : 403 , errorMessage : 'Terminal disabled', body : req.body});
                await terminal.save();
                return res.sendStatus(403);
            }
            const user = req.body.user === true ? await UsersModel.findOne({phone : req.body.info.phone}) : false;
            if(await OrdersController.checkOrder(req.body, user)){
                let order = await OrdersController.buildOrder(req.body, user);
                if(!order) {
                    terminal.log.push({url : req.originalUrl, method: req.method, status : 303 , errorMessage : 'Order not found', body : req.body});
                    await terminal.save();
                    return res.sendStatus(303);
                }
                let orderTemp = await OrdersTempModel.setOrder(order);
                if(!orderTemp) {
                    terminal.log.push({url : req.originalUrl, method: req.method, status : 303 , errorMessage : 'Can\'t set order', body : req.body});
                    await terminal.save();
                    return res.sendStatus(303);
                }
                order.id = orderTemp.id;
                if(!user) {
                    let code = makeId();
                    orderTemp.phoneSign = code;
                    await orderTemp.save();
                    await SMSController.send(req.body.info.phone, code);
                    order.user = false;
                }else{
                    order.user = true;
                }
                terminal.log.push({url : req.originalUrl, method: req.method, status : 200 , body : req.body});
                await terminal.save();

                return res.status(200).send(order);
            }else{
                return res.sendStatus(303);
            }
        }else{
            return res.sendStatus(400);
        }
    }catch (e) {
        console.log('/form/submit', e.message);
        return res.sendStatus(500);
    }
});

router.get('/form/submit/check', async (req, res) => {
   try{
       let result = await OrdersModel.checkSubmit(req.query.id);
       if(result){
           res.status(200).send(result);
       }else{
           res.sendStatus(303);
       }
   } catch (e) {
       console.log(e.message);
       res.sendStatus(500);
   }
});

/*
* Receive payment data from LiqPay
* */

router.post('/form/complete', async function(req, res){
    try {
        if(req.query.type === 'service'){
            let lpay = new LiqPay(process.env.LPAY_PUBLIC, process.env.LPAY_PRIVATE);
            if(lpay.str_to_sign(process.env.LPAY_PRIVATE + req.body.data + process.env.LPAY_PRIVATE) === req.body.signature){
                let data = JSON.parse(base64.decode(req.body.data));
                if(data.status === 'sandbox' || data.status === 'success' || data.status === 'wait_accept'){
                    await OrdersModel.setOrder({type: 'payment',id: data.order_id});
                    return res.status(200);
                }else if(data.status === 'reversed'){
                    console.log(data);
                    let order = await OrdersModel.findOne({id: data.order_id});
                    if(!order) return res.sendStatus(404);
                    order.body.forEach(val => val.status === 'processing' ? val.status = 'refunded' : false);
                    await order.save();
                    return res.sendStatus(200);
                }else{
                    return res.sendStatus(303);
                }
            }
        }else if(req.query.type === 'subscription'){
            let lpay = new LiqPay(process.env.LPAY_PUBLIC, process.env.LPAY_PRIVATE);
            if(lpay.str_to_sign(process.env.LPAY_PRIVATE + req.body.data + process.env.LPAY_PRIVATE) === req.body.signature){
                let data = JSON.parse(base64.decode(req.body.data));
                if(data.status === 'sandbox' || data.status === 'success' || data.status === 'wait_accept'){
                    await SubscriptionModel.setSubscription(data.order_id);
                    return res.sendStatus(200);
                }else{
                    return res.sendStatus(303);
                }
            }else{
                let subscription = await SubscriptionModel.setSubscription(req.query.id);
                console.log(subscription);
            }
        }else if(req.query.type === 'cash'){
            let terminal = await terminalAuth(req, res);
            if(!terminal)return res.sendStatus(403);
            if(terminal.status.disabled){
                terminal.log.push({url : req.originalUrl, method: req.method, status : 403 , errorMessage : 'Terminal disabled', body : req.body});
                await terminal.save();
                return res.sendStatus(403);
            }
            if(!req.query.id){
                terminal.log.push({url : req.originalUrl, method: req.method, status : 400 , errorMessage : 'id undefined', body : req.body});
                await terminal.save();
                return res.sendStatus(400);
            }

             let orderTemp = await OrdersTempModel.findOne({id: req.query.id});
             if(!orderTemp){
                 terminal.log.push({url : req.originalUrl, method: req.method, status : 303 , errorMessage : 'Order not found', body : req.body});
                 await terminal.save();
                 return res.sendStatus(303);
             }
             if(orderTemp.phoneSign && !req.query.sign){
                 terminal.log.push({url : req.originalUrl, method: req.method, status : 303 , errorMessage : 'sign required', body : req.body});
                 await terminal.save();
                 return res.sendStatus(303);
             }
             if(orderTemp.phoneSign && orderTemp.phoneSign !== req.query.sign){
                 terminal.log.push({url : req.originalUrl, method: req.method, status : 303 , errorMessage : 'sign incorrect', body : req.body});
                 await terminal.save();
                 return res.sendStatus(303);
             }

            let order = await OrdersModel.setOrder({type: 'cash',id: req.query.id});
            if(!order){
                terminal.log.push({url : req.originalUrl, method: req.method, status : 400 , errorMessage : 'Can\'t set the order', body : req.body});
                await terminal.save();
                return res.sendStatus(400);
            }
            order = await OrdersModel.findOne({_id: order});
            let qr = bcrypt.hashSync(process.env.ORDER_SECRET + order.id + process.env.ORDER_SECRET);
            order._doc.qr = qr;
            delete order._doc._id;
            delete order._doc.__v;
            terminal.log.push({url : req.originalUrl, method: req.method, status : 200, body : req.body});
            await terminal.save();
            await BookersController.sendOrder(order.id);
            return res.status(200).send(order);
        }else {
            return res.sendStatus(400);
        }
    }catch (e) {
        console.log(e.message);
        return res.sendStatus(500);
    }

});

router.get('/qr', async (req, res) => {
    let qr = await qrcode.toDataURL('I am a pony!');
    res.send(qr).status(200);
});

module.exports = router;