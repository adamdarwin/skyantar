const express = require('express'),
    router = express.Router(),
    validator = require('validator'),
    bcrypt = require('bcryptjs'),
    request = require('request-promise-native'),
    LiqPay = require('./../liqpay/liqpay');

const ManagerModel = require('./../models/Manager');
const SubscriptionTypesModel = require('./../models/SubscriprtionTypes');
const StructureModel = require('./../models/Structure');
const UsersModel = require('./../models/Users');
const NotificationsModel = require('./../models/Notifications');
const SubscriptionsModel = require('./../models/Subscriptions');
const OrdersModel = require('./../models/Orders');
const ServiceModel = require('./../models/Services');
const ProductsModel = require('./../models/Products');
const TerminalModel = require('./../models/Terminal');


const SMSController = require('./../controllers/SMS');
const OrdersController = require('./../controllers/Orders');
const BookerController = require('./../controllers/Booker');

const managerAuth = require('.././middlewares/managerAuth');


module.exports = function (io) {
   router.post('/manager/create-manager', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.manager_edit) return res.sendStatus(403);

         if (await ManagerModel.findOne({phone: req.body.phone})) return res.sendStatus(303);
         if (await ManagerModel.findOne({email: req.body.email})) return res.sendStatus(303);
         if (!req.body.name || !req.body.sname || !req.body.lname || !req.body.email || !req.body.phone) return res.sendStatus(400);
         if (!validator.isMobilePhone(req.body.phone, 'uk-UA')) return res.sendStatus(303);
         let manager = await ManagerModel.createManager(req.body);
         if (!manager) return res.sendStatus(303);
         delete manager._doc.tokens;
         return res.status(200).send(manager);
      } catch (e) {
         console.log(e.message);
         return res.sendStatus(500);
      }
   });

   router.post('/manager/login', async (req, res) => {
      try {
         const manager = await ManagerModel.findByCredentials({login: req.body.login, password: req.body.password});
         if (!manager) {
            res.status(404).send('Incorrect');
            return false;
         }
         const token = await manager.generateJWT();
         delete manager._doc.tokens;
         delete manager._doc.password;
         let key = false;
         do{
            key = Object.keys(io.sockets.connected).find(key => io.sockets.connected[key].manager.user.login === manager.login);
            if(key) delete io.sockets.connected[key];
         } while(key);
         res.status(200).send({manager, token});
      } catch (e) {
         console.log('/manager/login', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Manager logout
   * */

   router.post('/manager/logout', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         req.user.tokens = req.user.tokens.filter((token) => {
            return token.token !== req.token;
         });
         await req.user.save();
         let tag = false;
         do{
            tag = Object.keys(io.sockets.connected).find(key => io.sockets.connected[key].manager.user.login === req.user.login);
            if(tag) io.sockets.connected[tag].disconnect();
         } while(tag);
         return res.status(200).send('Logged out');
      } catch (e) {
         console.log('/manager/logout', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Get manager model
   * */

   router.get('/manager', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         let user = req.user;
         delete user._doc.tokens;
         delete user._doc.password;
         res.status(200).send(user);
      } catch (e) {
         console.log(e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Change manager info
   * */

   router.patch('/manager', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         let obj = {};
         if (Object.keys(req.body).length < 1) return res.sendStatus(303);
         if (req.user.new && (Object.keys(req.body).length > 1 || !req.body.password)) return res.sendStatus(303);
         Object.keys(req.body).forEach(key => obj[key] = req.body[key]);
         if (!req.body.password) return res.sendStatus(400);
         if (req.user.new) {
            obj.new = false;
            let result = await ManagerModel.findOneAndUpdate({login: req.user.login}, obj, {runValidators: true}).catch(e => {
               return false;
            });
            if (result) {
               let user = await ManagerModel.findOne({phone: req.user.phone});
               delete user._doc.password;
               delete user._doc.tokens;
               delete user._doc._id;
               delete user._doc.__v;
               return res.status(200).send(user);
            } else {
               return res.sendStatus(400);
            }
         } else {
            if (obj.current) {
               if (await bcrypt.compare(obj.current, req.user.password)) {
                  let result = await ManagerModel.findOneAndUpdate({login: req.user.login}, obj, {runValidators: true}).catch(e => {
                     return false;
                  });
                  let user = await ManagerModel.findOne({phone: req.user.phone});
                  delete user._doc.password;
                  delete user._doc.tokens;
                  delete user._doc._id;
                  delete user._doc.__v;
                  res.status(200).send(user);
               } else {
                  return res.sendStatus(303);
               }
            } else {
               return res.sendStatus(400);
            }
         }

      } catch (e) {
         console.log(e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Generates verification code
   *
   * @return   {string}    Verification code
   * */

   const makeId = () => {
      let text = "";
      let possible = "0123456789";

      for (let i = 0; i < 4; i++)
         text += possible.charAt(Math.floor(Math.random() * possible.length));

      return text;
   };

   /*
   * Restore password
   * */

   router.get('/manager/restore', async (req, res) => {
      try {
         if (!req.query.login) return res.status(303).send('Invalid params');
         let user = await ManagerModel.findOne({login: req.query.login});
         if (!user) return res.status(303).send('Invalid params');
         let code = makeId();
         let sign = bcrypt.hashSync(process.env.PHONE_SECRET + code + req.query.login + process.env.PHONE_SECRET);
         await SMSController.send(user.phone, code);
         return res.status(200).send({login: req.query.login, sign: sign, code: code});
      } catch (e) {
         console.log('/manager/restore', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Check is verification code is accepted
   * */

   router.get('/manager/restore/confirm', async (req, res) => {
      try {
         if (req.query.login && req.query.sign && req.query.code) {
            if (bcrypt.compareSync(process.env.PHONE_SECRET + req.query.code + req.query.login + process.env.PHONE_SECRET, req.query.sign)) {
               return res.sendStatus(200);
            } else {
               return res.status(303).send('Invalid params');
            }
         } else {
            return res.status(303).send('Invalid params');
         }
      } catch (e) {
         console.log(e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Change user password
   * */

   router.post('/manager/restore/confirm', async (req, res) => {
      try {
         if (req.query.login && req.query.sign && req.body.password) {
            if (bcrypt.compareSync((process.env.PHONE_SECRET + req.query.code + req.query.login + process.env.PHONE_SECRET), req.query.sign)) {
               if (await ManagerModel.findOneAndUpdate({login: req.query.login}, {password: req.body.password}, {runValidators: true})) {
                  let user = await ManagerModel.findOne({login: req.query.login});
                  return res.sendStatus(200);
               } else {
                  console.log('/manager/restore/confirm SQL injection!!!');
                  return res.status(303).send('Invalid params');
               }
            } else {
               return res.status(303).send('Invalid params');
            }
         } else {
            return res.status(303).send('Invalid params');
         }
      } catch (e) {
         console.log(e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Change user phone
   * */

   router.patch('/manager/phone', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);

         if (!req.body.phone) return res.sendStatus(400);
         if (!validator.isMobilePhone(req.body.phone, 'uk-UA')) {
            return res.sendStatus(400);
         }
         if (await ManagerModel.findOne({phone: req.body.phone})) {
            return res.sendStatus(303);
         } else {
            let code = makeId();
            await SMSController.send(req.user.phone, code);
            let sign = bcrypt.hashSync(process.env.PHONE_SECRET + code + req.body.phone + process.env.PHONE_SECRET, 10);
            return res.status(200).send({
               'sign': sign,
               'phone': req.body.phone
            });
         }
      } catch (e) {
         console.log(e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Verify patched phone
   * */

   router.post('/manager/phone', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.body.phone || !req.body.code || !req.body.sign) return res.sendStatus(400);
         if (!bcrypt.compareSync((process.env.PHONE_SECRET + req.body.code + req.body.phone + process.env.PHONE_SECRET), req.body.sign))
            return res.sendStatus(303);
         if (await ManagerModel.findOneAndUpdate({phone: req.user.phone}, {phone: req.body.phone}, {runValidators: true}).catch(e => {
            console.log(e.message);
            return false;
         })) {
            let user = await ManagerModel.findOne({phone: req.body.phone});
            if (user) {
               delete user._doc.tokens;
               delete user._doc.password;
               delete user._doc.__v;
               return res.status(200).send(user);
            } else {
               return res.sendStatus(302);
            }
         } else {
            return res.sendStatus(500);
         }
      } catch (e) {
         console.log(e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Change email
   * */

// TODO: make email confirmation

   /*
   * Edit manager info
   * */

   router.post('/manager/edit/profile', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.manager_edit) return res.sendStatus(403);

         let user = await ManagerModel.findOne({login: req.query.login});
         if (!user) return res.sendStatus(303);
         let obj = {};
         Object.keys(req.body).forEach(key => {
            if (key !== 'password' && key !== 'status' && key !== 'new' && key !== 'login') obj[key] = req.body[key];
         });
         if (!await ManagerModel.findOneAndUpdate({login: req.query.login}, obj, {runValidators: true})) return res.sendStatus(400);
         user = await ManagerModel.findOne({login: req.query.login});
         delete user._doc.password;
         delete user._doc.tokens;
         delete user._doc._id;
         delete user._doc.__v;
         return res.status(200).send(user);
      } catch (e) {
         console.log(e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Edit manager access rights
   * */

   router.post('/manager/edit/access', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.manager_edit) return res.sendStatus(403);

         let user = await ManagerModel.findOne({login: req.query.login});
         if (!user) return res.sendStatus(303);
         let obj = {};
         obj.access = {};
         Object.keys(req.body).forEach(key => obj.access[key] = req.body[key]);
         await ManagerModel.findOneAndUpdate({login: req.query.login}, obj, {runValidators: true});
         user = await ManagerModel.findOne({login: req.query.login});
         user.password = undefined;
         user.tokens = undefined;
         return res.status(200).send(user);
      } catch (e) {
         console.log('/manager/edit/access', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Block manager
   * */

   router.post('/manager/block', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.manager_edit) return res.sendStatus(403);

         if (!req.query.login || !req.query.block) return res.sendStatus(400);

         let manager = await ManagerModel.findOne({login: req.query.login});
         if (!manager) return res.sendStatus(303);
         if(req.query.block === 'true'){
            if (manager.status === 'banned') return res.sendStatus(303);
            manager.status = 'banned';
         }else if(req.query.block === 'false'){
            if (manager.status === 'active' ||manager.status === 'new') return res.sendStatus(303);
            manager.status = 'active';
         }else{ return res.sendStatus(400);}
         return res.status(200).send(await manager.save());
      } catch (e) {
         console.log('/manager/block', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Get all managers
   * */

   router.get('/manager/get/managers', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);

         let arr = [];
         Object.keys(io.sockets.connected).forEach(key => {
            return arr.push(io.sockets.connected[key].manager.user.login);
         });
         let managerSocket = Object.keys(io.sockets.connected).find(val => io.sockets.connected[val].manager.user.login === req.user.login);
         if (managerSocket) io.sockets.connected[managerSocket].emit('online', arr); // emit all online managers login

         if (!req.query.status || req.query.status === 'online') {
            let managerArr = [];
            Object.keys(io.sockets.connected).forEach(key => {
               return managerArr.push(io.sockets.connected[key].manager.user);
            });
            return res.status(200).send({array: managerArr});
         }

         let obj = {};
         obj.$and = [];

         if (req.query.status === 'active') {
            obj.$and.push({status: 'active'});
         }
         if (req.query.status === 'offline') {
            obj.$and.push({status: {$ne: 'banned'}});
         }
         if (req.query.status === 'banned') {
            obj.$and.push({status: 'banned'});
         }

         if (!obj.$and[0]) delete obj.$and;

         let all = await ManagerModel.find(obj);
         if (req.query.status === 'offline') all = all.filter(val => !arr.find(key => key === val.login));
         all.forEach(val => {
            delete val.tokens;
            delete val.password;
         });
         return res.status(200).send({array: all});
      } catch (e) {
         console.log('/manager/get/managers', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Get managers actions
   * */

// TODO: do that

   /*
   * Get all users
   * */

   router.get('/manager/get/users', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.user_edit && !req.user.access.user_verify && !req.user.access.subscription_check) return res.sendStatus(403);

         if (req.query.user) return res.status(200).send(await UsersModel.findOne({phone: req.query.user}));

         let obj = {};
         obj.$and = [];

         if(req.query.phone) obj.$and.push({phone: req.query.phone});
         if (req.query.relatives) obj.$and.push({relatives: {$elemMatch: {verified: req.query.relatives}}});
         if (req.query.bdayVerified) obj.$and.push({bdayVerified: req.query.bdayVerified});
         if (req.query.docs) req.query.docs === 'true' ? obj.$and.push({docs: {$size: 2}}) : obj.$and.push({$nor: [{docs: {$size: 2}}]});
         if (req.query.docsRelative) req.query.docsRelative === 'true' ? obj.$and.push({relatives: {$elemMatch: {docs: {$size: 2}}}}) : obj.$and.push({$nor: [{relatives: {$elemMatch: {docs: {$size: 2}}}}]});

         if (!obj.$and[0]) delete obj.$and;

         let cnt = await UsersModel.countDocuments(obj);
         let page = req.query.page ? req.query.page : 0;
         let offset = req.query.offset ? req.query.offset : 10;
         let arr = await UsersModel.find(obj).skip(offset * page).limit(+offset);
         for (let i = 0; i < arr.length; i++) {
            arr[i]._doc.hasSubscriptions = (await SubscriptionsModel.countDocuments({phone : arr[i].phone})) > 0;
            arr[i]._doc.password = undefined;
            arr[i]._doc.tokens = undefined;
         }
         return res.status(200).send({array: arr, amount: cnt});
      } catch (e) {
         console.log('/manager/get/users', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Get user docs
   * */

   router.get('/manager/get/users/docs', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.user_edit && !req.user.access.user_verify && !req.user.access.subscription_check) return res.sendStatus(403);

         if (!req.query.file) return res.sendStatus(400);

         return res.sendFile(process.env.FILE_PATH + req.query.file);
      } catch (e) {
         console.log('/manager/get/users/docs', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Get user orders
   * */

   router.get('/manager/get/orders', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.order_manage && !req.user.access.subscription_check) return res.sendStatus(403);

         let obj = {};
         obj.$and = [];

         if(req.query.relative) obj.$and.push({'subscription.relative.id' : req.query.relative});

         if (req.query.phone) obj.$and.push({phone: req.query.phone});
         if(req.query.id) obj.$and.push({id: req.query.id});
         if (req.query.status) obj.$and.push({body: {$elemMatch: {status: req.query.status}}});
         if(req.query.type) obj.$and.push({type: req.query.type});
         if (!obj.$and[0]) delete obj.$and;

         let cnt = await OrdersModel.countDocuments(obj);
         let page = req.query.page ? req.query.page : 0;
         let offset = req.query.offset ? req.query.offset : 10;
         let arr = await OrdersModel.find(obj).skip(offset * page).limit(+offset);

         // Names for services & products
         let services = [];
         let products = [];
         (await ServiceModel.find({})).forEach(val => services.push({id: val.id, name: val.name}));
         (await ProductsModel.find({})).forEach(val => products.push({id: val.id, name: val.name, optionName : val.optionName ? val.optionName : null}));

         arr.forEach(val => {
            if(val.type === 'subscription'){
               let service = services.find(service => service.id === val.body[0].id);
               val.body[0]._doc.name = service ? service.name : null;
               val.body[0].products.forEach(key => {
                  let product = products.find(product => product.id === key.id);
                  key._doc.name = product ? product.name : null;
                  key._doc.optionName = product ? product.optionName : null;
               })
            }else if(val.type === 'payment'){
               for (let i = 0; i < val.body.length; i++) {
                  let service = services.find(service => service.id === val.body[i].id);
                  val.body[i]._doc.name = service ? service.name : null;
                  val.body[i].products.forEach(key => {
                     let product = products.find(product => product.id === key.id);
                     key._doc.name = product ? product.name : null;
                     key._doc.optionName = product ? product.optionName : null;
                  })
               }
            }
            delete val._doc.phoneSign;
         });
         return res.status(200).send({array: arr, amount: cnt});
      } catch (e) {
         console.log('/manager/get/users/subscriptions', e.message);
         return res.sendStatus(500);
      }
   });

   /*
    * Get user subscriptions
   * */

   router.get('/manager/get/users/subscriptions', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.user_edit && !req.user.access.subscription_check) return res.sendStatus(403);

         let obj = {};
         obj.$and = [];

         if(req.query.relative) obj.$and.push({'relative.id' : req.query.relative});
         if(req.query.phone) obj.$and.push({phone: req.query.phone});
         if(req.query.type) obj.$and.push({type: req.query.type});

         if (!obj.$and[0]) delete obj.$and;

         let cnt = await SubscriptionsModel.countDocuments(obj);
         let page = req.query.page ? req.query.page : 0;
         let offset = req.query.offset ? req.query.offset : 10;
         let arr = await SubscriptionsModel.find(obj).skip(offset * page).limit(+offset);
         arr.forEach(val => delete val._doc.phoneSign);
         return res.status(200).send({array: arr, amount: cnt});
      } catch (e) {
         console.log('/manager/get/users/subscriptions', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Verify user
   * */

   router.post('/manager/verify/user', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.user_edit && !req.user.access.user_verify) return res.sendStatus(403);

         //   TODO: set action

         if (!req.query.phone || !req.query.verify) return res.sendStatus(400);
         if (req.query.verify === false && !req.body.decline) return res.sendStatus(400);

         let user = await UsersModel.findOne({phone: req.query.phone});
         if (!user) return res.sendStatus(303);
         // if (user.bdayVerified) return res.sendStatus(303);
         req.query.verify = (req.query.verify === 'true');

         //Delete notification about verification
         await NotificationsModel.findOneAndDelete({type: 'validate_user', 'user.phone': req.query.phone, relative : null});

         if (!req.query.verify && req.body.decline) {
            await UsersModel.findOneAndUpdate({phone: req.query.phone}, {
               bdayVerified: false,
               decline: req.body.decline
            }, {runValidators: true});
            user = await UsersModel.findOne({phone: req.query.phone});
            return res.status(200).send(user);
         } else if (req.query.verify) {
            await UsersModel.findOneAndUpdate({phone: req.query.phone}, {
               bdayVerified: true,
               decline: null
            }, {runValidators: true});
            user = await UsersModel.findOne({phone: req.query.phone});
            return res.status(200).send(user);
         }
      } catch (e) {
         console.log('/manager/verify/user', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Verify relative
   * */

   router.post('/manager/verify/relative', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.user_edit && !req.user.access.user_verify) return res.sendStatus(403);

         if (!req.query.user || !req.query.relative || !req.query.verify) return res.sendStatus(400);
         if (req.query.verify === false && !req.body.decline) return res.sendStatus(400);
         let user = await UsersModel.getRelative(req.query.user, req.query.relative);
         if (!user) return res.sendStatus(303);
         let relative = user.relatives.findIndex(val => val._id.toString() === req.query.relative);
         if (relative < 0 || relative.verified) return res.sendStatus(303);
         req.query.verify = (req.query.verify === 'true');

         //Delete notification about verification
         await NotificationsModel.findOneAndDelete({
            type: 'validate_relative',
            'user.phone': user.phone,
            'relative.id': req.query.relative
         });

         if (!req.query.verify && req.body.decline) {
            user.relatives[relative].decline = req.body.decline;
            user.relatives[relative].verified = false;
            user = await user.save();
         } else if (req.query.verify) {
            user.relatives[relative].decline = null;
            user.relatives[relative].verified = true;
            user = await user.save();
         }
         delete user._doc.tokens;
         return res.status(200).send(user);
      } catch (e) {
         console.log('/manager/verify/relative', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Get notifications
   * */

   router.get('/manager/get/notifications', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);

         let obj = {};
         obj.$and = [];

         if (req.query.type) obj.$and.push({type: req.query.type});
         obj.$or = [];
         Object.keys(req.user.access).forEach(val => {
            if (val === '$init') return false;
            return req.user.access[val] ? obj.$or.push({access: val}) : false
         });

         if (!obj.$and[0]) delete obj.$and;

         let cnt = await NotificationsModel.countDocuments(obj);
         let page = req.query.page ? req.query.page : 0;
         let offset = req.query.offset ? req.query.offset : 10;
         let arr = await NotificationsModel.find(obj).skip(offset * page).limit(+offset);
         return res.status(200).send({array: arr, amount: cnt});
      } catch (e) {
         console.log('/manager/get/notifications', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Get all structures
   * */

   router.get('/manager/get/structure', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.structure_edit) return res.sendStatus(403);

         if (req.query.id) return res.status(200).send({obj: await StructureModel.findOne({_id: req.query.id})});

         return res.status(200).send({arr: await StructureModel.find({})});
      } catch (e) {
         console.log('/manager/get/structures', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Create structure
   * */

   router.post('/manager/create/structure', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.structure_edit) return res.sendStatus(403);

         let result = await StructureModel.setStructure({
            name: req.body.name,
            services: req.body.services,
            amountPerTime: req.body.amountPerTime
         });
         if (!result) return res.sendStatus(303);
         return res.status(200).send(result);
      } catch (e) {
         console.log('manager/create/structure', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Edit structure
   * */

   router.post('/manager/edit/structure', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.structure_edit) return res.sendStatus(403);

         if(!req.body.id) return res.sendStatus(400);
         let structure = await StructureModel.findOne({_id: req.body.id});
         if(!structure)return res.sendStatus(303);

         if(req.body.name){
            if(await StructureModel.findOne({name: req.body.name}))return res.sendStatus(303);
            structure.name = req.body.name;
         }
         if(req.body.services && !req.body.amountPerTime) return res.sendStatus(400);
         if(req.body.services && req.body.amountPerTime){
            for (let i = 0; i < req.body.services.length; i++) {
               if(!req.body.amountPerTime.find(val => val.find(val => val === req.body.services[i].id))) return res.sendStatus(400);
               if(!await ServiceModel.findOne({id: req.body.services[i].id}))return res.sendStatus(303);
            }

            for (let i = 0; i < req.body.amountPerTime.length; i++) {
               for (let j = 0; j < req.body.amountPerTime[i].length; j++) {
                  if(req.body.amountPerTime[i].filter(val => val === req.body.amountPerTime[i][j]).length > 1)return res.sendStatus(400);
                  if(!req.body.services.find(val => val.id === req.body.amountPerTime[i][j])) return res.sendStatus(400);
               }
            }

            structure.services = req.body.services;
            structure.amountPerTime = req.body.amountPerTime;
         }else if(!req.body.services && req.body.amountPerTime){
            for (let i = 0; i < req.body.amountPerTime.length; i++) {
               for (let j = 0; j < req.body.amountPerTime[i].length; j++) {
                  if(req.body.amountPerTime[i].filter(val => val === req.body.amountPerTime[i][j]).length > 1)return res.sendStatus(400);
                  if(!structure.services.find(val => val.id === req.body.amountPerTime[i][j])) return res.sendStatus(400);
               }
            }
            structure.amountPerTime = req.body.amountPerTime;
         }
         return res.status(200).send(await structure.save());
      } catch (e) {
         console.log('/manager/edit/structure', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Delete structure
   * */

   router.delete('/manager/delete/structure', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.structure_edit) return res.sendStatus(403);

         if(!req.body.id) return res.sendStatus(400);

         return await StructureModel.deleteStructure(req.body.id) ? res.sendStatus(200) : res.sendStatus(303);
      }catch (e) {
         console.log('/manager/delete/structure', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Get service
   * */

   router.get('/manager/get/service', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.services_edit && !req.user.access.structure_edit) return res.sendStatus(403);

         if (req.query.id) return res.status(200).send({obj: await ServiceModel.findOne({id: req.query.id})});
         return res.status(200).send({arr: await ServiceModel.find({})});
      } catch (e) {
         console.log('/manager/get/service', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Create new service
   * */

   router.post('/manager/create/service', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.structure_edit) return res.sendStatus(403);

         if (!req.body.name || !req.body.cost || !req.body.max_people || !req.body.time || !req.body.structure || !req.body.preordered || !req.body.type) return false;

         let service = await ServiceModel.createNew(req.body);
         return service ? res.status(200).send(service) : res.sendStatus(303);
      } catch (e) {
         console.log('/manager/create/service', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Edit service
   * */

   router.post('/manager/edit/service', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.structure_edit && !req.user.access.services_edit) return res.sendStatus(403);

         if (!req.body.id) return res.sendStatus(400);
         let service = await ServiceModel.findOne({id: req.body.id});
         if (!service) return res.sendStatus(303);
         let keys = Object.keys(req.body);
         for (let i = 0; i < keys.length; i++) {
            switch (keys[i]) {
               case 'id': {
                  break;
               }
               case 'products': {
                  for (let j = 0; j < req.body[keys[i]].length; j++) {
                     if (!await ProductsModel.findOne({id: req.body[keys[i]][j]})) return res.sendStatus(303);
                  }
                  service.products = req.body.products;
                  break;
               }
               case 'cost': {
                  if (req.body.cost.length !== 8) return res.sendStatus(400);
                  for (let j = 0; j < req.body[keys[i]].length; j++) {
                     if (req.body[keys[i]][j] <= 0) return res.sendStatus(303);
                  }
                  service.cost = req.body.cost;
                  break;
               }
               case 'structure': {
                  if (!req.user.access.structure_edit) return res.sendStatus(403);
                  if (!req.body.pair || req.body.pair < -1) return res.sendStatus(400);
                  if (req.body.structure === service.structure) return res.sendStatus(400);
                  let structure = await StructureModel.findOne({_id: req.body.structure});
                  if (!structure) return res.sendStatus(303);
                  let oldStructure = await StructureModel.findOne({_id: service.structure});
                  oldStructure.amountPerTime = oldStructure.amountPerTime.filter(val => !val.find(val => val === service.id));
                  oldStructure.services = oldStructure.services.filter(val => val.id !== service.id);
                  await oldStructure.save();
                  structure.services.push({id: service.id});
                  req.body.pair === -1 ? structure.amountPerTime[req.body.pair].push(req.body.pair) : structure.amountPerTime.push([req.body.pair]);
                  await structure.save();
                  service.structure = req.body.structure;
                  break
               }
               case 'time': {
                  if (req.body.time.length !== 8) return res.sendStatus(400);
                  for (let j = 0; j < req.body[keys[i]].length; j++) {
                     if (+req.body[keys[i]][j].start.slice(0, 2) > +req.body[keys[i]][j].end.slice(0, 2)) return res.sendStatus(400);
                     if (+req.body[keys[i]][j].start.slice(0, 2) === +req.body[keys[i]][j].end.slice(0, 2) && +req.body[keys[i]][j].start.slice(3) >= +req.body[keys[i]][j].end.slice(3)) return res.sendStatus(400);
                  }
                  service.time = req.body.time;
                  break;
               }
               case 'customPrice' : {
                  for (let j = 0; j < req.body[keys[i]].length; j++) {
                     if(!req.body[keys[i]][j].dateStart) return res.sendStatus(400);
                     if(!req.body[keys[i]][j].dateEnd){
                        req.body[keys[i]][j].dateEnd = new Date(new Date(new Date(req.body[keys[i]][j].dateStart).setUTCDate(new Date(req.body[keys[i]][j].dateStart).getDate() + 1)).setUTCHours(0,0,0)).toISOString();
                     }
                  }
                  for (let j = 0; j < req.body[keys[i]].length; j++) {
                     if(req.body[keys[i]][j].dateEnd && new Date(req.body[keys[i]][j].dateEnd) - new Date(req.body[keys[i]][j].dateStart) <= 0) return res.sendStatus(400);
                     let arr = req.body[keys[i]].map(val => val);
                     arr.splice(j, 1);
                     if(arr.find(val => {
                        if(val.dateEnd && req.body[keys[i]][j].dateEnd){
                           if(new Date(req.body[keys[i]][j].dateStart) <= new Date(val.dateEnd) && new Date(req.body[keys[i]][j].dateEnd) >= new Date(val.dateStart)){
                              console.log(req.body[keys[i]][j].dateStart ,req.body[keys[i]][j].dateEnd);
                              console.log(val.dateStart, val.dateEnd);
                              return true;
                           }
                        }
                     }))return res.sendStatus(400);
                     if(req.body[keys[i]][j].cost <= 0) return false;
                  }
                  service.customPrice = req.body[keys[i]];
                  break;
               }
               case 'evening_diff' : {
                  if(typeof req.body[keys[i]] !== 'number'|| isNaN(req.body[keys[i]])) return res.sendStatus(400);
                  for (let j = 0; j < service.cost.length; j++) {
                     if(service.cost[j] - req.body[keys[i]] < 3.01) return res.sendStatus(400);
                  }
                  service.evening_diff = req.body[keys[i]];
                  break;
               }
               case 'evening_time' : {
                  if(typeof req.body[keys[i]] !== 'string') return res.sendStatus(400);
                  service.evening_time = req.body[keys[i]];
                  break;
               }
               case 'day_diff' : {
                  if(typeof req.body[keys[i]] !== 'number'|| isNaN(req.body[keys[i]])) return res.sendStatus(400);
                  for (let j = 0; j < service.cost.length; j++) {
                     if(service.cost[j] - req.body[keys[i]] < 3.01) return res.sendStatus(400);
                  }
                  service.evening_diff = req.body[keys[i]];
                  break;
               }
               case 'day_time' : {
                  if(typeof req.body[keys[i]] !== 'string') return res.sendStatus(400);
                  service.evening_time = req.body[keys[i]];
                  break;
               }
               default: {
                  service[keys[i]] = req.body[keys[i]];
                  break;
               }
            }
         }
         return res.status(200).send({obj: await service.save()});
      } catch (e) {
         console.log('/manager/edit/structure', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Delete service
   * */

   router.delete('/manager/delete/service', managerAuth, async (req, res) => {
      try {
         if (!req.user.access.structure_edit && !req.user.access.services_edit) return res.sendStatus(403);

         if(!req.body.id) return res.sendStatus(400);

         return await ServiceModel.deleteService(req.body.id) ? res.sendStatus(200) : res.sendStatus(303);
      }catch (e) {
         console.log('/manager/delete/service', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Get products
   * */

   router.get('/manager/get/product', managerAuth, async(req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.product_create && !req.user.access.product_edit) return res.sendStatus(403);

         if(req.query.id) return res.status(200).send(await ProductsModel.findOne({id: req.query.id}));
         return res.status(200).send(await ProductsModel.find({}));
      }catch (e) {
         console.log('/manager/get/product', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Create product
   * */

   router.post('/manager/create/product', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.product_create) return res.sendStatus(403);

         if(!req.body.name)return res.sendStatus(400);
         if(!req.body.amount)return res.sendStatus(400);
         if(typeof req.body.amount === 'object' && !req.body.optionName) return res.sendStatus(400);

         let product = await ProductsModel.createNew(req.body);
         if(!product) return res.sendStatus(303);

         return res.status(200).send(product);
      }catch (e) {
         console.log('/manager/create/product', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Edit product
   * */

   router.post('/manager/edit/product', managerAuth, async(req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.product_create && !req.user.access.product_edit) return res.sendStatus(403);
         if(!req.body.id) return res.sendStatus(400);
         if(req.body.name && await ProductsModel.findOne({name: req.body.name})) return res.sendStatus(303);

         let product = await ProductsModel.findOne({id: req.body.id});
         if(!product) return res.sendStatus(303);

         if(req.body.amount && typeof req.body.amount === 'object'){
            for (let i = 0; i < req.body.amount.length; i++) {
               if(!req.body.amount[i].option || !req.body.amount[i].amount || !req.body.amount[i].cost) return res.sendStatus(400);
               req.body.amount[i].id = i + 1;
            }
         }else{
            if(req.body.amount && typeof req.body.amount !== 'number') return res.sendStatus(400);
         }

         Object.keys(req.body).forEach(key => key === 'id' ? false : product[key] = req.body[key]);

         return res.status(200).send(await product.save());
      }catch (e) {
         console.log('/manager/edit/product', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Delete product
   * */

   router.delete('/manager/delete/product', managerAuth, async(req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.product_create && !req.user.access.product_edit) return res.sendStatus(403);

         if(!req.body.id) return res.sendStatus(400);

         return await ProductsModel.deleteProduct(req.body.id) ? res.sendStatus(200) : res.sendStatus(303);
      }catch (e) {
         console.log('/manager/delete/product', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Get subscription
   * */

   router.get('/manager/get/subscription', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.subscription_edit && !req.user.access.subscription_check) return res.sendStatus(403);

         if(req.query.id) return res.status(200).send({obj: await SubscriptionTypesModel.findOne({id: req.query.id})});
         return res.status(200).send({arr: await SubscriptionTypesModel.find({})});
      }catch (e) {
         console.log('/manager/get/subscription', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Create subscription
   * */

   router.post('/manager/create/subscription', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.subscription_edit) return res.sendStatus(403);

         let subscription = await SubscriptionTypesModel.createNew(req.body);
         if(!subscription) return res.sendStatus(303);

         return res.status(200).send(subscription);
      }catch (e) {
         console.log('/manager/create/subscription', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Edit subscription
   * */

   router.post('/manager/edit/subscription', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.subscription_edit) return res.sendStatus(403);

         if(!req.body.id) return res.sendStatus(400);
         let type = await SubscriptionTypesModel.findOne({id: req.body.id});
         if(!type) return res.sendStatus(303);
         if(req.body.limit && req.body.limit < 0) return res.sendStatus(400);
         if(req.body.cost && req.body.cost < 0) return res.sendStatus(400);
         if(req.body.services){
            for (let i = 0; i < req.body.services.length; i++) {
               let service = await ServiceModel.findOne({id: req.body.services[i].id});
               if(!service) return res.sendStatus(400);
               req.body.services[i].name = service.name;
            }
         }
         if(req.body.appliedProducts){
            for (let i = 0; i < req.body.appliedProducts.length; i++) {
               if(!req.body.appliedProducts[i].amount || req.body.appliedProducts[i].amount < 0)return res.sendStatus(400);
               let product = await ProductsModel.findOne({id: req.body.appliedProducts[i].id});
               if(!product) return res.sendStatus(400);
               req.body.appliedProducts[i].name = product.name;
            }
         }
         if(req.body.visits && req.body.visits < 0) return res.sendStatus(400);
         if(req.body.expired && new Date(req.body.expired) <= new Date) return res.sendStatus(400);
         if(req.body.age && req.body.age !== 'adult' && req.body.age !=='child')return res.sendStatus(400);


         Object.keys(req.body).forEach(key => key === 'id' ? false : type[key] = req.body[key]);

         return res.status(200).send(await type.save());
      }catch (e) {
         console.log('/manager/edit/subscription', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Delete subscription type
   * */

   router.delete('/manager/delete/subscription', managerAuth, async(req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.subscription_edit) return res.sendStatus(403);

         if(!req.body.id) return res.sendStatus(400);

         return await SubscriptionTypesModel.deleteType(req.body.id) ? res.sendStatus(200) : res.sendStatus(303);
      }catch (e) {
         console.log('/manager/delete/subscription', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Get subscription/order by qr
   * */

   router.get('/manager/qr/check', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.subscription_check) return res.sendStatus(403);

         if (!req.query.type || !req.query.sign) return res.sendStatus(400);
         if (!bcrypt.compareSync((process.env.ORDER_SECRET + req.query.id + process.env.ORDER_SECRET), req.query.sign)) return res.sendStatus(400);

         switch (req.query.type) {
            case 'subscription': {
               let sub = await SubscriptionsModel.findOne({id: req.query.id});
               if (!sub) return res.sendStatus(303);
               let user = await UsersModel.findOne({phone: sub.phone});
               if (!sub) return res.sendStatus(303);
               return res.status(200).send({obj: sub, user: user});
            }
            case 'order': {
               let order = await OrdersModel.findOne({id: req.query.id});
               if (!order) return res.sendStatus(303);
               for (let i = 0; i < order.body.length; i++) {
                  order._doc.body[i]._doc.name = (await ServiceModel.findOne({id: order.body[i].id})).name;
                  for (let j = 0; j < order.body[i].products.length; j++) {
                     order._doc.body[i].products[j]._doc.name = (await ProductsModel.findOne({id: order.body[i].products[j].id})).name;
                  }
               }
               let user = await UsersModel.findOne({phone: order.phone});
               delete user._doc.password;
               delete user._doc.tokens;
               return res.status(200).send({obj: order, user: user});
            }
            default: {
               return res.sendStatus(303);
            }
         }
      } catch (e) {
         console.log('/manager/qr/subscription/check', e.message);
      }
   });

   /*
   * Create subscription by phone, send code
   * */
   router.post('/manager/user/visit/code', managerAuth, async(req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.subscription_check) return res.sendStatus(403);
         if(!req.query.type) return res.sendStatus(400);
         if(!req.body.id) return res.sendStatus(400);
         switch (req.query.type) {
            case 'subscription' : {
               let subscription = await SubscriptionsModel.findOne({id: req.body.id});
               if(!subscription) return res.sendStatus(303);
               let phone = false;
               if(req.query.userType === 'user') phone = subscription.phone;
               if(req.query.userType === 'relative') phone = subscription.relative.phone;
               let code = makeId();
               subscription.phoneSign = code;
               await subscription.save();
               await SMSController.send(phone, 'Abonement: ' + code);
               break;
            }
            case 'order' : {
               let order = await OrdersModel.findOne({id: req.body.id});
               if(!order) return res.sendStatus(303);
               let phone = false;
               if(req.query.userType === 'user') phone = order.phone;
               if(req.query.userType === 'relative') phone = order.subscription.relative.phone;
               let code = makeId();
               order.phoneSign = code;
               await order.save();
               await SMSController.send(phone, 'Zakaz' + code);
               break;
            }
            default : {
               return res.sendStatus(400);
            }
         }
         return res.status(200).send('Awaiting verification code');

      }catch (e) {
         console.log('/manager/user/visit/code', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Create order by subscription
   * */

   router.post('/manager/qr/subscription/new', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.subscription_check) return res.sendStatus(403);

         if (!req.query.id || !req.query.end || !req.query.type || !req.query.sign) return res.sendStatus(400);
         if(new Date(req.query.end) <= Date.now() || new Date(req.query.end).getDate() > new Date().getDate()) return res.sendStatus(400);
         let sub;
         switch (req.query.type) {
            case 'qr': {
               if (!bcrypt.compareSync((process.env.ORDER_SECRET + req.query.id + process.env.ORDER_SECRET), req.query.sign)) return res.sendStatus(400);

               sub = await SubscriptionsModel.findOne({id: req.query.id});
               if (!sub) return res.sendStatus(400);
               break;
            }
            case 'phone': {
               sub = await SubscriptionsModel.findOne({id: req.query.id, phoneSign : req.query.sign});
               if (!sub) return res.sendStatus(400);
               sub.phoneSign = '';
               await sub.save();
               break;
            }
            default : {
               return res.sendStatus(400);
            }
         }
         let user = await UsersModel.findOne({phone: sub.phone});
         if (!user) return res.sendStatus(400);
         let body = {
            info: {
               phone: user.phone,
               email: user.email,
               type: "subscription",
               subscription: req.query.id
            },
            body: {
               id: sub.services[0].id,
               start: new Date(Date.now()).toISOString(),
               end: req.query.end,
               products: sub.appliedProducts
            }
         };

         let order = await OrdersController.buildOrder(body, user.phone);
         if (!order) return res.sendStatus(303);
         order.body.forEach(val => val.status = 'active');
         return res.status(200).send(await order.save());
      } catch (e) {
         console.log('/manager/subscription/qr/new', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Start order
   * */

   router.post('/manager/orders/start', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.subscription_check) return res.sendStatus(403);

         if (!req.query.id || !req.query.action || !req.query._id) return res.sendStatus(400);
         if (req.query.action === 'disabled'){
            if(!req.query.note) return res.sendStatus(400);
            let order = await OrdersModel.findOne({id: req.query.id});
            if(!order) return res.sendStatus(303);
            let pos = order.body.findIndex(val => val._id.toString() === req.query._id);
            if(pos < 0) return res.sendStatus(303);

            order.body[pos].status = 'disabled';
            order.body[pos].disabled = req.query.note;

            return res.status(200).send(await order.save());
         }else{
            if(!req.query.sign || !req.query.type) return res.sendStatus(400);
            if (req.query.type === 'qr' && !bcrypt.compareSync((process.env.ORDER_SECRET + req.query.id + process.env.ORDER_SECRET), req.query.sign)) return res.sendStatus(400);
            let order = req.query.type === 'qr' ?  await OrdersModel.findOne({id: req.query.id}) : await OrdersModel.findOne({id: req.query.id, phoneSign: req.query.sign});
            if (!order) return res.sendStatus(303);
            order.phoneSign = '';
            switch (req.query.action) {
               case 'start': {
                  let pos = order.body.findIndex(val => val._id.
                  toString() === req.query._id);
                  if (pos < 0) return res.sendStatus(303);
                  if (order.body[pos].status !== 'ordered') return res.sendStatus(303);
                  if(order.body[pos].bday === true && order.body[pos].type === 'payment'){
                     let temp = await BookerController.sendOrder(req.query.id, req.query._id, true);
                     order = temp ? temp : order;
                  }
                  order.body[pos].status = 'active';
                  return res.status(200).send(await order.save());
               }
               case 'end': {
                  let pos = order.body.findIndex(val => val._id.toString() === req.query._id);
                  if(pos < 0) return res.sendStatus(303);
                  if (order.body[pos].status !== 'active') return res.sendStatus(303);
                  order.body[pos].status = 'used';
                  if((order.type === 'payment' || order.type === 'cash') && order.body[pos].id === '05' && order.body[pos].bday !== true && !order.body.find(val => val.status === 'refunded') && (new Date() - order.body[pos].start)/1000/60/60 < 1){
                     let liqpay = new LiqPay(process.env.LPAY_PUBLIC, process.env.LPAY_PRIVATE, process.env.LPAY_PUBLIC, process.env.LPAY_PRIVATE);
                     await liqpay.api("request", {
                        "action"   : "refund",
                        "version"  : "3",
                        "order_id" : order.id,
                        "amount"   : order.body[pos].cost * 0.5,
                        'server_url'    :  'https://apisky.zapleo.com/form/complete?type=refund'
                     });
                  }
                  return res.status(200).send(await order.save());
               }
               default: {
                  return res.sendStatus(400);
               }
            }
         }

      } catch (e) {
         console.log('/manager/orders/start', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Refund order
   * */

   router.post('/manager/refund', managerAuth, async(req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.order_manage) return res.sendStatus(403);

         if(!req.body.order)return res.sendStatus(400);
         if(req.body.pos && typeof req.body.pos !== 'object') return res.sendStatus(400);
         if(+ req.body.refund > 1 || + req.body.refund < 0) return res.sendStatus(400);

         let order = await OrdersModel.findOne({id: req.body.order});
         if(!order) return res.sendStatus(303);

         if(order.type === 'payment'){
            if(!req.body.refund) return res.sendStatus(400);
            let liqpay = new LiqPay(process.env.LPAY_PUBLIC, process.env.LPAY_PRIVATE, process.env.LPAY_PUBLIC, process.env.LPAY_PRIVATE);
            if(req.body.pos && req.body.pos.length > 0){
               let total = 0;
               for (let i = 0; i < req.body.pos.length; i++) {
                  if(!order.body[req.body.pos[i]]) return res.sendStatus(303);
                  if(order.body[req.body.pos[i]].start.getTime() >= new Date().setHours(0,0,0,0) + 22 * 60 * 60 * 1000) return res.sendStatus(303);
                  if(order.body[req.body.pos[i]].status !=='ordered') return res.sendStatus(303);
                  let refund = 0;
                  refund += order.body[req.body.pos[i]].cost;
                  order.body[req.body.pos[i]].products.forEach(val => {
                     if(Array.isArray(val.amount)){
                        val.amount.forEach(val => refund += val.cost * val.amount)
                     }else{
                        refund += val.cost * val.amount
                     }
                  });
                  refund *= (+ req.body.refund);
                  order.body[req.body.pos[i]].status = 'processing';
                  order.body[req.body.pos[i]].refund = refund;
                  order.body[req.body.pos[i]].refundM = 0.8;
                  total += refund;
               }
               await liqpay.api("request", {
                  "action"   : "refund",
                  "version"  : "3",
                  "order_id" : order.id,
                  "amount"   : total,
                  'server_url'    :  'https://apisky.zapleo.com/form/complete?type=refund'
               });
               return res.status(200).send({order : await order.save(), refund : total});
            }else{
               let total = 0;
               for (let i = 0; i < order.body.length; i++) {
                  if(!order.body[i]) return res.sendStatus(303);
                  if(order.body[req.body.pos[i]].start.getTime() >= new Date().setHours(0,0,0,0) + 22 * 60 * 60 * 1000) return res.sendStatus(303);
                  if(order.body[i].status !=='ordered') return res.sendStatus(303);
                  let refund = 0;
                  refund += order.body[i].cost;
                  order.body[i].products.forEach(val => {
                     if(Array.isArray(val.amount)){
                        val.amount.forEach(val => refund += val.cost * val.amount)
                     }else{
                        refund += val.cost * val.amount
                     }
                  });
                  refund *= (+ req.body.refund);
                  total += refund;
                  order.body[i].status = 'processing';
                  order.body[req.body.pos[i]].refund = refund;
                  order.body[req.body.pos[i]].refundM = 0.8;
               }
               order.refund = total;
               await liqpay.api("request", {
                  "action"   : "refund",
                  "version"  : "3",
                  "order_id" : order.id,
                  "amount"   : total,
                  'server_url'    :  'https://apisky.zapleo.com/form/complete?type=refund'
               });
               return res.status(200).send({order : await order.save(), refund : total});
            }
         }else if(order.type === 'subscription'){
            if(!order.body[0]) return res.sendStatus(303);
            if(order.body[0].start.getTime() - Date.now() < 24 * 60 * 60 * 1000) return res.sendStatus(303);
            if(order.body[0].status !=='ordered') return res.sendStatus(303);
            await SubscriptionsModel.findOneAndUpdate({id: order.subscription.id}, {$inc: {'visitsAmount' : 1}}, {runValidators: true});
            order.body[0].status = 'refunded';
            return res.status(200).send({order: await order.save()});
         }
         return res.sendStatus(200);
      }catch (e) {
         console.log('/manager/refund', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Create new terminal
   * */

   router.post('/manager/terminal', managerAuth, async(req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user._doc.access.terminal_manage) return res.sendStatus(403);

         if(!req.body.address || !req.body.password) return res.sendStatus(400);

         let terminal = await TerminalModel.createTerminal(req.body.address, req.body.password);
         return  terminal ? res.status(200).send(terminal) : res.sendStatus(400);
      }catch (e) {
         console.log('/manager/terminal/create', e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Clear relations for subscription
   * */

   router.post('/manager/clear/subscription', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user._doc.access.user_edit) return res.sendStatus(403);

         if(!req.query.id) return res.sendStatus(400);

         let subscription = await SubscriptionsModel.findOne({id: req.query.id});
         if(!subscription) return res.sendStatus(303);
         Object.keys(subscription.relative).forEach(key => key !== '$init' ? subscription.relative[key] = false : null);
         return res.status(200).send(await subscription.save());
      }catch (e) {
         console.log(e.message);
         return res.sendStatus(500);
      }
   });

   /*
   * Seed users
   * */

   router.post('/manager/seed/users', managerAuth, async (req, res) => {
      try {
         if (!req.user) return res.sendStatus(401);
         if (!req.user.access.manager_edit) return res.sendStatus(403);

         await UsersModel.seed(req.body.amount);
         return res.sendStatus(200);
      } catch (e) {
         console.log('/manager/seed/users', e.message);
         return res.sendStatus(500);
      }
   });
   return router;
};