const express = require('express'),
    router = express.Router(),
    objectHash = require('object-hash'),
    bcrypt = require('bcryptjs'),
    LiqPay = require('./../liqpay/liqpay');

const SubscriptionTypesModel = require('./../models/SubscriprtionTypes');
const SubscriptionTempModel = require('../models/SubscriptionTemp');
const SubscriptionModel = require('../models/Subscriptions');

const auth = require('.././middlewares/auth');

/*
* Get all subscription types
* */

router.get('/subscriptions', async (req, res) => {
    try {
        let result = await SubscriptionTypesModel.getSubscriptions();
        return result ? res.status(200).send(result) : res.sendStatus(303);
    }catch (e) {
        console.log(e.message);
        return res.sendStatus(500);
    }
});

/*
* Generate checkout button for selected subscription
* */

router.post('/subscriptions/buy', auth, async (req, res) => {
    try {
        if(!req.user)return res.sendStatus(401);
        if(!req.body.body) return res.sendStatus(400);
        let arr = req.body.body;
        let cost = 0;
        let subscriptionsArr = [];
        for (let i = 0; i < arr.length; i++) {
            if(!arr[i].id)return res.sendStatus(400);
            let subscription = await SubscriptionTypesModel.getSubscriptions(arr[i].id);
            if(!subscription) return res.sendStatus(400);
            if(subscription._doc.limit !== false && subscription._doc.limit < arr[i].amount)return res.sendStatus(303);

            for (let j = 0; j < arr[i].amount; j++) {
                cost += subscription.cost;
                let result = {
                    phone:  req.user.phone,
                    email:  req.user.email,
                    type:   subscription.id,
                    visitsAmount: subscription.visits,
                    maxVisitsPerTime: subscription.maxVisitsPerTime,
                    name: subscription.name,
                    cost: subscription.cost,
                    services: subscription.services,
                    appliedProducts: subscription.appliedProducts,
                    description: subscription.description,
                    setRelatives: subscription.setRelatives,
                    expired: subscription.expired,
                    age: subscription.age
                };
                subscriptionsArr.push(result);
            }
        }
        if(subscriptionsArr.find(val => {return val.age === 'child';}) && !subscriptionsArr.find(val => val.age ==='adult'))return res.sendStatus(400);

        let time = Date.now();
        let result = {};
        result.body = [];
        for (let i = 0; i < subscriptionsArr.length; i++) {
            let temp = await SubscriptionTempModel.setSubscription(subscriptionsArr[i], time);
            if(!temp)return res.sendStatus(303);
            result.body.push(subscriptionsArr[i]);
        }
        let lpay = new LiqPay(process.env.LPAY_PUBLIC, process.env.LPAY_PRIVATE);
        result.total = cost;
        result.button = lpay.cnb_form({
            'action'        :  'pay',
            'amount'        :  cost,
            'currency'      :  'UAH',
            'description'   :  'Order '+time,
            'order_id'      :  time,
            'version'       :  '3',
            'sandbox'       :  '1',
            'server_url'    :  'https://testapisky.zapleo.com/form/complete?type=subscription'
        });
        return res.status(200).send(result);

    }catch (e) {
        console.log(e.message);
        return res.sendStatus(500)
    }
});

/*
* Add relative to subscription
* */

router.post('/subscriptions/add-relative', auth, async (req, res) => {
   try {
       if(!req.user)return res.sendStatus(401);
       if(!req.body.subscription)return res.sendStatus(400);
       if(!req.body.relative) return res.sendStatus(400);
       let relative = req.user.relatives.find(val => val._id.toString() === req.body.relative);
       if(!relative)return res.sendStatus(303);
       if(!relative.verified)return res.sendStatus(303);
       let subscription = await SubscriptionModel.getOne(req.body.subscription);
       if(!subscription)return res.sendStatus(303);
       if(subscription.relative.id) return res.sendStatus(303);
       let result = await SubscriptionModel.setRelative(subscription, relative);
       return result ? res.status(200).send(result) : res.sendStatus(303);
   } catch (e) {
       console.log(e.message);
       return res.sendStatus(500);
   }
});

module.exports = router;
