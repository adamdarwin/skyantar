const mongoose = require('mongoose'),
    jwt = require('jsonwebtoken'),
    bcrypt = require('bcryptjs');

const TerminalSchema = new mongoose.Schema({
    address: {
        // physical address of terminal
        type : String,
        required: true
    },
    status: {
        // Current status : ready, active, disabled
        type: String,
        required: true,
        default : 'ready'
    },
    login :{
        type: String,
        default : '' + Date.now()
    },
    password : {
        type: String,
        required: true
    },
    log: {
       // loq of requests from terminal
       type: [{
           url : {
               type : String,
               required: true
           },
           method: {
               type : String,
               required: true
           },
           status : {
               type: Number,
               required: true
           },
           timestamp: {
               type: Number,
               default : Date.now()
           },
           body : {
               type: mongoose.SchemaTypes.Mixed,
               default : null
           },
           errorMessage : {
               type : String,
               default : null
           }
       }],
        default: null
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }]
});

/*
* Generates JWT for terminal session
*
* @return   {string}    JWT
* */

TerminalSchema.methods.generateJWT= async function(){
    try {
        const token = jwt.sign({id: this.id}, process.env.JWT_SECRET, {expiresIn: '1 day'});
        this.tokens = [{token}];
        await this.save().catch(err => {if (err)console.log(err)});
        return token;
    }catch (e) {
        console.log(e.message);
        return false;
    }
};

/*
* Check is terminal credentials are correct
*
* @param    {object}    Object with phone and password
*
* @return   {object}    User model
* */

TerminalSchema.statics.findByCredentials = async ({phone, password}) => {
    try{
        const user = await Terminal.findOne({phone});
        if(!user){
            return false;
        }
        return await bcrypt.compare(password, user.password) ? user : false;
    }catch (e) {
        console.log(e.message);
        return false;
    }
};

TerminalSchema.pre('save', async function (next) {
    if (this.isModified('password')) {
        this.password = await bcrypt.hash(this.password, 8)
    }
    if(this.isModified('log')){
        const logMax = 50; // Maximum log notes
        if(this.log.length > logMax) this.log.splice(0, this.log.length - logMax);
    }
    next();
});
TerminalSchema.pre('findOneAndUpdate', async function (next) {
    if (Object.keys(this._update).indexOf('password') !== -1) {
        this._update.password = await bcrypt.hash(this._update.password, 8)
    }
    next();
});

const Terminal = new mongoose.model('terminal', TerminalSchema);

/*
* Create new terminal
*
* @param    {string}   address  Address of terminal
* @param    {string}   password Password for login
*
* @return   {object}    Saved terminal
* */

Terminal.createTerminal = async (address, password) =>{
    try {
        if(password.length < 10) return false;
        let terminal = new Terminal({
           address,
           password
        });
        terminal = await terminal.save();
        delete terminal._doc.password;
        return terminal;
    }catch (e) {
        console.log('createTerminal', e.message);
        return false;
    }
};

module.exports = Terminal;
