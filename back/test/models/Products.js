const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductsSchema = new Schema({
    id: {
      type: String,
      required: true,
      unique: true
    },
    name    :   {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    cost    :   {
        type: Number
    },
    amount  :   Schema.Types.Mixed,
    preferred: {
        type: Boolean,
        default: false,
        required: true
    },
    status: {
        // Active or not
        type: Boolean,
        required: true,
        default: true
    },
    optionName : {
        type: String,
        required: false,
        default: null
    }
});

let Product = new mongoose.model('products', ProductsSchema);

/*
* Returns JSON of product
* @param    {string}    id  Id of product.
* @return   {object}    Product object
* */

Product.getProduct = async id =>{
    try {
        let doc = await Product.findOne({id:id.toString()});
        return doc ? doc : false;
    }catch (e) {
        console.log(e.message);
        return false;
    }
};


/*
* Returns full amount of products
*
* @param    {string}    id  Id of product
*
* @return   {number}    Amount of products
* */

Product.getMaxAvailable = async (id, option = null) => {
    try{
        let doc = await Product.findOne({id: id });
        if(option){
            for (let i = 0; i < doc.amount.length; i++) {
                if(doc.amount[i].option === option)return doc.amount[i].amount;
            }
            return false;
        }else{
            return doc.amount
        }
    }catch (e) {
        console.log(e.message);
        return false;
    }
};

/*
* Create new product
*
* @param    {object}    obj Product object
*
* @return    {object}    Product model
* */

Product.createNew = async obj => {
  try {
      const ServiceModel = require('./Services');
      if(await Product.findOne({name: obj.name}))return false;
      let id = (await Product.countDocuments({})) + 1 + '';
      id = '1' + id;
      while(await Product.findOne({id: id})){
          id = + (id.slice(1)) + 1;
          id = '1' + id;
      }
      if(typeof obj.amount === 'object'){
          for (let i = 0; i < obj.amount.length; i++) {
              if(!obj.amount[i].option || !obj.amount[i].amount || !obj.amount[i].cost) return false;
              obj.amount[i].id = i+1;
          }
      }else{
          if(obj.amount < 0) return false;
      }
      let serviceArr = [];
      if(obj.service){
          if(typeof obj.service !== 'object') return false;
          for (let i = 0; i < obj.service.length; i++) {
              let service = await ServiceModel.findOne( {id: obj.service[i]});
              if(!service) return false;
              service.products.push(id);
              serviceArr.push(service);
          }
      }
      let product = new Product({
          id: id,
          preferred : obj.preferred ? obj.preferred : false,
          name: obj.name,
          optionName : obj.optionName ? obj.optionName : false,
          amount : obj.amount,
          cost: obj.cost ? obj.cost : undefined
      });
      for (let i = 0; i < serviceArr.length; i++) {
          await serviceArr[i].save();
      }
      return await product.save();
  }  catch (e) {
      console.log('Product.createNew', e.message);
      return false
  }
};

/*
* Delete product
*
* @param    {string}    id  Product id
*
* @return   {boolean}   Was deleted
* */

Product.deleteProduct = async id => {
  try {
      const ServiceModel = require('./Services');
      let product = await Product.findOne({id: id});
      if(!product) return false;

      let abc = await ServiceModel.updateMany({products : {$eq : id}}, {$pull: {products: id}}, {runValidators : true});
      return !!await Product.findOneAndDelete({id: id});
  }  catch (e) {
      console.log('Product.deleteProduct', e.message);
      return false;
  }
};

module.exports = Product;