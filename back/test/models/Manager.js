const mongoose = require('mongoose'),
    validator = require('validator'),
    bcrypt = require('bcryptjs'),
    jwt = require('jsonwebtoken');

const ManagerSchema = new mongoose.Schema({
    login: {
        type: String,
        required: true,
        unique: true
    },
    name: {
        type: String,
        required: true,
        trim: true
    },
    sname: {
        type: String,
        required: true,
        trim: true
    },
    lname: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    phone: {
        type: String,
        validate(value){
            if(!validator.isMobilePhone(value, 'uk-UA')){
                throw new Error('Phone is invalid');
            }
        },
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    status: {
        // status of manager account : active/banned
        type: String,
        required: true,
        default: 'active'
    },
    new : {
        type: Boolean,
        default: true
    },
    tokens: [{
        token: {
            type: String,
            requried: true
        }
    }],
    access: {
        manager_edit : {
            type: Boolean,
            default: false
        //    Create manger, edit manager, suspend manager
        },
        manager_actions : {
            type: Boolean,
            default: false
        //    View managers actions
        },
        user_edit : {
            type: Boolean,
            default: false
            //    Create user, edit user, ban user
        },
        user_verify : {
            type: Boolean,
            default: false
            //    Verify user and relative docs
        },
        structure_edit : {
          type: Boolean,
          default : false
        },
        services_edit : {
            type: Boolean,
            default: false
        //    Edit service
        },
        product_edit : {
            type: Boolean,
            default: false
            //    Change product status (active/inactive)
        },
        product_create: {
            type : Boolean,
            default: false
        }, // Create/delete product
        subscription_edit : {
            type: Boolean,
            default: false
            //    Change product price
        },
        subscription_check : {
            type: Boolean,
            default: false
            //    Change product price
        },
        requests : {
            type : Boolean,
            default: false
        //    Process user requests
        },
        liqPay : {
            type: Boolean,
            default: false
            //    Edit liqPay settings
        },
        order_manage : {
            type: Boolean,
            default: false,
        //    Make refunds, manage orders
        },
        terminal_manage : {
            type: Boolean,
            default: false,
            //    Create, delete, manage terminal
        }
    }
});

ManagerSchema.methods.generateJWT= async function(){
    try {
        const token = jwt.sign({id: this.id}, process.env.JWT_SECRET, {expiresIn: '1 day'});
        this.tokens = [{token}];
        await this.save().catch(err => {if (err)console.log(err)});
        return token;
    }catch (e) {
        console.log(e.message);
        return false;
    }
};

/*
* Check is user credentials are correct
*
* @param    {object}    Object with phone and password
*
* @return   {object}    User model
* */

ManagerSchema.statics.findByCredentials = async ({login, password}) => {
    try {
        const user = await Manager.findOne({login:login});
        if(!user){
            return false;
        }
        return await bcrypt.compare(password, user.password) ? user : false;
    }catch (e) {
        console.log(e.message);
        return false;
    }
};

ManagerSchema.pre('save', async function (next) {
    if (this.isModified('password')) {
        this.password = await bcrypt.hash(this.password, 8)
    }
    next();
});
ManagerSchema.pre('findOneAndUpdate', async function (next) {
    if (Object.keys(this._update).indexOf('password') !== -1) {
        this._update.password = await bcrypt.hash(this._update.password, 8)
    }
    next();
});

const Manager = new mongoose.model('managers', ManagerSchema);

const translit = sym => {
    const rus = 'абвгдежзийклмнопрстуфхцчшщэюя';
    const eng = 'abvgdezziyklmnoprstufhwcsseuy';
    const ua = 'абвгдєжзиіїклмнопрстуфхцчшщэюя';
    const engua = 'abvgdezziiiklmnoprstufhwcsseuy';
    if(sym.charCodeAt() === 1111 || sym.charCodeAt() === 1110 || sym.charCodeAt() === 1108){
        let i = ua.indexOf(sym);
        if(i === -1)throw new Error('Invalid symbol');
        return engua[i];
    }else{
        let i = rus.indexOf(sym);
        if(i === -1)throw new Error('Invalid symbol');
        return eng[i];
    }
};

/*
* Generate random password for manager for the first entry
*
* @return   {string}    Password
* */

const makePassword = () => {
    let text = "";
    let possible = "0123456789";

    for (let i = 0; i < 6; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};

/*
* Create new manager
*
* @param    {Object}    data    Manager data
*
* @return   {Object}    Saved manager model
* */

Manager.createManager = async data => {
    try {
        let login = 'sky' + Date.now().toString().slice(3, -4) + translit(data.name[0].toLowerCase()) + translit(data.sname[0].toLowerCase()) + translit(data.lname[0].toLowerCase());
        let password = makePassword();
        let manager = new Manager({
            phone: data.phone,
            name: data.name,
            sname: data.sname,
            lname: data.lname,
            login: login,
            email: data.email,
            password: password
        });
        let result = await manager.save();
        if(result) result.password = password;
        return result ? result : false;
    }catch (e) {
        console.log(e.message);
        return false;
    }
};
module.exports = Manager;
