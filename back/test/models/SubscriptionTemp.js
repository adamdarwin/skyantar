const mongoose = require('mongoose'),
    validator = require('validator');

const SubscriptionTempSchema = new mongoose.Schema({
    orderId: {
        type: String,
        required: true,
        unique: false
    },
   phone: {
       type: String,
       required: true,
       validate(value){
           if(!validator.isMobilePhone(value, 'uk-UA')){
               throw new Error('Phone is invalid');
           }
       }
   },
    email: {
        type: String,
        default: null,
        trim: true,
        validate(value){
            if(value && !validator.isEmail(value)){
                throw new Error('Email is invalid');
            }
        }
    },
    type: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    cost: {
        type: Number,
        required: true
    },
    services: [{
        id : {
            type: String,
            required: true
        },
        name:{
            type: String,
            required: true
        }
    }],
    appliedProducts: [{
        id : {
            type: String,
            required: true
        },
        amount:{
            type: Number,
            required: true
        },
        name:{
            type: String,
            required: true
        }
    }],
    description: {
        type: String,
        required: true,
    },
    visitsAmount : {
       type: Number,
        required: true
    },
    maxVisitsPerTime:{
        type: Number,
        required: true,
        default: null
    },
    setRelatives: {
        type: Boolean,
        required: true
    },
    relative : {
        phone: {
            type: mongoose.Schema.Types.Mixed,
            required: true,
            default: false
        },
        name: {
            type: mongoose.Schema.Types.Mixed,
            required: true,
            default: false
        },
        sname: {
            type: mongoose.Schema.Types.Mixed,
            required: true,
            default: false
        }
    },
    expired : {
       type: String,
        required: true
    },
    age: {
        type: String,
        required: true
    }
});

const SubscriptionTemp = new mongoose.model('subscriptions-temp', SubscriptionTempSchema);

/*
* Create temp subscription order
*
* @param    {object}    obj Subscription object
*
* @return   {bool}  Was this subscription saved
* */

SubscriptionTemp.setSubscription = async (obj, time) => {
    try {
        obj.orderId = time;
        let temp = new SubscriptionTemp(obj);
        let result = await temp.save();
        return result.orderId ? result.orderId : false;
    }catch (e) {
        console.log(e.message);
        return false;
    }
};

/*
* Get temp subscription from base
*
* @param    {string}    id  Id of recorded subscription
*
* @return   {object}    Found subscription model
* */

SubscriptionTemp.getSubscriptions = async orderId => {
  try {
      return await SubscriptionTemp.find({orderId: orderId});
  }  catch (e) {
      console.log(e.message);
      return false;
  }
};

module.exports = SubscriptionTemp;