const mongoose = require('mongoose');

const NotificationSchema = new mongoose.Schema({
    access : {
        type: String,
        required: true,
        default : null
    },
    time : {
        type : Number,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    user : {
        type: {
            name: {
                type: String,
                default : null
            },
            sname: {
                type: String,
                default : null
            },
            phone: {
                type: String,
                default : null
            }
        },
        default: null
    },
    relative : {
        type: {
            id: {
                type: String,
                default: null
            },
            name: {
                type: String,
                default : null
            },
            sname: {
                type: String,
                default : null
            },
            phone: {
                type: String,
                default : null
            }
        },
        default: null
    }
});

const Notifications = new mongoose.model('notifications', NotificationSchema);

/*
* Create new notification
*
* @param    {string}    type    Type of action
* @param    {string}    user    Users phone
*
* @return   {object}    Notification model
* */

Notifications.createNew = async (type, user = null, relative = null) => {
    try {
        switch (type) {
            case 'validate_user' : {
                if(!user) return false;
                let notification = new Notifications({
                   access :  'user_verify',
                    time : new Date().getTime(),
                    type : type,
                    description : 'Были добавлены фотографии пользователя ' + user.sname + ' ' + user.name + ' Необходима верификация.',
                    user : {
                        name: user.name,
                        sname: user.sname,
                        phone: user.phone
                    }
                });
                return await notification.save();
            }
            case 'validate_relative' : {
                if(!user || !relative) return false;
                let notification = new Notifications({
                    access: 'user_verify',
                    time: new Date().getTime(),
                    type: type,
                    description: 'Были добавлены фотографии родственника ' + relative.name + ' ' + relative.sname + ' пользователя ' + user.sname + ' ' + user.name + ' Необходима верификация.',
                    user : {
                        name: user.name,
                        sname: user.sname,
                        phone: user.phone
                    },
                    relative : {
                        name: relative.name,
                        sname: relative.sname,
                        phone: relative.phone,
                        id: relative._id.toString()
                    },
                });
                return await notification.save();
            }
            default: return false
        }
    }catch (e) {
        console.log('Notifications.createNew', e.message);
        return false;
    }
};

module.exports = Notifications;