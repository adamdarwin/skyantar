const mongoose = require('mongoose');

let SmsSchema = new mongoose.Schema({
    phone : {
        type : String,
        required : true
    },
    code : {
        type : String,
        required : true,
        unique : true
    },
    created : {
        // Unix timestamp when code was created
        type : Number,
        default : Date().now,
        required : true
    },
    type : {
        type: String,
        required : true
    }
});

let SmsModel = new mongoose.model('sms', SmsSchema);

const makeId = () => {
    let text = "";
    let possible = "0123456789";

    for (let i = 0; i < 4; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};

/*
* Returns new sms code for selected phone
*
* @param    {String}    phone   Recipient's phone
* @param    {String}    type    Type of access
* */

let smsNew = async (phone, type) => {
    try {
        if(await SmsModel.findOne({phone, type})) return false;
        let code = makeId();
        let sms = new SmsModel({
            phone,
            code,
            type
        });
        await sms.save();
        return code;
    }catch (e) {
        console.log('smsNew', e.message);
        return false;
    }
};


module.exports = SmsModel;