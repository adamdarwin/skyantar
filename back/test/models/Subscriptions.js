const mongoose =require('mongoose'),
    validator = require('validator'),
    request = require('request-promise-native');

const SubscriptionTempModel = require('./../models/SubscriptionTemp');
const SubscriptionTypeModel = require('./../models/SubscriprtionTypes');
const UsersModel = require('./../models/Users');

const SubscriptionSchema = new mongoose.Schema({
    id: {
      type: String,
      required: true,
      unique: true
    },
    phone: {
        type: String,
        required: true,
        validate(value){
            if(!validator.isMobilePhone(value, 'uk-UA')){
                throw new Error('Phone is invalid');
            }
        }
    },
    email: {
        type: String,
        default: null,
        trim: true,
        validate(value){
            if(value && !validator.isEmail(value)){
                throw new Error('Email is invalid');
            }
        }
    },
    type: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    cost: {
        type: Number,
        required: true
    },
    services: [{
        id : {
            type: String,
            required: true
        }
    }],
    appliedProducts: [{
        id : {
            type: String,
            required: true
        },
        amount: {
            type: Number,
            required: true
        }
    }],
    description: {
        type: String,
        required: true,
    },
    visitsAmount : {
        type: Number,
        required: true
    },
    maxVisitsPerTime:{
        type: Number,
        required: true,
        default: null
    },
    setRelatives: {
      type: Boolean,
      required: true
    },
    relative : {
        id: {
            type: mongoose.Schema.Types.Mixed,
            required: true,
            default: false
        },
        name: {
            type: mongoose.Schema.Types.Mixed,
            required: true,
            default: false
        },
        sname: {
            type: mongoose.Schema.Types.Mixed,
            required: true,
            default: false
        },
        phone: {
            type: mongoose.Schema.Types.Mixed,
            required: true,
            default: false
        }
    },
    expired : {
        type: String,
        required: true
    },
    age: {
        type: String,
        required: true
    },
    phoneSign : {
        // field for SMS code storing
        type: String,
        default: null,
        required: false
    }
});

const Subscriptions = new mongoose.model('subscriptions', SubscriptionSchema);

/*
* Save subscription to main base
*
* @param    {string}   id  Id of temp subscription
*
* @return   {bool}  Was subscription recorded successfully
* */

Subscriptions.setSubscription = async (orderId) => {
  try {
      let arr = await SubscriptionTempModel.getSubscriptions(orderId);
      if(!arr)return false;
      console.log('Subscriptions amount: ', arr.length);
      for (let i = 0; i < arr.length; i++) {
          await SubscriptionTempModel.deleteOne({orderId: orderId});
          let sub = new Subscriptions({
              id: Date.now(),
              phone: arr[i].phone,
              email: arr[i].email,
              type: arr[i].type,
              visitsAmount: arr[i].visitsAmount,
              maxVisitsPerTime: arr[i].maxVisitsPerTime,
              name: arr[i].name,
              cost: arr[i].cost,
              services: arr[i].services,
              appliedProducts: arr[i].appliedProducts,
              setRelatives: arr[i]._doc.setRelatives,
              description: arr[i].description,
              expired: arr[i].expired,
              age: arr[i].age
          });
          let result = await sub.save().catch(e => console.log(e));
          if(!result)return false;
          let subType = await SubscriptionTypeModel.findOne({id: arr[i].type});
          if(subType._doc.limit !== false){
              await SubscriptionTypeModel.findOneAndUpdate({id : arr[i].type}, {$inc : {limit: -1}});
          }

          /*
          * 1C api set subscription
          * */

          let user = await UsersModel.findOne({phone: result.phone});
          let options = { method: 'POST',
              url: process.env.API_C,
              headers: {
                  'cache-control': 'no-cache',
                  'Content-Type': 'application/json',
                  'accept-encoding': 'gzip, deflate',
                  Authorization: 'Basic '+ new Buffer(process.env.API_C_LOGIN + ':' + process.env.API_C_PASSWORD).toString('base64')
              },
              body: {
                  type: 'sale',
                  data: new Date().toISOString(),
                  IDdoc: sub.id,
                  Customer: [
                      {
                          IDWebKL: user.phone,
                          N: user.name,
                          F: user.sname,
                          P: ' ',
                          Birth: user.bday.toISOString(),
                          Phone: user.phone
                      }
                  ],
                  product: [{
                      IDWebTov: result.type,
                      quantity: '1',
                      price: result.cost
                  }]
              },
              json: true };
          let req = {};
          await request.post(options).on('response', resp => req = resp)
              .catch(e => {
                  console.log(e.message);
              });
          if(req.statusCode === 200){
              console.log('Subscription bought and recorded to 1C');
          }else{
              if(req.statusCode.toString()[0] === '4'){
                  console.log('1C server error');
                  // pingApi(options, 10000);
              } else if(req.statusCode.toString()[0] === '5'){
                  console.log('1C client error!');
              } else if(req.statusCode.toString()[0] === '3'){
                  console.log('1C request error!')
              }
          }
      }
      return true;
  }  catch (e) {
      console.log('trouble here');
      console.log(e.message);
      return false;
  }
};

/*
* 1C ping function
* @param    {object}    options Options for request
* @param    {number}    timeout Timeout for setTimeout (ms)
*
* @return   {bool}  Was request sent successfully
* */

pingApi = async (options, timeout) => {
    let req;
    while(req !== 200){
        await new Promise(resolve => setTimeout(resolve, timeout));
        await request.post(options).on('response', resp => req = resp.statusCode)
            .catch(e => {
                console.log(e.message);
            });
        console.log(req);
    }
    return true;
};

/*
* Get all subscriptions of user
*
* @param    {string}    phone   Users phone number
*
* @return   {array} Array of subscriptions
* */

Subscriptions.getSubscriptions = async phone => {
    try {
        let arr = await Subscriptions.find({ $or:[
                {phone: phone},
                {'relative.phone' : phone}
            ]});
        if(!arr[0]) return false;
        arr.forEach(value => {
            delete value._doc._id;
            value.services.forEach(value => {
                delete value._doc._id;
            });
            value.appliedProducts.forEach(value => {
                delete value._doc._id;
            });
        });
        return arr;
    }catch (e) {
        console.log(e.message);
        return false;
    }
};

/*
* Get selected user subscription
*
* @param    {string}    id      Subscription id
*
* @return   {object}    Subscription object
* */

Subscriptions.getOne = async (id) => {
    try {
        let subscription = await Subscriptions.findOne({id: id});
        if(!subscription) return false;
        delete subscription._doc._id;
        subscription.appliedProducts.forEach(value => {
            delete value._doc._id;
        });
        return subscription;
    }catch (e) {
        console.log(e);
        return false;
    }
};

/*
* Decreases amount of visits for selected subscription on selected number
*
* @param    {string}    id      Id of subscription
* @param    {number}    amount  Decrease amount
*
* @return   {number}    Left amount of visits
* */

Subscriptions.decreaseOn = async (id, amount) => {
    try {
        let current = await Subscriptions.findOne({id: id});
        if(!current) return false;
        if(current.visitsAmount - amount < 0)return false;
        await Subscriptions.findOneAndUpdate({id: id}, {visitsAmount : current.visitsAmount - amount}, {runValidators : true});
        return current.visitsAmount - amount;
    }catch (e) {
        console.log(e.message);
        return false;
    }
};

/*
* Set relative to subscription
*
* @param    {object}    subscription    Subscription object
* @param    {object}    relative        Relative object
*
* @return   {object}    Updated subscription object
* */

Subscriptions.setRelative = async (subscription, relative) => {
    try {
        let subscriptionType = await SubscriptionTypeModel.getSubscriptions(subscription.type);
        if(subscription.visitsAmount !== subscriptionType.visits){return false;}
        if(!await Subscriptions.findOneAndUpdate(
            {id: subscription.id},
            {
                'relative.id' : relative._id.toString(),
                'relative.name': relative.name,
                'relative.sname': relative.sname,
                'relative.phone' : relative.phone
            },
            {runValidators: true}
            ))return false;
        return await Subscriptions.findOne({id: subscription.id});
    }catch (e) {
        console.log(e.message);
        return false;
    }
};

module.exports = Subscriptions;