const mongoose = require('mongoose');

const StructureSchema = new mongoose.Schema({
   name: {
       type: String,
       required: true,
       unique: true
   },
    services: [{
        id: {
            type: String,
            required: false,
            trim: true
        }
    }],
    amountPerTime: [[{type: String}]],
    status: {
        // Active or not
        type: Boolean,
        required: true,
        default: true
    }
});

const Structure = new mongoose.model('structures', StructureSchema);

const ServicesModel = require('./Services');

/*
* Create new structure
*
* @param    {string}    name            Name of new structure
* @param    {array}     services        Array of exists services
* @param    {array}     amountPerTime   Array of possible service combination
*
* @return   {obj}   New structure object
* */

Structure.setStructure = async ({name, services, amountPerTime}) => {
  try {
      if(!name || !services || !amountPerTime)return false;
      if(await Structure.findOne({name: name}))return false;
      //Check is each service was specified in amountPerTime array
      for (let i = 0; i < services.length; i++) {
          if(!amountPerTime.find(val => val.find(val => val === services[i].id))) return false;
      }
      //Check for repeats in each pair
      for (let i = 0; i < amountPerTime.length; i++) {
          for (let j = 0; j < amountPerTime[i].length; j++) {
              if(amountPerTime[i].filter(val => val === amountPerTime[i][j]).length > 1)return false;
              if(!services.find(val => val.id === amountPerTime[i][j])) return false;
          }
      }

      let structure = await new Structure({
          name,
          services,
          amountPerTime
      }).save();
      for (let i = 0; i < structure.services.length; i++) {
          await ServicesModel.findOneAndUpdate({id: structure.services[i].id}, {structure: structure.id}, {runValidators: true});
      }
      return structure;
  }  catch (e) {
      console.log('setStructure', e.message);
      return false;
  }
};

/*
* Returns id of structure for selected service
* @param    {string}    id  Id of structure
*
* @return   {object}    Structure
* */

Structure.getStructure = async id => {
    try {
        let structure = await Structure.findOne({_id: id});
        return structure ? structure : false;
    }catch (e) {
        console.log(e.message);
        return false;
    }
};

/*
* Delete structure
*
* @param    {string}    id  Structure _id
*
* @return   {boolean}      Was deleted
* * */

Structure.deleteStructure = async id => {
    try {
        const ServicesModel = require('./Services');
        let structure = await Structure.findOne({_id: id});
        if(!structure) return false;

        for (let i = 0; i < structure.services.length; i++) {
            await ServicesModel.findOneAndUpdate({id: structure.services[i].id}, {structure : null, status: false}, {runValidators: true})
        }
        return !! await Structure.deleteOne({_id: id});
    }  catch (e) {
        console.log('Structure.deleteStructure', e.message);
        return false;
    }
};


module.exports = Structure;