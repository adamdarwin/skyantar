const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const ServicesSchema = new Schema({
    id: {
      type: String,
      required: true,
      unique: true
    },
    name : {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    products : [
        {
            type: String
        }
    ],
    cost : [{
        type: Number,
        required: true
    }],
    max_people: {
        type: Number,
        required: true
    },
    time        :   [{
        start :{
            type: String,
            required: true,
            trim: true
        },
        end :{
            type: String,
            required: true,
            trim: true
        }
    }],
    customPrice: [
        // Custom price for selected date (time)
        {
            dateStart : {
                type: Date,
                required: true
            },
            dateEnd : {
                type: Date,
                required: true,
                default: null
            },
            cost: {
                type: Number,
                required: true
            }
        }
    ],
    structure: {
        type: String,
        default: null
    },
    type: {
      type: String,
      required : true
    },
    preordered: {
        type: Number,
        required: true,
        default: 24 * 60 * 60 * 1000
    },
    bdayDiscount: {
        type: Number,
        required: true,
        default: 1
    },
    status: {
        // Active or not
        type: Boolean,
        required: true,
        default: true
    },
    day_time: {
        // time when day starts
        type: String,
        required: true,
        default: '14:00'
    },
    day_diff: {
        //price diff before and after day: day_diff = day price - morning price
        type: Number,
        required: true,
        default: 0
    },
    evening_diff: {
        //price diff before and after evening: evening_diff = evening price - morning price
        type: Number,
        required: true,
        default: 0
    },
    evening_time: {
        // time when evening starts
        type: String,
        required: true,
        default: '17:00'
    }
});

const Service = mongoose.model('services', ServicesSchema);

const ProductsModel = require('./Products');
const HolidaysModel = require('./holidays');
const SubscriptionTypesModel = require('.././models/SubscriprtionTypes');
const StructuresModel = require('.././models/Structure');

/*
* Returns array with service information
*
* @param    {string}    id  Id of service.
*
* @return   {array}     Array
* */
Service.findService = async (id, user =null) => {
    try{
        let form = {
            main: await Service.findOne({id : id.toString()})
        };
        if(form.main){
            if(!form.main.status) return false;
            form.main._doc._id ? delete form.main._doc._id : null;
            form.main._doc.max_people ? delete form.main._doc.max_people : null;

            if(form.main.products[0]){
                let arr = await Promise.all(form.main.products.map(item => ProductsModel.getProduct(item)));


                form.products = arr;
                form.products = form.products.filter(val => val.status);
                form.products.forEach(item => {
                    if(item){
                        item._doc._id ? delete item._doc._id : null;
                        if(typeof item._doc.amount !== "object"){
                            delete item._doc.amount;
                        }else{
                            Object.keys(item._doc.amount).forEach(val => {
                                if(typeof item._doc.amount[val] === 'object'){
                                    delete item._doc.amount[val].amount;
                                }
                            })
                        }
                    }
                });
            }
            form.subscriptions = await SubscriptionTypesModel.getSubscriptionsByService(id);
            return form;
        }else{
            return false;
        }
    }catch (e) {
        console.log('Service.findService', e.message);
        return false
    }
};

/*
* Get all services id
*
* @return   {array} Array of services id
* */

Service.getAll = async () => {
    try{
        let arr = await Service.find({});
        arr = arr.filter(val => !!val.status);
        return arr.map(item => item.id);
    }catch (e) {
        console.log(e.message);
        return false;
    }
};

/*
* Get cost of service with selected parameters
*
* @param    {string}  id        Id of service
* @param    {string}  start     Start time of visit
* @param    {string}  end       End time of visit
* @param    {number}  people    Amount of people
* @param    {string}  user      Users phone number
*
* @return   {object}    Cost of service and bday status
* */

Service.getCost = async (id, start, end, people, user=null) => {
    try{
        let service = await Service.findOne({id});
        if(!service) return false;
        let result = false;
        let cost;
        let custom = service.customPrice.find(val => new Date(start) <= new Date(val.dateEnd) && new Date(start) >= new Date(val.dateStart) ? val : false);
        if(custom){
            result = id === '05' ? custom.cost : custom.cost * people * ( - ( (new Date(start)) - (new Date(end))) / 3600000);
        }else{
            if(!await HolidaysModel.check(start.substr(5,5))){
                cost = service.cost[(new Date(start)).getDay()];
                if(new Date(start) >= new Date(new Date(start).setUTCHours(service.day_time.split(':')[0], service.day_time.split(':')[1]))){
                    cost -= service.day_diff;
                    if((service.id !== '05' || (new Date(start).getDay() === 0 || new Date(start).getDay() === 6)) && new Date(start) >= new Date(new Date(start).setUTCHours(service.evening_time.split(':')[0], service.evening_time.split(':')[1])))
                        cost = service.cost[(new Date(start)).getDay()] - service.evening_diff;
                }
                result = id === '05' ? cost * people : cost * people * ( - ( (new Date(start)) - (new Date(end))) / 3600000);
            }else{
                result = id === '05' ? service.cost[7] * people : service.cost[new Date(start).getDay()] * people * (-((new Date(start)) - (new Date(end))) / 3600000);
            }
        }
        if(result <= 0) return false;
        if(user && user.bday && user.bday.getMonth() === new Date(start).getMonth() && user.bday.getDate() === new Date(start).getDate() && user.bdayVerified){
            result = cost * (people -1) + cost * service._doc.bdayDiscount / 100;
            return result ? {cost : result, bday : true} : false;
        }
        return result !== false ? {cost : result, bday : false} : false;
    }catch (e) {
        console.log('Service.getCost', e.message);
        return false;
    }
};

/*
* Returns max available amount
*
* @param    {string}    id  Id of service
*
* @return   {number}    Max people amount in on time
* */

Service.getMaxAvailable = async id => {
    try{
        return (await Service.findOne({id: id.toString()})).max_people;
    }catch (e) {
        console.log(e.message);
        return false;
    }
};

/*
* Create new service
*
* @param    {object}    obj Service object
*
* @return   {object}    Service model
* */

Service.createNew = async obj => {
    try {
        if(await Service.findOne({name: obj.name}))return false;

        let structure = await StructuresModel.findOne({_id: obj.structure});
        if(!structure) return false;
        let id = (await Service.countDocuments({})) + 1 + '';
        id = '0' + id;
        while(await Service.findOne({id: id})){
            id = + (id.slice(1)) + 1;
            id = '0' + id;
        }
        structure.services.push({id: id});
        if(obj.pair > -1){
            if(obj.pair > structure.amountPerTime.length - 1) return false;
            await StructuresModel.findOneAndUpdate({_id:structure.id}, {$push: {['amountPerTime.' + obj.pair + ''] : id}}, {runValidators: true});
            await structure.save();
        }else if(obj.pair === -1){
            structure.amountPerTime.push([id]);
        }else{
            return false;
        }
        let products = obj.products ? obj.products : [];
        for (let i = 0; i < products.length; i++) {
            if(!await ProductsModel.findOne({id: products[i]}))return false;
        }
        if(obj.cost.length < 8)return false;
        if(obj.time.length < 8)return false;
        for (let i = 0; i < obj.cost.length; i++) {
            if(obj.cost[i] < 0) return false;
        }
        for (let i = 0; i < obj.time.length; i++) {
            if(!obj.time[i].end || !obj.time[i].start) return false;
            if(obj.time[i].end <= obj.time[i].start)return false;
        }
        if(obj.preordered < 0) return false;
        if(obj.type !== 'reserve' && obj.type !== 'visit') return false;
        if(!obj.bdayDiscount)obj.bdayDiscount = false;
        if(obj.bdayDiscount && obj.bdayDiscount <= 0) return false;

        if(obj.evening_diff  && (typeof obj.evening_diff !== 'number' || isNaN(obj.evening_diff))) return false;
        if(obj.evening_time && typeof obj.evening_time !== 'string') return false;
        if(obj.evening_time && obj.evening_time.split(':').length !== 2) return false;
        if(obj.evening_time && (obj.evening_time.split(':')[0].length !== 2 || obj.evening_time.split(':')[1].length !== 2)) return false;

        if(obj.day_diff  && (typeof obj.day_diff !== 'number' || isNaN(obj.day_diff))) return false;
        if(obj.day_time && typeof obj.day_time !== 'string') return false;
        if(obj.day_time && obj.day_time.split(':').length !== 2) return false;
        if(obj.day_time && (obj.day_time.split(':')[0].length !== 2 || obj.day_time.split(':')[1].length !== 2)) return false;

        if(obj.customPrice){
            for (let i = 0; i < obj.customPrice.length; i++) {
                if(!obj.customPrice[i].dateStart) return false;
                if(!obj.customPrice[i].dateEnd){
                    obj.customPrice[i].dateEnd = new Date(new Date(new Date(obj.customPrice[i].dateStart).setUTCDate(new Date(obj.customPrice[i].dateStart).getDate() + 1)).setUTCHours(0,0,0)).toISOString();
                }
            }
            for (let i = 0; i < obj.customPrice.length; i++) {
                if(obj.customPrice[i].dateEnd && new Date(obj.customPrice[i].dateEnd) - new Date(obj.customPrice[i].dateStart) <= 0) return false;
                let arr = obj.customPrice.map(val => val);
                arr.splice(i, 1);
                if(arr.find(val => {
                    if(val.dateEnd && obj.customPrice[i].dateEnd){
                        if(new Date(obj.customPrice[i].dateStart) <= new Date(val.dateEnd) && new Date(obj.customPrice[i].dateEnd) >= new Date(val.dateStart)){
                            console.log(obj.customPrice[i].dateStart ,obj.customPrice[i].dateEnd);
                            console.log(val.dateStart, val.dateEnd);
                            return true;
                        }
                    }
                }))return res.sendStatus(400);
                if(obj.customPrice[i].cost <= 0) return false;
            }
        }

        let service = new Service({
            id: id,
            name: obj.name,
            products: products,
            cost: obj.cost,
            max_people: obj.max_people,
            time: obj.time,
            structure: obj.structure,
            preordered: obj.preordered,
            type: obj.type,
            bdayDiscount: obj.bdayDiscount,
            evening_diff: obj.evening_diff ? obj.evening_diff : 0,
            evening_time: obj.evening_time ? obj.evening_time : '17:00',
            customPrice: obj.customPrice ? obj.customPrice : []
        });
        await structure.save();
        return await service.save();
    }catch (e) {
        console.log('Service.createNew', e.message);
        return false;
    }
};

/*
* Delete service
*
* @param    {string}    id  Service id
*
* @return   {boolean}   Was deleted
* */

Service.deleteService = async id => {
  try {
      let service = await Service.findOne({id: id});
      if(!service) return false;

      let structure = await StructuresModel.findOne({_id: service.structure});
      let amountPerTime = structure.amountPerTime;
      for (let j = 0; j < structure.amountPerTime.length; j++) {
          structure.amountPerTime[j] = structure.amountPerTime[j].filter(val => val !== id);
      }
      amountPerTime = amountPerTime.filter(val => !!val[0]);
      let services = structure.services;
      for (let j = 0; j < structure.services.length; j++) {
          services = services.filter(val => val.id !== id);
      }
      await StructuresModel.findOneAndUpdate({_id: service.structure}, {amountPerTime: amountPerTime, services: services}, {runValidators: true});
      return !!await Service.findOneAndDelete({id});
  }  catch (e) {
      console.log('Service.deleteService', e.message);
      return false;
  }
};

module.exports = Service;