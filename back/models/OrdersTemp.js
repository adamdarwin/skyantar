const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    validator = require('validator');

let OrdersTempSchema = new Schema({
    id: {
        type: String,
        unique: true,
        trim: true,
        required: true
    },
    phone: {
        type: String,
        required: true,
        validate(value){
            if(!validator.isMobilePhone(value, 'uk-UA')){
                throw new Error('Phone is invalid');
            }
        }
    },
    email: {
        type: String,
        default: null,
        trim: true,
        validate(value){
            if(value && !validator.isEmail(value)){
                throw new Error('Email is invalid');
            }
        }
    },
    body: [{
        id:{
            type: String,
            required: true,
            trim: true
        },
        start: {
            type: Date,
            required: true
        },
        end: {
            type: Date,
            required: true
        },
        people:{
            type: Number,
            required: true,
            default: 1
        },
        cost: {
            type: Number
        },
        products: [{
            id:{
                type: String,
                required: true
            },
            amount:{
                type: Schema.Types.Mixed,
                required: true
            },
            cost:{
                type: Number
            }
        }]
    }],
    total: {
        type: Number,
        required: true
    },
    used: {
        type: Boolean,
        default: false,
    },
    phoneSign : {
        type : String,
        default : null
    }
});

let OrderTemp = mongoose.model('orders-temp', OrdersTempSchema);

/*
* Save order to temp database
*
* @param    {array} arr Object arr
*
* @return   {bool}  Was the order saved
* */

OrderTemp.setOrder = async arr => {
    try{
        let order = new OrderTemp({
            id: Date.now(),
            phone: arr.phone,
            email: arr.email,
            body: arr.body,
            total: arr.total
        });
        return await order.save();
    }catch (e) {
        console.log('OrderTemp.setOrder', e.message);
        return false;
    }
};

/*
* Returns order by id
*
* @param    {string}    id  Order id
*
* @return   {object}    Order object
* */

OrderTemp.getOrder = async id => {
    try {
        return await OrderTemp.findOne({id: id});
    }catch (e) {
        console.log('OrderTemp.getOrder', e.message);
        return false;
    }
};

module.exports = OrderTemp;