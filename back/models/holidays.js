const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const HolidaysSchema = new Schema({});

const Holidays = new mongoose.model('holidays', HolidaysSchema);

/*
* Checkin is date are holiday
* @param    {string}    Date of order
*
* @return   {bool}  Is date are holiday
* */

Holidays.check = async date =>{
    try{
        let holidays = await Holidays.findOne({});
        return !!holidays._doc[date];
    }catch (e) {
        console.log('Holidays.check', e.message);
        return false;
    }
};


Holidays.getForMonth = async (date=null) => {
    try {
        date = date ? new Date(date): new Date(Date.now());
        let arr = await Holidays.findOne({});
        let result = [];
        Object.keys(arr._doc).forEach(item => {
            if(+item.slice(0,-3) === date.getMonth()+1){
                result.push(item);
            }
        });
        return result;
    }  catch (e) {
        console.log('Holidays.getForMonth', e.message);
        return false;
    }
};

module.exports = Holidays;