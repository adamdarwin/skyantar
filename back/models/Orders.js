const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    validator = require('validator');


let OrdersSchema = new Schema({
    id: {
        type: String,
        unique: true,
        trim: true,
        required: true
    },
    phone: {
        type: String,
        default: null,
        validate(value){
            if(!validator.isMobilePhone(value, 'uk-UA')){
                throw new Error('Phone is invalid');
            }
        }
    },
    email: {
        type: String,
        default: null,
        trim: true,
        validate(value){
            if(value && !validator.isEmail(value)){
                throw new Error('Email is invalid');
            }
        }
    },
    type: {
        type: String,
        required: true,
        trim: true
    },
    subscription: {
        type: {
            name:{
                type: String,
                default: null,
                required: true
            },
            id:{
                type: String,
                default: null,
                required: true
            },
            relative : {
                type: {
                    id : {
                        type: String,
                        required : true
                    },
                    name : {
                        type: String,
                        required : true
                    },
                    sname : {
                        type: String,
                        required : true
                    },
                    phone : {
                        type: String,
                        required : true
                    }
                },
                default: null,
                required: true
            }
        },
        default : null
    },
    body: [{
        id:{
            type: String,
            required: true,
            trim: true
        },
        start: {
            type: Date,
            required: true
        },
        end: {
            type: Date,
            required: true
        },
        people:{
            type: Number,
            required: true,
            default: 1
        },
        cost: {
            type: Number
        },
        products: [{
            id:{
                type: String,
                required: true
            },
            amount:{
                type: Schema.Types.Mixed,
                required: true
            },
            cost:{
                type: Number
            }
        }],
        status : {
            type: String,
            required: true,
            default: 'ordered'
            //    Can be: 'ordered', 'active', 'used', 'disabled', 'processing', 'refunded'
        },
        disabled : {
            type: String,
            default: null
        },
        bday: {
            required: true,
            type: Boolean,
            default: false
        },
        refund : {
            //    amount of refund
            type: Number,
            default: null
        },
        refundM : {
        //    refund multiplier
            type: Number,
            default: null
        },
        sentToBooker : {
            type: Boolean,
            default : false
        }
    }],
    total: {
        type: Number
    },
    phoneSign : {
        // field for SMS code storing
        type: String,
        default: null,
        required: false
    }
});


let Order = mongoose.model('orders', OrdersSchema);

let OrdersTempModel = require('./OrdersTemp');
let ServicesModel = require('./Services');
let HolidaysModel = require('./holidays');
let StructureModel = require('./Structure');

/*
* Saves order from temp order base to main
* @param    {string}    type    Type of order
* @param    {object}    id      Id of temp oder
* @param    {object}    order   Order if type is subscription
*
* @return   {bool}  Was the order saved
* */

Order.setOrder = async ({type, id = null, body = null}) => {
    try{
        if(type === 'subscription'){
            if(!body || !body.subscription.id || !body.subscription.name) return false;
            let service = await ServicesModel.findService(body.subscription.service);
            let structure = await StructureModel.getStructure(service.main.structure);
            let newOrder = new Order({
                id: Date.now(),
                phone: body.phone,
                email: body.email ? body.email : null,
                type: 'subscription',
                subscription: {
                    id: body.subscription.id,
                    name: body.subscription.name,
                    relative : body.subscription.relative
                },
                body: [{
                    id: body.subscription.service,
                    start: body.subscription.start,
                    end: body.subscription.end,
                    people: body.subscription.amount,
                    products: body.subscription.appliedProducts
                }]
            });
            let result = await newOrder.save();
            return result ? result : false
        }else if(type === 'payment' || type === 'cash'){
            if(!id)return false;
            let orderTemp = await OrdersTempModel.getOrder(id);
            if(orderTemp){
                let order = new Order({
                    id: orderTemp.id,
                    phone: orderTemp.phone,
                    email: orderTemp.email ? orderTemp.email : null,
                    type: type,
                    body: orderTemp.body,
                    total: orderTemp.total
                });
                let result = await order.save();
                if(result){
                    await OrdersTempModel.deleteOne({id: id});
                }
                return result._id.toString();
            }else{
                console.log('Can\'t save the order');
                return false
            }
        }
    }catch (e) {
        console.log('Order.setOrder', e.message);
        return false;
    }
};

/*
* Check can we create order in selected time with selected amount of people
*
* @param    {string}    service     Service
* @param    {string}    people      Amount of people
* @param    {string}    start       Start time
* @param    {string}    end         End time
*
* @return   {bool}    Can we?
* */

Order.checkServicesByTime = async ({service, people, start, end}) => {
    try{
        let id = service.main._doc.id;
        let structure = await StructureModel.getStructure(service.main.structure);
        if (!structure) {
            return false;
        }
        let near = structure.amountPerTime;
        for (let i = 0; i < near.length; i++) {
            if(!near[i].find(value => {return value === id})){
                for (let j = 0; j < near[i].length; j++) {
                    let arr = await Order.findOne({
                        $and: [
                            {body : {$elemMatch : {id : near[i][j]}}},
                            {body : {$elemMatch : {start: {$lt: new Date(end)}}}},
                            {body : {$elemMatch : {end: {$gt: new Date(start)}}}},
                        ]
                    });
                    if(arr)return false;
                }
            }
        }
        if(service.main.type === 'reserve') {
            let arr = await Order.findOne({
                $and: [
                    {body : {$elemMatch : {id : id}}},
                    {body : {$elemMatch : {start: {$lt: new Date(end)}}}},
                    {body : {$elemMatch : {end: {$gt: new Date(start)}}}},
                ]
            });
            return !!!arr;
        }else if(service.main.type === 'visit'){
            let arr = await Order.find({
                $and: [
                    {body : {$elemMatch : {id : id}}},
                    {body : {$elemMatch : {start: {$lt: new Date(end)}}}},
                    {body : {$elemMatch : {end: {$gt: new Date(start)}}}},
                ]
            });
            if(arr[0]){
                let sum = arr.reduce((sum, val) => {
                    return sum += val.body.reduce((sum, val) => {
                        return val.id === id ? sum += val.people: sum;
                    }, 0)
                }, 0);
                return sum + people <= await ServicesModel.getMaxAvailable(id);
            }else{
                return true;
            }
        }
    }catch (e) {
        console.log('Order.checkServicesByTime', e.message);
        return false;
    }
};

/*
* Get amount of used products for set time
*
* @param    {string}    id      Id of product
* @param    {string}    start   Start time
* @param    {string}    end     End time
* @param    {string}    option  Option name
*
* @return   {number}    Amount of used products
* */

Order.checkProductsByTime = async ({id, start, end, option = null}) => {
    try{
        if (option) {
            let arr = await Order.find(
                {
                    $and : [
                        {body: {$elemMatch: {products: {$elemMatch: {id: id, amount: {$elemMatch: {option: option}}}}}}},
                        {body: {$elemMatch : {start: {$lt: new Date(end)}}}},
                        {body: {$elemMatch : {end: {$gt: new Date(start)}}}}
                    ]
                }
            );
            if(arr[0]){
                return arr.reduce((sum, val) => {
                    return sum += val.body.reduce((sum, val) => {
                        return sum += val.products.reduce((sum, val) => {
                            if(typeof val.amount !== 'number'){
                                return sum += val.amount.reduce((sum, val) => {
                                    return val.option === option ? sum+= +val.amount : sum;
                                }, 0)
                            }else{
                                return sum;
                            }
                        }, 0)
                    }, 0);
                }, 0);
            }else{
                return 0;
            }
        } else {
            let arr = await Order.find(
                {
                    $and : [
                        {body: {$elemMatch: {products: {$elemMatch: {id: id}}}}},
                        {body: {$elemMatch : {start: {$lt: new Date(end)}}}},
                        {body: {$elemMatch : {end: {$gt: new Date(start)}}}}
                    ]
                }
            );
            if(arr){
                return arr.reduce((sum, val) => {
                    return sum += val.body.reduce((sum, val) => {
                        return sum += val.products.reduce((sum, val) => {
                            if(typeof val.amount === 'number'){
                                return val.id === id ? sum+=val.amount : sum;
                            }else{
                                if(typeof val.amount === 'string'){
                                    throw new Error('Amount should be number.');
                                }
                                return sum;
                            }
                        }, 0)
                    }, 0);
                }, 0);
            }else{
                return 0;
            }
        }
    }catch(e){
        console.log('Order.checkProductsByTime', e.message);
        return false;
    }
};

/*
* Check is selected day is fully busy for selected service
*
* @param    {string}    id      Service id
* @param    {object}    date    Selected date object
*
* @return   {bool}  Is busy
* */

// TODO: optimize?
Order.getBusyDate = async (id, date = null) => {
    try {
        let max = await ServicesModel.getMaxAvailable(id);
        let service = await ServicesModel.findService(id);
        let serviceTime = service.main.time;
        date = date ? date: new Date(Date.now()).toISOString();
        let start;
        let end;
        if(await HolidaysModel.check(date)){
            start = new Date(date.toISOString().replace(/T00:00/, 'T'+serviceTime[7].start));
            end = new Date(date.toISOString().replace(/T00:00/, 'T'+serviceTime[7].end));
        }else{
            start = new Date(date.toISOString().replace(/T00:00/, 'T'+serviceTime[date.getDay()].start));
            end = new Date(date.toISOString().replace(/T00:00/, 'T'+serviceTime[date.getDay()].end));
        }
        for (let i = 0; i < (end - start)/(30*60*1000); i++) {
            if(await Order.checkServicesByTime({
                service: service,
                people: 1,
                start: new Date(start.valueOf()+(30*60*1000)*i),
                end: new Date(start.valueOf()+(30*60*1000)*(i+1))
            }))return true;
        }
    }catch (e) {
        console.log('Order.getBusyDate', e.message);
        return false;
    }
};

/*
* Returns busy time for a day
*
* @param    {string}    id      Id of service
* @param    {string}    time    Checking date
* @param    {number}    people  Number of people
* */

Order.getBusyTime = async (id, time, people = null) => {
    try{
        people = people ? people : 1;
        let service = await ServicesModel.findService(id);
        let serviceTime = service.main.time[new Date(time).getDay()];
        let serviceStart = new Date((new Date(new Date(time).setUTCHours(0,0,0,0)).toISOString()).replace(/T00:00/, 'T'+serviceTime.start));
        let serviceEnd = new Date((new Date(new Date(time).setUTCHours(0,0,0,0)).toISOString()).replace(/T00:00/, 'T'+serviceTime.end));
        let arr = [];
        for (let i = 0; i < (serviceEnd - serviceStart)/(30*60*1000); i++) {
            let start = new Date(serviceStart.valueOf()+30*60*1000*i).toISOString();
            let end = new Date(serviceStart.valueOf()+30*60*1000*(i+1)).toISOString();
            if(!await Order.checkServicesByTime({service : service, people : people, start : start, end : end})){
                if(arr.length > 0 && arr[arr.length - 1].end >= start){
                    arr[arr.length - 1].end = end;
                }else{
                    arr.push({start: start, end: end});
                }
            }
        }
        return arr;
    }catch (e) {
        console.log('Order.getBusyTime', e.message);
        return false;
    }
};

/*
* Returns edited & sorted array of time
*
* @param    {array}     arr Array of times
* @param    {string}    id  Service id
*
* @return   {array} Edited array
* */

const getCutsTime = (arr, id) => {
    let result = arr.map(item => {
        for (let i = 0; i < item.body.length; i++) {
            if(item.body[i].id === id){
                return {
                    start: item.body[i].start,
                    end: item.body[i].end,
                    people: item.body[i].people
                }
            }
        }
    });
    return result.sort(function(item, next){
        return item.start <= next.start ? -1 : 1;
    })
};

/*
* Return sorted array of separated start/end time
*
* @param    {array}     arr         Time array
* @param    {string}    dayStart    Service start time
* @param    {string}    dayEnd      Service end time
*
* @return   {array} Sorted separated time
* */

const getSeparatedTime = (arr, dayStart, dayEnd) => {
    let result = [];
    for (let i = 0; i < arr.length; i++) {
        if(!result.find(item => {
            return item ? item.getTime() === arr[i].start.getTime() : false;
        })){
            result.push(arr[i].start);
        }
        if(!result.find(item => {
            return item ? item.getTime() === arr[i].end.getTime() : false;
        })){
            result.push(arr[i].end);
        }
    }
    result.push(new Date(dayStart), new Date(dayEnd));
    return result.sort(function(item, next){
        return item <= next ? -1 : 1;
    });
};

/*
* Check is order was saved and payed
* @param    {string}    id  Order id
*
* @return   {string}    Verification code for order
* */

Order.checkSubmit = async id =>{
    let doc = await Order.findOne({id});
    return doc ? doc._id.toString() : false;
};

/*
* Returns all orders requested by the phone
*
* @param    {string}    phone   Phone number
*
* @return   {array} Array of order objects
* */

Order.getAll = async phone => {
    try {
        return await Order.find({phone});
    }catch (e) {
        console.log('Order.getAll', e.message);
        return false;
    }
};

module.exports = Order;