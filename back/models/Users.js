const mongoose = require('mongoose'),
    validator = require('validator'),
    bcrypt = require('bcryptjs'),
    jwt = require('jsonwebtoken');


let UsersSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    sname: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        default: null,
        validate(value){
            if(value && !validator.isEmail(value)){
                throw new Error('Email is invalid');
            }
        }
    },
    bday: {
        type: Date
    },
    bdayVerified: {
        type: mongoose.SchemaTypes.Bool,
        default: false
    },
    phone: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        validate(value){
            if(!validator.isMobilePhone(value, 'uk-UA')){
                throw new Error('Phone is invalid');
            }
        }
    },
    docs: [
        {
            type: String,
            required: true,
            trim: true
        }
    ],
    password:{
        type: String,
        required: true,
        minLength: 7,
        trim: true
    },
    decline: {
        // Decline message
        type: String,
        default: null
    },
    relatives: {
        type: [{
            name:{
                type: String,
                verified: true,
                trim: true
            },
            sname: {
                type: String,
                verified: true,
                trim: true
            },
            phone: {
                type: String,
                validate(value){
                    if(!validator.isMobilePhone(value, 'uk-UA')){
                        throw new Error('Phone is invalid');
                    }
                },
                default: null
            },
            bday: {
                type: String,
                required: true
            },
            docs: [{
                type: String,
                required: true,
                trim: true
            }],
            verified: {
                type: Boolean,
                required: true,
                default: false
            },
            decline: {
                // Decline message
                type: String,
                default: null
            }
        }],
        default: null,
        unique: false
    },
    tokens: [{
        token: {
            type: String,
            requried: true
        }
    }]
});

/*
* Generates JWT for user session
*
* @return   {string}    JWT
* */

UsersSchema.methods.generateJWT= async function(){
    try {
        const token = jwt.sign({id: this.id}, process.env.JWT_SECRET, {expiresIn: '1 day'});
        this.tokens = [{token}];
        await this.save().catch(err => {if (err)console.log(err)});
        return token;
    }catch (e) {
        console.log(e.message);
        return false;
    }
};

/*
* Check is user credentials are correct
*
* @param    {object}    Object with phone and password
*
* @return   {object}    User model
* */

UsersSchema.statics.findByCredentials = async ({phone, password}) => {
    try{
        const user = await Users.findOne({phone});
        if(!user){
            return false;
        }
        return await bcrypt.compare(password, user.password) ? user : false;
    }catch (e) {
        console.log(e.message);
        return false;
    }
};

UsersSchema.pre('save', async function (next) {
    if (this.isModified('password')) {
        this.password = await bcrypt.hash(this.password, 8)
    }
    next();
});
UsersSchema.pre('findOneAndUpdate', async function (next) {
    if (Object.keys(this._update).indexOf('password') !== -1) {
        this._update.password = await bcrypt.hash(this._update.password, 8)
    }
    next();
});


let Users = new mongoose.model('users', UsersSchema);

/*
* Save user to database
*
* @param    {object}    data    User data
*
* @return   {object}    User model
* */

Users.setUser = async data => {
    try{
        let user = new Users({
            name: data.name,
            sname: data.sname,
            email:  data.email,
            phone: data.phone,
            password: data.password,
            bday: new Date(new Date(data.bday).setUTCHours(0,0,0,0)).toISOString()
        });
        let result = await user.save();
        delete  result._doc.tokens;
        delete  result._doc.password;
        return result;
    }catch (e) {
        console.log(e.message);
        return false;
    }
};

/*
* Check is user already exists
*
* @param    {string}    phone   User phone
*
* @return   {bool}  Is exists
* */

Users.isExists = async phone => {
    try {
        return !!(await Users.findOne({phone}));
    }catch (e) {
        console.log('Users.isExists', e.message);
        return false
    }
};

/*
* TEMP seed users
* */

Users.seed = async amount => {
    try {
        for (let i = 0; i < amount; i++) {
            let user = new Users({
                name: 'Bot',
                sname: 'Botovich',
                email:  'bot@bot.net',
                phone: '+380' + (100000000 + i),
                password: 'qwerty',
                bday: new Date().toISOString()
            });
            console.log(user.phone);
            await user.save();
            user = undefined;
        }
        return true;
    }  catch (e) {
        console.log('User.seed', e.message);
        return false
    }
};

/*
* Get user by relative phone number
*
* @param    {string}    phone   Relative phone
*
* @return   {object}    User
* */

Users.getRelative = async (userPhone, id) => {
    try {
        let user = await Users.findOne({phone: userPhone,relatives : {$elemMatch : {_id: id}}});
        if(!user) return false;
        return user.relatives.find(val => val._id.toString() === id ? val : false) ? user : false;
    }  catch (e) {
        console.log('User.getRelative', e.message);
        return false;
    }
};

/*
* Update user with new relative
*
* @param    {string}    name    Relative name
* @param    {string}    sname   Relative sname
* @param    {string}    phone   Relative phone
* @param    {object}    docs    Relative docs
* */


//TODO: delete temp route

Users.deleteUser = async phone => {
    try {
        await Users.deleteOne({phone});
        return true
    }catch (e) {
        return e.message;
    }
};

module.exports = Users;