const mongoose = require('mongoose');


let SubscriptionTypesSchema = new mongoose.Schema({
    id: {
        type: String,
        required: true,
        unique: true
    },
    name: {
        type: String,
        required: true,
        unique: true
    },
    cost: {
        type: Number,
        required: true
    },
    services: [{
        id : {
            type: String,
            required: true
        },
        name: {
            type: String,
            required: true
        }
    }],
    appliedProducts: [{
        id : {
            type: String,
            required: true
        },
        name: {
            type: String,
            required: true
        },
        amount: {
            type: Number,
            required: true
        }
    }],
    visits: {
        type: Number,
        required: true
    },
    maxVisitsPerTime:{
        type: Number,
        required: true,
        default: 1
    },
    description: {
        type: String,
        required: true
    },
    expired: {
        type: String,
        required: true
    },
    setRelatives: {
        type: Boolean,
        required: true,
        default: false
    },
    limit: {
        type: Number,
        required: true
    },
    age: {
        type: String,
        required: true
    }
});

const SubscriptionType = new mongoose.model('subscription-types', SubscriptionTypesSchema);

/*
* Create new subscription type
*
* @param    {object}    sub Subscription information
*
* @return   {string}    Subscription type id
* */

SubscriptionType.createSubscriptionType = async (sub) => {
    try {
        const ServicesModel = require('./../models/Services');
        const ProductsModel = require('./../models/Products');
        for (let i = 0; i < sub.services.length; i++) {
            let service = await ServicesModel.findService(sub.services[i].id);
            if(!service) return false;
            sub.services[i].name = service.main.name;
        }
        for (let i = 0; i < sub.appliedProducts.length; i++) {
            let product = await ProductsModel.getProduct(sub.appliedProducts[i].id);
            if(!product) return false;
            sub.appliedProducts[i].name = product.name;
        }
        let subModel = new SubscriptionType(sub);
        let result = {};
        await subModel.save().then(doc => result = doc);
        result.services = result.services ? result.services.map(item => {
            delete item._doc._id;
            return item;
        }): null;
        result.appliedProducts = result.appliedProducts ? result.appliedProducts.map(item => {
            delete item._doc._id;
            return item;
        }): null;
        return result;
    }catch (e) {
        console.log('SubscriptionType.createSubscriptionType', e.message);
        return false;
    }
};

/*
* Get subscriptions for a service
*
* @param    {string}    id  Id of service
*
* @return   {array} Array of subscriptions
* */

SubscriptionType.getSubscriptionsByService = async id => {
    try {
        let arr = await SubscriptionType.find({services: {$elemMatch : {id : id}}});
        return arr[0] ? arr : false;
    }catch (e) {
        console.log('SubscriptionType.getSubscriptionsByService', e.message);
        return false;
    }
};

/*
* Get subscriptions by id
*
* @param    {string}    id  Id pf subscription
*
* @return   {array, object} Array or object of found subscription(s)
* */

SubscriptionType.getSubscriptions = async id => {
    try {
        if(id){
            let arr = await SubscriptionType.findOne({id: id});
            return arr ? arr : false;
        }else{
            let arr = await SubscriptionType.find({});
            arr.forEach(val => {
                val.services = val.services.map(item => {
                    delete item._doc._id;
                    return item
                });
                val.appliedProducts = val.appliedProducts.map(item => {
                    delete item._doc._id;
                    return item
                });
            });
            return arr[0] ? arr : false;
        }
    }catch (e) {
        console.log('SubscriptionType.getSubscriptions', e.message);
        return false;
    }
};

/*
* Create new subscription type
*
* @param    {object}    obj Subscription type object
*
* @return   {object}    Subscription model
* */

SubscriptionType.createNew = async obj => {
    try {
        const ServiceModel = require('./Services');
        const ProductModel = require('./Products');
        if(!obj.name || await SubscriptionType.findOne({name:  obj.name}))return false;
        if(!obj.limit || obj.limit < 0) return false;
        if(!obj.cost || obj.cost < 0) return false;
        if(!obj.services || !obj.services[0]) return false;
        for (let i = 0; i < obj.services.length; i++) {
            let service = await ServiceModel.findOne({id: obj.services[i].id});
            if(!service) return false;
            obj.services[i].name = service.name;
        }
        if(!obj.appliedProducts)obj.appliedProducts = [];
        for (let i = 0; i < obj.appliedProducts.length; i++) {
            if(obj.amount < 0) return false;
            let product = await ProductModel.findOne({id: obj.appliedProducts[i].id});
            if(!product) return false;
            obj.appliedProducts[i].name = product.name;
        }
        if(obj.visits < 0) return false;
        if(new Date(obj.expired) <= new Date) return false;
        if(!obj.age) return false;
        if(obj.age !== 'adult' && obj.age !=='child')return false;

        let id = (await SubscriptionType.countDocuments({})) + 1 + '';
        id = '2' + id;

        while(await SubscriptionType.findOne({id: id})){
            id = + (id.slice(1)) + 1;
            id = '2' + id;
        }

        let type = new SubscriptionType({
            id:  id,
            description: obj.description,
            limit: obj.limit,
            name: obj.name,
            cost: obj.cost,
            services: obj.services,
            appliedProducts: obj.appliedProducts,
            visits: obj.visits,
            expired: obj.expired,
            setRelative: obj.setRelative ? obj.setRelative : false,
            age: obj.age
        });
        return await type.save();
    }catch (e) {
        console.log('SubscriptionType.createNew', e.message);
        return false;
    }
};

/*
* Delete subscription type
*
* @param    {string}    id  Subscription type id
*
* @return   {boolean}   Was deleted
* */

SubscriptionType.deleteType = async id => {
    try {
        let subscription = await SubscriptionType.findOne({id: id});
        if(!subscription) return false;
        return !! await SubscriptionType.findOneAndDelete({id: id});
    }catch (e) {
        console.log('SubscriptionType.deleteType', e.message);
        return false;
    }
};

module.exports = SubscriptionType;